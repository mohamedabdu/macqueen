//
//  childTableViewCell.swift
//  MacQueen
//
//  Created by Kareem on 5/18/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class childTableViewCell: UITableViewCell, CellProtocol {

    @IBOutlet weak var agesView: UIView!
    @IBOutlet weak var childAgesBtn: UIButton!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var childLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.setupFont()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        agesView.applyShadow(cornerRadius: 20)
    }
    var onUpdateAge: PickerDidSelectPath?
    weak var delegate: UIViewController?
    func setup() {
        childLbl.text = "\("Child".localized) \((path ?? 0)+1)"
        yearLbl.text = "\("Year".localized) 1"
        guard let scene = delegate?.controller(PickerController.self, storyboard: .PickerViewHelper) else { return }
        scene.source = [1,2,3,4,5,6,7,8,9]
        scene.titleClosure = { path in
            return (path+1).string
        }
        scene.didSelectClosure = { path in
            self.onUpdateAge?(path+1)
            self.yearLbl.text = "\("Year".localized) \(path+1)"
        }
        childAgesBtn.UIViewAction {
            self.delegate?.pushPop(vcr: scene)
        }
    }
}
