//
//  notificationsTableViewCell.swift
//  MacQueen
//
//  Created by Kareem on 5/18/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class notificationsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bodyLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.setupFont()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
