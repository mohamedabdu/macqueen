//
//  guestsNamesTableViewCell.swift
//  MacQueen
//
//  Created by Kareem on 5/20/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class guestsNamesTableViewCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var guestNameLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.setupFont()
    }
    func setup() {
        guard let model = model as? BookModel.LeadPassenger else { return }
        guestNameLbl.text = "\(model.lastName ?? "") \(model.lastName ?? "")"
    }
}
