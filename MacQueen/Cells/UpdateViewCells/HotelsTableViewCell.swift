//
//  HotelsTableViewCell.swift
//  MacQueen
//
//  Created by Kareem on 5/14/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher

class HotelsTableViewCell: UITableViewCell, CellProtocol {

    //MARK: -  @IBOutlet 
    @IBOutlet weak var hotelsView: UIView!
    @IBOutlet weak var hotelImageView: UIImageView!
    @IBOutlet weak var favoritesBtn: UIButton!
    @IBOutlet weak var hotelNameLabel: UILabel!
    @IBOutlet weak var hotelPriceLabel: UILabel!
    @IBOutlet weak var hotelRateView: CosmosView!
    
    var closureOpenDeals: HandlerView?
    var fromReservation: Bool = false
    var isComingFromFav: Bool = false
    //var closureFavorite: HandlerView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.setupFont()
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        hotelsView.applyShadow(cornerRadius: 25)
        hotelImageView.roundCorners(corners: [.topLeft, .topRight] , radius: 25)
      }
    func setup() {
        guard let model = model as? hotel else { return }
        if fromReservation {
            let url = URL(string: model.hotel?.images?.first ?? "")
            hotelImageView.kf.setImage(with: url)
            hotelNameLabel.text = model.name
            hotelPriceLabel.text = "\(model.currency ?? "")\(model.price ?? "")"
            hotelRateView.rating = model.hotel?.rate?.double() ?? 0
        } else {
            if isComingFromFav {
                let url = URL(string: model.imageFav ?? "")
                hotelImageView.kf.setImage(with: url)
            } else {
                let url = URL(string: model.image ?? "")
                hotelImageView.kf.setImage(with: url)
            }
            hotelNameLabel.text = model.hotel_name
            hotelPriceLabel.text = "SAR\(model.min_price?.string ?? "")"
            hotelRateView.rating = model.rate?.double() ?? 0
        }
        
       
    }
    @IBAction func viewDeals(_ sender: Any) {
        closureOpenDeals?()
    }
}
