//
//  HotelsCollectionViewCell.swift
//  MacQueen
//
//  Created by Kareem on 5/12/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import Cosmos
class HotelsCollectionViewCell: UICollectionViewCell, CellProtocol {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var hotelsView: UIView!
    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var hotelNamellbl: UILabel!
    @IBOutlet weak var hotelRate: CosmosView!
    
    override func awakeFromNib() {
        self.contentView.setupFont()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        // do your thing
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        hotelImage.roundCorners(corners: [.topLeft, .topRight] , radius: 25)
        hotelsView.applyShadow(cornerRadius: 25)
    }
    func setup() {
        guard let model = model as? hotel else { return }
        let url = URL(string: model.imageFav ?? "")
        hotelImage.kf.setImage(with: url)
        hotelNamellbl.text = model.hotel_name
        hotelRate.rating = model.rate?.double() ?? 0
    }
}
