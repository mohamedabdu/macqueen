//
//  DealsTableViewCell.swift
//  MacQueen
//
//  Created by Kareem on 5/18/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

protocol DealCellDelegate: class {
    func didBook(section: Int)
}
class DealsTableViewCell: UITableViewCell, CellProtocol {

    
    @IBOutlet weak var dealPriceLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var adultLbl: UILabel!
    @IBOutlet weak var refundLbl: UILabel!
    @IBOutlet weak var roomLbl: UILabel!
    @IBOutlet weak var benefitsLbl: UILabel!
    @IBOutlet weak var totalPriceView: UIView!
    @IBOutlet weak var totalPriceViewHeigh: NSLayoutConstraint!
    @IBOutlet weak var totalViewBottomSpace: NSLayoutConstraint!
    
    var row: IndexPath?
    weak var delegate: DealCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.setupFont()
    }
    
    func setup() {
        guard let model = model as? deals else { return }
        guard let row = row else { return }
        setupDealView()
        if model.rooms?.count ?? 0 == 1 {
            priceLbl.text = ""
        } else {
            priceLbl.text = "SAR\(model.rooms?[row.row].price?.string ?? "")"
        }
        adultLbl.text = model.rooms?[row.row].pax?.adults_number?.string
        if case model.rooms?[row.row].isRefundable = true {
            refundLbl.text = "\("Refundable".localized)"
        } else {
            refundLbl.text = "\("Not Refundable".localized)"
            refundLbl.textColor = .red
        }
        roomLbl.text = "\(model.rooms?[row.row].name ?? "")"
        benefitsLbl.text = "\(model.rooms?[row.row].benefits ?? "")"
        dealPriceLbl.text = "SAR\(model.price?.string ?? "")"
        
    }
    func setupDealView() {
        guard let row = row else { return }
        guard let model = model as? deals else { return }
        if (row.row+1) == model.rooms?.count ?? 0 {
            totalPriceViewHeigh.constant = 56
            totalPriceView.isHidden = false
            totalViewBottomSpace.constant = 17
        } else {
            totalPriceViewHeigh.constant = 0
            totalPriceView.isHidden = true
            totalViewBottomSpace.constant = 5
        }
    }
    @IBAction func bookNow(_ sender: Any) {
        self.delegate?.didBook(section: row?.section ?? 0)
    }
}
