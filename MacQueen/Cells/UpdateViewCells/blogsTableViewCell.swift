//
//  blogsTableViewCell.swift
//  MacQueen
//
//  Created by Kareem on 5/19/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class blogsTableViewCell: UITableViewCell, CellProtocol {

    //MARK: - @IBOutlet
    @IBOutlet weak var blogsView: UIView!
    @IBOutlet weak var blogImages: UIImageView!
    @IBOutlet weak var blogTitleLbl: UILabel!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.setupFont()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        blogImages.roundCorners(corners: [.topLeft, .topRight] , radius: 25)
        blogsView.applyShadow(cornerRadius: 25)
    }

    func setup() {
        guard let model = model as? BlogModel.Datum else { return }
        let url = URL(string: model.featuredImage ?? "")
        blogImages.kf.setImage(with: url)
        blogTitleLbl.text = model.title
    }
}
