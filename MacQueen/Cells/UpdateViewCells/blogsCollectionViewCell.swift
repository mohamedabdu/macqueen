//
//  blogsCollectionViewCell.swift
//  MacQueen
//
//  Created by Kareem on 5/12/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class blogsCollectionViewCell: UICollectionViewCell, CellProtocol {
   
    //MARK: - @IBOutlet
    @IBOutlet weak var blogsView: UIView!
    @IBOutlet weak var blogImages: UIImageView!
    @IBOutlet weak var blogTitleLbl: UILabel!
    
    override func awakeFromNib() {
        self.contentView.setupFont()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        blogImages.roundCorners(corners: [.topLeft, .topRight] , radius: 25)
        blogsView.applyShadow(cornerRadius: 25)
    }
    func setup() {
        guard let model = model as? BlogModel.Datum else { return }
        let url = URL(string: model.featuredImage ?? "")
        blogImages.kf.setImage(with: url)
        blogTitleLbl.text = model.title
    }
}
