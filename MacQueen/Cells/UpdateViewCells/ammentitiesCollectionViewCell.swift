//
//  ammentitiesCollectionViewCell.swift
//  MacQueen
//
//  Created by Kareem on 5/14/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class ammentitiesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    
    override func awakeFromNib() {
        self.contentView.setupFont()
    }
    
}
