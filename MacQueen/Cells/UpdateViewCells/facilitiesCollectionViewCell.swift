//
//  facilitiesCollectionViewCell.swift
//  MacQueen
//
//  Created by Kareem on 5/14/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class facilitiesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var facilityImage: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        self.contentView.setupFont()
    }
    
}
