//
//  RoomsTableViewCell.swift
//  MacQueen
//
//  Created by Kareem on 4/21/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class RoomsTableViewCell: SwipeTableViewCell, CellProtocol {

    //MARK:-  @IBOutlet
    @IBOutlet weak var roomLbl: UILabel!
    @IBOutlet weak var childsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var adultLbl: UILabel!
    @IBOutlet weak var minusAdultBtn: UIButton!
    @IBOutlet weak var addAdultBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var childLbl: UILabel!
    @IBOutlet weak var childsAgesTableView: UITableView! {
        didSet {
            childsAgesTableView.delegate = self
            childsAgesTableView.dataSource = self
        }
    }
    
    //MARK:- Variable
    var childArr = [Int]()
    var adults: Int = 0
    var closureUpdateChilds: (([Int]) -> Void)?
    override func layoutSubviews() {
        super.layoutSubviews()

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.setupFont()
    }
    
    func setup() {
        roomLbl.text = "\("Room".localized) \((path ?? 0)+1)"
    }
    func callDelegate() {
        var array: [Int] = []
        for item in childArr {
            array.append(item)
        }
        closureUpdateChilds?(array)
    }
    func ages() -> [Int] {
        var array: [Int] = []
        for item in childArr {
            array.append(item)
        }
        return array
    }
}


//MARK:-  TableView Delegate
extension RoomsTableViewCell : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.childArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: childTableViewCell.self, indexPath)
        cell.delegate = self.delegate as? UIViewController
        cell.onUpdateAge = { age in
            self.childArr[indexPath.row] = age
            self.callDelegate()
        }
        cell.model = ""
        cell.yearLbl.text = "\("Year".localized) \(childArr[indexPath.row])"
        return cell
    }
    

}
