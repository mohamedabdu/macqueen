//
//  popularHotelsTableViewCell.swift
//  MacQueen
//
//  Created by Kareem on 4/21/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class popularHotelsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var popularHotelsCollectionView: UICollectionView! {
        didSet {
            popularHotelsCollectionView.delegate = self
            popularHotelsCollectionView.dataSource = self
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension popularHotelsTableViewCell : UICollectionViewDataSource , UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "popularHotelsCollectionViewCell", for: indexPath) as! popularHotelsCollectionViewCell
        return cell
    }
}
