//
//  GredientBtn.swift
//  MacQueen
//
//  Created by Kareem on 4/16/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import UIKit

class GradientBtn: UIButton {
    @IBInspectable var firstColor: UIColor = UIColor.appColor(.secondary)!
    @IBInspectable var secondColor: UIColor = UIColor.appColor(.primary)!
    @IBInspectable var vertical: Bool = true
    
    lazy var gradientLayer: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [firstColor.cgColor, secondColor.cgColor]
        layer.startPoint = CGPoint.zero
        return layer
    }()
    
    //MARK: -
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        applyGradient()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        applyGradient()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        applyGradient()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientFrame()
    }
    
    //MARK: -
    
    func applyGradient() {
        updateGradientDirection()
        layer.sublayers = [gradientLayer]
    }
    
    func updateGradientFrame() {
        gradientLayer.frame = bounds
    }
    
    func updateGradientDirection() {
        gradientLayer.endPoint = vertical ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 1)
    }
    
}
class Gradientvw: UIView {
    @IBInspectable var firstColor: UIColor = UIColor.appColor(.secondary)!
    @IBInspectable var secondColor: UIColor = UIColor.appColor(.primary)!
    @IBInspectable var vertical: Bool = true
    
    lazy var gradientLayer: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [firstColor.cgColor, secondColor.cgColor]
        layer.startPoint = CGPoint.zero
        return layer
    }()
    
    //MARK: -
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        applyGradient()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        applyGradient()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        applyGradient()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientFrame()
    }
    
    //MARK: -
    
    func applyGradient() {
        updateGradientDirection()
        layer.sublayers = [gradientLayer]
    }
    
    func updateGradientFrame() {
        gradientLayer.frame = bounds
    }
    
    func updateGradientDirection() {
        gradientLayer.endPoint = vertical ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 1)
    }
    
}



extension UIColor {
    static func appColor(_ name: AssetsColor) -> UIColor? {
        switch name {
        case .primary:
            return UIColor(named: "Primary")
        case .secondary:
            return UIColor(named: "Secondary")
        }
    }
}


enum AssetsColor {
    case primary
    case secondary
    
}
