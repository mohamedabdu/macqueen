//
//  UitableViewExtension.swift
//  MacQueen
//
//  Created by Kareem on 1/22/20.
//  Copyright © 2020 Mahmoud Fares. All rights reserved.
//

import Foundation
import UIKit



extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

extension UITableView {
    @IBInspectable
    var hideSeparatorForEmptyCells: Bool {
        set {
            tableFooterView = newValue ? UIView() : nil
        }
        get {
            return tableFooterView == nil
        }
    }
}

// Animate TableView
extension UITableView {
    func reloadWithAnimation() {
        self.reloadData()
        let cells = self.visibleCells
        let tableHeight: CGFloat = self.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for c in cells {
            let cell: UITableViewCell = c as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.beginFromCurrentState], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}

extension UICollectionView {
    func reloadWithAnimation() {
        self.reloadData()
        let cells = self.visibleCells
        let tableHeight: CGFloat = self.bounds.size.height
        for i in cells {
            let cell: UICollectionViewCell = i as UICollectionViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for c in cells {
            let cell: UICollectionViewCell = c as UICollectionViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.beginFromCurrentState], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}
/////////////////////////////////////////////
typealias HandlerView = (() -> Void)
internal var handlerActions: [UIView: HandlerView] = [:]
extension UIView {
    internal static func emptyHanlder() {
        handlerActions = [:]
    }
    internal func emptyHanlder() {
        handlerActions = [:]
    }
    internal func UIViewAction(selector: @escaping HandlerView) {
        self.isUserInteractionEnabled = true
        actionHandleBlock(action: selector)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.triggerActionHandleBlock))
        self.addGestureRecognizer(tap)
    }
    internal func actionHandleBlock(action:(() -> Void)? = nil) {
        if action != nil {
            handlerActions[self] = action
        } else {
            guard let action = handlerActions[self] else { return }
            action()
        }
    }
    
    @objc func triggerActionHandleBlock() {
        self.actionHandleBlock()
    }
}
