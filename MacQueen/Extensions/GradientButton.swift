//
//  GradientButton.swift
//  Gofer
//
//  Created by Mahmoud Fares on 11/2/19.
//  Copyright © 2019 Vignesh Palanivel. All rights reserved.
//

import UIKit
@IBDesignable
class GradientButton: UIButton {
    let gradientLayer = CAGradientLayer()

    @IBInspectable
    var topGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    override func layoutSubviews() {
        super .layoutSubviews()
        gradientLayer.frame = self.bounds
    }
    @IBInspectable
    var bottomGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    @IBInspectable
    var LeftGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: LeftGradientColor, bottomGradientColor: RightGradientColor)
        }
    }
    
    @IBInspectable
    var RightGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: LeftGradientColor, bottomGradientColor: RightGradientColor)
        }
    }


    @IBInspectable
    var gradianConrner: CGFloat = 0 {
        didSet {
            gradientLayer.cornerRadius = gradianConrner
            gradientLayer.masksToBounds = true
        }
    }

    private func setGradient(topGradientColor: UIColor?, bottomGradientColor: UIColor?) {
        if let topGradientColor = topGradientColor, let bottomGradientColor = bottomGradientColor {
            gradientLayer.frame = bounds
            gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = layer.cornerRadius
            layer.insertSublayer(gradientLayer, at: 0)
            
        } else {
            gradientLayer.removeFromSuperlayer()
        }
    }

}
@IBDesignable
class GradientView: UIView {
    let gradientLayer = CAGradientLayer()

    @IBInspectable
    var topGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    override func layoutSubviews() {
        super .layoutSubviews()
        gradientLayer.frame = self.bounds
    }
    @IBInspectable
    var bottomGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    @IBInspectable
    var gradianConrner: CGFloat = 0 {
        didSet {
            gradientLayer.cornerRadius = gradianConrner
            gradientLayer.masksToBounds = true
        }
    }

    private func setGradient(topGradientColor: UIColor?, bottomGradientColor: UIColor?) {
        if let topGradientColor = topGradientColor, let bottomGradientColor = bottomGradientColor {
            gradientLayer.frame = bounds
            gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = layer.cornerRadius
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.removeFromSuperlayer()
        }
    }

}


@IBDesignable
open class GradientVieww: UIView {
    @IBInspectable
    public var startColor: UIColor = .white {
        didSet {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            setNeedsDisplay()
        }
    }
    @IBInspectable
    public var endColor: UIColor = .white {
        didSet {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            setNeedsDisplay()
        }
    }

    private lazy var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [self.startColor.cgColor, self.endColor.cgColor]
        return gradientLayer
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.insertSublayer(gradientLayer, at: 0)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.insertSublayer(gradientLayer, at: 0)
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
}



@IBDesignable class GradientViewww: UIView {

@IBInspectable
public var firstColor: UIColor = .white {
    didSet {
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        setNeedsDisplay()
    }
}

@IBInspectable
public var secondColor: UIColor = .white {
    didSet {
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        setNeedsDisplay()
    }
}
@IBInspectable var vertical: Bool = true {
    didSet {
        updateGradientDirection()
    }
}

lazy var gradientLayer: CAGradientLayer = {
    let layer = CAGradientLayer()
    layer.colors = [firstColor.cgColor, secondColor.cgColor]
    layer.startPoint = CGPoint.zero
    return layer
}()

required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    applyGradient()
}

override init(frame: CGRect) {
    super.init(frame: frame)
    applyGradient()
}

override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    applyGradient()
}

    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientFrame()
    }

    // MARK: - Helper

    func applyGradient() {
        updateGradientDirection()
        layer.sublayers = [gradientLayer]
    }

    func updateGradientFrame() {
        gradientLayer.frame = bounds
    }

    func updateGradientDirection() {
        gradientLayer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
    }
}

