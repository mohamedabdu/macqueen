//
//  UiTextFieldExtension.swift
//  MacQueen
//
//  Created by Kareem on 2/19/20.
//  Copyright © 2020 Mahmoud Fares. All rights reserved.
//

import Foundation
import UIKit
import SwiftMessages

@IBDesignable
extension UITextField {
    
    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }
    
    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
}




extension UIViewController: UIGestureRecognizerDelegate {
    
    
    
    
    
    
    // MARK: - Transparent With Nav Bar
    func transparentNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    
    // MARK: - Nav Bar Title Image
    func setTitleImg(_ img: UIImage = #imageLiteral(resourceName: "Logo")) {
        let imageView: UIImageView = UIImageView(image: img)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
    }
    
   
    
    // MARK: - Back And Dismiss
    @IBAction func backClicked(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func setRoomHomeClicked(_ sender: Any) {
        RootWindowController.setRootWindowForHome()
    }

    
    @IBAction func backToRootClicked(_ sender: Any) {
       self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func dismissClicked(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: true)
    }
    
    @IBAction func dismisstoRootClicked(_ sender: Any) {
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openSideMenu(_ sender: UIBarButtonItem) {
//        if self.revealViewController() != nil {
//            self.revealViewController().rearViewRevealWidth = self.view.frame.width - 100
//            revealViewController().rearViewController = nil
//            sender.action = #selector(SWRevealViewController.rightRevealToggle(_:))
//        }
//        sender.target = self.revealViewController()
//    self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
      }

    
    
    // MARK: - Pop back n viewcontroller
    func popBack(_ numOfVCs: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < numOfVCs else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - numOfVCs], animated: true)
                return
            }
        }
    }
    
    // MARK: - Pop back to specific viewcontroller
    func popBack<T: UIViewController>(toControllerType: T.Type) {
        if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: toControllerType) {
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                    break
                }
            }
        }
    }
    
    
    // MARK: - Add Or Remove Pop Gesture
    func addPopGesture() {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
//        SettingsViewController.isEnabled = true
    }
    
    func removePopGesture() {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    // MARK: - Set Activity Indicator Play Or Stop
    func setActivityIndicator(activity: UIActivityIndicatorView, play: Bool) {
        if play {
            activity.startAnimating()
            self.view.isUserInteractionEnabled = false
        }else {
            activity.stopAnimating()
            self.view.isUserInteractionEnabled = true
        }
    }
    
   
    // MARK: - Show Alert
    func showAlertWith(title: String? = nil, msg: String? = nil, type: Theme = .warning, layout: MessageView.Layout = .cardView) {
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: layout)
        
        // Theme message elements with the warning style.
        
        view.configureTheme(type)
        view.button?.isHidden = true

        if type == .warning {
            view.configureTheme(backgroundColor: .red, foregroundColor: .white)
        }
        
        // Add a drop shadow.
//        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        
        view.configureContent(title: title ?? "", body: msg ?? "", iconText: "")

        // Show the message.
        SwiftMessages.show(view: view)
    }
    
    func showAlertWithAction(alertTitle: String, alertMsg: String, buttonTitle: String, cancelBtnTitle: String? = nil, action: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: .alert)
        alert.view.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        let action = UIAlertAction(title: buttonTitle, style: .default, handler: action)
        var cancelAction = UIAlertAction()
        if cancelBtnTitle == nil {
            cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        }else {
            cancelAction = UIAlertAction(title: cancelBtnTitle, style: .cancel, handler: nil)
        }
        alert.addAction(action)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Check For Empty Strings
    func isNotEmptyString(text: String, withAlertMessage message: String) -> Bool{
        if text == ""{
            showAlertWith(msg: message)
            return false
        }
        else{
            return true
        }
    }
    
    // MARK: - Check Confirmation
    func isTextsIdentical(text1: String, text2: String, withAlertMessage message: String) -> Bool{
        if text1 == text2 {
            return true
        }else {
            showAlertWith(msg: message)
            return false
        }
    }
    
    // MARK: - Email And Phone Validation
    func isEmailValid(emailString: String) -> Bool{
        let regExPattern = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", argumentArray: [regExPattern])
        if !emailTest.evaluate(with: emailString){
            showAlertWith(msg: "Please, Insert Email Correctly")

        }
        return emailTest.evaluate(with: emailString)
    }
   
    func isPhoneNumberValid(phoneNumber: String) -> Bool{
        var isValid = true
        let nameValidation = phoneNumber.replacingOccurrences(of: " ", with: "")
        
        if (nameValidation.count < 9 || nameValidation.count > 14) { //check length limitation
            isValid = false
        }
        
        if !isValid{
            showAlertWith(msg: "Please, Insert Phone Number Correctly")
        }
        
        return isValid
    }
}


