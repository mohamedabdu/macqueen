//
//  textFieldImage.swift
//  MacQueen
//
//  Created by Mahmoud Fares on 12/24/19.
//  Copyright © 2019 Mahmoud Fares. All rights reserved.
//

import UIKit
@IBDesignable
class TextFieldImage: UITextField {

    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    @IBInspectable var topPadding: CGFloat = 0
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }

    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }

    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }

    @IBInspectable var PlaceholderColor: UIColor = UIColor.lightGray {
        didSet {
            updateView()

        }
    }

    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRec = super.leftViewRect(forBounds: bounds)
        textRec.origin.x += leftPadding
        return textRec
    }

    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRec = super.rightViewRect(forBounds: bounds)
        textRec.origin.x -= rightPadding
        return textRec
    }

    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftView = nil
            leftViewMode = UITextField.ViewMode.never
        }

        if let rightImage = rightImage {
            rightViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = rightImage
            imageView.tintColor = color
            rightView = imageView
        } else {
            rightView = nil
            rightViewMode = UITextField.ViewMode.never
        }

        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowRadius = 2
        layer.masksToBounds = false
        layer.shadowOpacity = 1.0
        // set backgroundColor in order to cover the shadow inside the bounds
        layer.backgroundColor = UIColor.white.cgColor
    }

}

//
//extension UIView {
//    func dropShadow(scale: Bool = true) {
//        layer.masksToBounds = false
//        layer.shadowColor = UIColor.black.cgColor
//        layer.shadowOpacity = 0.2
//        layer.shadowOffset = .zero
//        layer.shadowRadius = 1
//        layer.shouldRasterize = true
//        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
//    }
//}
//
