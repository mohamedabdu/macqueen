//
//  CustomTabbarVC.swift
//  AswaqElnokhba
//
//  Created by apple on 5/8/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class CustomTabbarVC: UITabBarController {
    
    var arrViewController: [UIViewController] = []
    var selectedTitleColor: UIColor = .black
    var normalTitleColor: UIColor = .gray
    var fontTitle: UIFont = UIFont.systemFont(ofSize: 12)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.layer.cornerRadius = 10
        self.tabBar.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    
    func addtabbrItem(atIndex: Int, viewContrller: UIViewController, andTitle: String, andImgName: String, andSeletedImg: String, isHideTitle: Bool, imageInsets: UIEdgeInsets, titlePos: UIOffset)  {
        arrViewController.insert(viewContrller, at: atIndex)
        self.viewControllers = arrViewController
        
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.tintColor = UIColor.black
        
        let tabbrCus = self.tabBar
        let tabbarItemCus = tabbrCus.items?[atIndex]
        if isHideTitle {
            tabbarItemCus?.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 50)
        }else{
            tabbarItemCus?.title = andTitle
            tabbarItemCus?.titlePositionAdjustment = titlePos
        }
        tabbarItemCus?.imageInsets = imageInsets
        let img = UIImage(imageLiteralResourceName: andImgName).withRenderingMode(.alwaysOriginal)
        let imgSelected = UIImage(imageLiteralResourceName: andSeletedImg).withRenderingMode(.alwaysOriginal)
        tabbarItemCus?.image = img
//        if !ADevice.hasSafeArea{
//             tabbarItemCus?.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -2 , right: 0)
//        }
        tabbarItemCus?.selectedImage = imgSelected
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: normalTitleColor,
                                                          NSAttributedString.Key.font: fontTitle,], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedTitleColor], for: .selected)
    }
    
    
//    func generateNavigation(vc:UIViewController)->CNavigation{
//        let navigation = CNavigation(rootViewController: vc)
//        let Image = UIImage(named: "back")
//        
//        navigation.navigationBar.backIndicatorImage = Image
//        navigation.navigationBar.backIndicatorTransitionMaskImage = Image
//        navigation.navigationBar.tintColor = UIColor.black
//        navigation.navigationBar.titleTextAttributes =  [NSAttributedString.Key.font:UIFont.NavigationTextFont,NSAttributedString.Key.foregroundColor:UIColor.NavigationTextColor]
//        
//        return navigation
//    }
    
}

