//
//  SavedCardCell.swift
//  MacQueen
//
//  Created by M.abdu on 6/10/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class SavedCardCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var visaImage: UIImageView!
    @IBOutlet weak var cardNumLbl: UILabel!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.applyShadow(cornerRadius: 30)
        // Initialization code
        self.contentView.setupFont()
    }
    func setup() {
        guard let model = model as? String else { return }
        cardNumLbl.text = model
    }
}
