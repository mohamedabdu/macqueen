//
//  RoomPassengerCell.swift
//  MacQueen
//
//  Created by M.abdu on 6/8/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class RoomPassengerCell: UITableViewCell, CellProtocol, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    
    @IBOutlet weak var roomTitle: UILabel!
    @IBOutlet weak var firstnameTxf: UITextField!
    @IBOutlet weak var lastnameTxf: UITextField!
    @IBOutlet weak var titleTxf: UITextField!
    
    let thePicker = UIPickerView()
    let pickerData = ["Mr", "Mrs"]

    override func awakeFromNib() {
        self.contentView.setupFont()
    }
    
    var roomModel: RoomsModel?
    func setup() {
        roomTitle.text = "\("Lead".localized) \("Passengers".localized) \((path ?? 1)+2)"
        thePicker.delegate = self
        titleTxf.inputView = thePicker
        titleTxf.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = pickerData[safe: thePicker.selectedRow(inComponent: 0)]
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
}

