//
//  ImageCollectionCell.swift
//  MacQueen
//
//  Created by M.abdu on 6/15/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var gallaryImages: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setup() {
        
    }
}
