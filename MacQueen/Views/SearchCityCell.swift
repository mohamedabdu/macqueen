//
//  SearchCityCell.swift
//  MacQueen
//
//  Created by M.abdu on 6/4/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class SearchCityCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        self.contentView.setupFont()
    }
    func setup() {
        guard let model = model as? cities else { return setupHotel() }
        titleLbl.text = "\( model.name ?? "") - \(model.country_name ?? "")"
    }
    func setupHotel() {
        guard let model = model as? hotels else { return }
        titleLbl.text = "\( model.name ?? "") - \(model.country_name ?? "")"
    }
}
