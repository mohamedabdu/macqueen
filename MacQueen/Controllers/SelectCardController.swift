//
//  PaymentConfirmedViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/20/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import Frames
import PassKit
class SelectCardController: UIViewController {
    @IBOutlet weak var savedCardTbl: UITableView! {
        didSet {
            savedCardTbl.delegate = self
            savedCardTbl.dataSource = self
        }
    }
    @IBOutlet weak var cardsHeight: NSLayoutConstraint!
    
    var checkoutAPIClient: CheckoutAPIClient?
    let merchantId = "merchant.com.zeidex.MacQueen"
    let merchantIdSandbox = "merchant.co.macqueen.sandbox"
    var paramters: [String: Any] = [:]
    var hotel: hotel?
    var cards: [SavedCardModel.Data] = []
    var deal: deals?
    var cardNumber: String?
    //MARK:-  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        getPaymentToken()
        getSavedCard()
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        cardsHeight.constant = savedCardTbl.contentSize.height

    }

    func getPaymentToken() {
        WebServiceFactory().makePaymentSdk { (error, data, code) in
            self.checkoutAPIClient = CheckoutAPIClient(publicKey: data?.data?.public_key ?? "",
                                                  environment: .sandbox)
        }
    }
    func getSavedCard() {
        WebServiceFactory().getSavedCards { (error, data, code) in
            if data != nil {
                self.cards.append(contentsOf: data?.data ?? [])
                self.savedCardTbl.reloadData {
                    self.cardsHeight.constant = self.savedCardTbl.contentSize.height
                }
            }
        }
    }
    func pay() {
        startLoading()
        WebServiceFactory().book(params: paramters, hotelId: hotel?.hotel_id ?? 0, provider: hotel?.provider ?? "") { (error, data, code) in
            self.stopLoading()
            if code > 201 {
                self.showAlert(title: "Alert".localized, message: "Payment Can't be processed".localized)
            }
            let storyBoard = UIStoryboard.init(name: "Payment", bundle: nil)
            let scene = storyBoard.instantiateViewController(identifier: "PaymentConfirmedViewController") as! PaymentConfirmedViewController
            scene.book = data
            scene.hotel = self.hotel
            scene.deal = self.deal
            scene.cardNumber = self.cardNumber
            scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
            self.present(scene , animated: true, completion: nil)
        }
        
    }
    @IBAction func addNewCard(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Updated", bundle: nil)
        let scene = storyBoard.instantiateViewController(identifier: "ApplePayController") as! ApplePayController
        scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        pushPop(vcr: scene)
        return
//        let storyBoard = UIStoryboard.init(name: "Payment", bundle: nil)
//        let scene = storyBoard.instantiateViewController(identifier: "AddCardController") as! AddCardController
//        scene.delegate = self
//        scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
//        pushPop(vcr: scene)
    }
}
extension SelectCardController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: SavedCardCell.self, indexPath)
        cell.model = "**** **** **** \(cards[indexPath.row].last4 ?? "")"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard.init(name: "Payment", bundle: nil)
        let scene = storyBoard.instantiateViewController(identifier: "CheckCvvController") as! CheckCvvController
        scene.delegate = self
        scene.card = cards[indexPath.row]
        cardNumber = "**** **** **** \(cards[indexPath.row].last4 ?? "")"
        scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        pushPop(vcr: scene)
    }
}
extension SelectCardController: AddCardDelegate {
    func didAddCard(card: CardModel) {
        cardNumber = card.number
        let cardToken = getCardTokenRequest(card: card)
        print(cardToken)
        checkoutAPIClient?.createCardToken(card: cardToken, successHandler: { token in
            let payment = PaymentModel(token: token.token, type: "token")
            self.paramters["payment"] = payment.dec
            self.pay()
        }, errorHandler: { error in
            self.showAlert(title: "", message: error.errorType)
        })
    }
    func didCheckedCvv(card: CardModel) {
        let payment = PaymentModel(id: card.id ?? "", cvv: card.cvv ?? "", type: "id")
        self.paramters["payment"] = payment.decID
        self.pay()
    }
    private func getCardTokenRequest(card: CardModel) -> CkoCardTokenRequest {
        let cardUtils = CardUtils()
        let cardNumber = cardUtils.standardize(cardNumber: card.number ?? "")
        return CkoCardTokenRequest(number: cardNumber, expiryMonth: card.month ?? "", expiryYear: card.year ?? "", cvv: card.cvv ?? "")
    }
}
// MARK:  Apple Pay
extension SelectCardController: PKPaymentAuthorizationViewControllerDelegate {
    
    @IBAction func onTouchApplePayButton() {
        
        createPaymentRequest()
        
//        if PKPaymentAuthorizationController.canMakePayments() {
//            let paymentRequest = createPaymentRequest()
//            if let applePayViewController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest) {
//                applePayViewController.delegate = self
//                self.present(applePayViewController, animated: true)
//            }
//        }
    }
    
    private func createPaymentRequest() {
        let paymentRequest = PKPaymentRequest()
        
        
        let paymentNetwork = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetwork, capabilities: .capability3DS) {
            paymentRequest.countryCode = "SA"
            paymentRequest.currencyCode = "SAR"
            paymentRequest.merchantIdentifier = merchantIdSandbox
            paymentRequest.supportedNetworks = paymentNetwork
            paymentRequest.merchantCapabilities = PKMerchantCapability.capability3DS
            paymentRequest.paymentSummaryItems = [
                PKPaymentSummaryItem(label: "Deal".localized, amount: NSDecimalNumber(value: (deal?.price ?? 0)) ),
            ]
            
//            let sameDay = PKShippingMethod(label: "sameDay", amount: 9.9)
//            sameDay.detail = "ittemsad weasd d"
//            sameDay.identifier = "sameDay"
//            paymentRequest.shippingMethods = [sameDay]
            guard let applePayVc = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest) else { return }
            applePayVc.delegate = self
            self.present(applePayVc , animated: true, completion: nil)
            
            
        }
        
        
    }
    func payWithApple(token: String) {
        let payment = PaymentModel(token: token, type: "token")
        self.paramters["payment"] = payment.dec
        self.pay()
    }

    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
    }

    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController,
                                            didAuthorizePayment payment: PKPayment,
                                            handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {

        print(payment.token)
        checkoutAPIClient?.createApplePayToken(paymentData: payment.token.paymentData, successHandler: { applePayToken in
            print(applePayToken)
            self.payWithApple(token: applePayToken.token)
            completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
        }, errorHandler: { error in
            completion(PKPaymentAuthorizationResult(status: .failure, errors: nil))
            print(error)
        })
    }
    
}
