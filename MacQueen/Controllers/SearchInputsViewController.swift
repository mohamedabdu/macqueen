//
//  SearchInputsViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/17/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import CalendarDateRangePickerViewController


class SearchInputsViewController: UIViewController {
    enum SearchType: String {
        case city
        case hotel
    }
    //MARK: -  @IBOutlet
    @IBOutlet weak var recentSearchTbl: UITableView!
    @IBOutlet weak var roomsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet var searchInputViews: [UIView]!
    //MARK: -  Variables
    let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    var selectedDates = [String]()
    var type: SearchType = .city
    var city: cities?
    var hotel: hotels?
    var adultCount: Int = 1
    var rooms: [RoomsModel] = []
    var checkoutDate: String?
    var checkinDate: String?
    var recentSearch: RecentSearchModel?
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
    }
    override func viewDidLayoutSubviews() {
     super.viewDidLayoutSubviews()
        dropShadowForAllViews()
     }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchSaerch()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    func setup() {
        switch type {
            case .city:
                cityLabel.text = "\(city?.name ?? "") - \(city?.country_name ?? "")"
            case .hotel:
                cityLabel.text = "\(hotel?.name ?? "") - \(hotel?.country_name ?? "")"
            
        }
        rooms.append(RoomsModel(adultsNumber: 1, childNumber: 0, children_ages: []))
        roomsLabel.text = "\(rooms.count) \("Rooms".localized), \(adultCount) \("Adults".localized), \(0) \("Childs".localized)"

        let todayDate = Date()
        guard let tomorrowDate = Calendar.current.date(byAdding: .day , value: 1 , to: todayDate) else { return }
        didTapDoneWithDateRange(startDate: todayDate, endDate: tomorrowDate)
    }
    //MARK:-  @IBAction
    
    @IBAction func findHotels(_ sender: Any) {
        if type == .city {
            SearchInputsViewController.storeSearch(word: city?.name)
        } else {
            SearchInputsViewController.storeSearch(word: hotel?.name, type: .hotel)
        }
        let hotelsListViewController = storyBoard.instantiateViewController(identifier: "HotelsListViewController") as! HotelsListViewController
        hotelsListViewController.type = type
        hotelsListViewController.city = city
        hotelsListViewController.hotel = hotel
        hotelsListViewController.date = dateLabel.text
        hotelsListViewController.checkOutDate = checkoutDate
        hotelsListViewController.checkInDate = checkinDate
        hotelsListViewController.rooms = rooms
        hotelsListViewController.closureSearchInput = {
            self.fetchSaerch()
        }
        hotelsListViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(hotelsListViewController , animated: true, completion: nil)
    }
    @IBAction func searchClicked(_ sender: UIButton) {
        let searchViewController = storyBoard.instantiateViewController(identifier: "SearchViewController") as! SearchViewController
        searchViewController.delegate = self
        searchViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(searchViewController , animated: true, completion: nil)
    }
    @IBAction func calenderClicked(_ sender: UIButton) {
        self.setupCalender()
    }
    @IBAction func roomsClicked(_ sender: UIButton) {
        let roomsViewController = storyBoard.instantiateViewController(identifier: "RoomsViewController") as! RoomsViewController
        roomsViewController.delegate = self
        roomsViewController.rooms = rooms
        roomsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(roomsViewController , animated: true, completion: nil)
    }
    
    //MARK:- Methods
    
    private func dropShadowForAllViews(){
        for view in self.searchInputViews {
            view.applyShadow(cornerRadius: 20)
        }
    }
    
    private func setupCalender() {
        let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        dateRangePickerViewController.minimumDate = Calendar.current.date(byAdding: .day , value: 0 , to: Date())
        
        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 100 , to: dateRangePickerViewController.minimumDate)
        let navController = UINavigationController(rootViewController: dateRangePickerViewController)
        navController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(navController , animated: true, completion: nil)
          }
}


//MARK: - Calender Delegate
extension SearchInputsViewController : CalendarDateRangePickerViewControllerDelegate {
    func didTapCancel() {
          self.dismiss(animated: true, completion: nil)
      }

      func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        selectedDates.removeAll()
        let fromDate = formatter.string(from: startDate)
        let toDate = formatter.string(from: endDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let dateObj = dateFormatter.date(from: fromDate) else { return }
        checkinDate = dateFormatter.string(from: dateObj)
        let format = DateFormatter()
        format.dateFormat = "E , MMM d"
        let fromDateStr = format.string(from: dateObj)
        guard let toDateObj = dateFormatter.date(from: toDate) else { return }
        checkoutDate = dateFormatter.string(from: toDateObj)
        let toDateStr = format.string(from: toDateObj)
        self.dateLabel.text = fromDateStr + "  -  " + toDateStr
        self.dismiss(animated: true, completion: nil)
      }
}
extension SearchInputsViewController: SearchDelegate {
    func didSelectCity(city: cities) {
        type = .city
        self.city = city
        cityLabel.text = self.city?.name
    }
    
    func didSelectHotel(hotel: hotels) {
        type = .hotel
        self.hotel = hotel
        cityLabel.text = self.hotel?.name
    }
}
extension SearchInputsViewController: RoomsDelegate {
    func didSelectRooms(rooms: [RoomsModel]) {
        roomsLabel.text = ""
        self.rooms = rooms
        var adultCount = 0
        var childCount = 0
        for item in rooms {
            adultCount += item.adults
            childCount += item.children
        }
        self.adultCount = adultCount
        roomsLabel.text = "\(rooms.count) \("Rooms".localized), \(adultCount) \("Adults".localized), \(childCount) \("Childs".localized)"

    }
}
extension SearchInputsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentSearch?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.cell(type: SearchCityCell.self, indexPath)
        cell.titleLbl.text = recentSearch?.data?[indexPath.row].keyword
        return cell
    }
}
// MARK:  Recent
extension SearchInputsViewController {
    func fetchSaerch() {
        recentSearchTbl.delegate = self
        recentSearchTbl.dataSource = self
        startLoading()
        WebServiceFactory().recentSearch { (error, data, code) in
            self.stopLoading()
            if data != nil {
                self.recentSearch = data
                self.recentSearchTbl.reloadData()
            }
        }
    }
    static func storeSearch(word: String?, type: SearchInputsViewController.SearchType = .city) {
        var paramters: [String: Any] = [:]
        paramters["keyword"] = word
        paramters["device_token"] = UserDefaults.standard.string(forKey: "DEVICE_TOKEN") ?? ""
        paramters["meta[type]"] = type.rawValue
        WebServiceFactory().storeSearch(params: paramters) { (error, data, code) in
        }
    }
}
