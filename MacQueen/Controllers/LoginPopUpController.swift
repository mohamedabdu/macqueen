//
//  PaymentConfirmedViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/20/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import Frames
class LoginPopUpController: UIViewController {
   
    weak var delegate: UIViewController?
    var backToHome: Bool = false
    //MARK:-  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
    }

    @IBAction func login(_ sender: Any) {
        RootWindowController.setRootWindowForLogin()
    }
    @IBAction func signUp(_ sender: Any) {
        RootWindowController.setRootWindowForRegister()
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if self.backToHome {
                self.delegate?.tabBarController?.selectedIndex = 0
            }
        })
    }
    
}
