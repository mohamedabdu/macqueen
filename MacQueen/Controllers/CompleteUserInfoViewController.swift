//
//  CompleteUserInfoViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/17/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class CompleteUserInfoViewController: UIViewController {

    //MARK:-  @IBOutlet
    @IBOutlet var completeUserInfoViews: [UIView]!
    @IBOutlet weak var firstnameTxf: UITextField!
    @IBOutlet weak var lastnameTxf: UITextField!
    @IBOutlet weak var emailTxf: UITextField!
    @IBOutlet weak var passwordTxf: UITextField!
    
    var isOTPEmail: Bool = false
    var mobile: String?
    //MARK:-  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
    }
    
    override func viewDidLayoutSubviews() {
     super.viewDidLayoutSubviews()
        dropShadowForAllViews()
     }
    //MARK:- Methods
    private func dropShadowForAllViews() {
        for view in self.completeUserInfoViews {
            view.applyShadow(cornerRadius: 30)
        }
    }
    func setup() {
        if mobile != nil {
            emailTxf.text = mobile
            
        }
    }
    func validate(txfs: [UITextField]) -> Bool {
        var validate = true
        txfs.forEach { (item) in
            if case item.text?.isEmpty = true {
                item.attributedPlaceholder = NSAttributedString(string: item.placeholder ?? "",
                                                                attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
                validate = false
            }
        }
        return validate
    }
    func setupShowBtn(_ sender: UIButton) {
        var text = sender.title(for: .normal)
        if text == "Show".localized {
            text = "Hide".localized
        } else {
            text = "Show".localized
        }
        sender.setTitle(text, for: .normal)
    }
    @IBAction func showPassword(_ sender: UIButton) {
        setupShowBtn(sender)
        passwordTxf.isSecureTextEntry = !passwordTxf.isSecureTextEntry
    }
    @IBAction func register(_ sender: Any) {
        var paramters: [String: Any] = [:]

        if !validate(txfs: [emailTxf, firstnameTxf, lastnameTxf, passwordTxf]) {
            return
        }
        if case emailTxf.text?.isEmail = true {
            paramters["email"] = emailTxf.text
            isOTPEmail = true
        } else {
            paramters["mobile"] = emailTxf.text
            isOTPEmail = false
        }
        paramters["first_name"] = firstnameTxf.text
        paramters["last_name"] = lastnameTxf.text
        paramters["password"] = passwordTxf.text
        paramters["password_confirmation"] = passwordTxf.text
        if let devicetoken = UserStatus.firebaseToken {
            paramters["device_token"] = devicetoken
        } else {
            paramters["device_token"] = "nil"
        }
        startLoading()
        WebServiceFactory().Register(paramters: paramters) { (error, user, code) in
            self.stopLoading()
            if error != nil {
                self.showAlert(title: "Alert".localized, message: error?.eMsg)
            } else {
                let scene = self.controller(OTPViewController.self, storyboard: .auth)
                scene.forEmail = self.isOTPEmail
                scene.user = user
                scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
                self.present(scene, animated: true, completion: nil)
            }
            
        }
        
    }
}
