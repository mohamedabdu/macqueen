//
//  LoginViewController.swift
//  MacQueen
//
//  Created by Kareem on 4/16/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import ActionPicker

class ChangePasswordController: UIViewController {

    
    //MARK:- @IBOutlet
    @IBOutlet var loginViews: [UIView]!
    @IBOutlet weak var passwordTxf: UITextField!
    @IBOutlet weak var newPassTxf: UITextField!
    @IBOutlet weak var confirmPassTxf: UITextField!
    
    var email: String?
    var code: String?
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)

    }
    override func viewDidLayoutSubviews() {
     super.viewDidLayoutSubviews()
        dropShadowForAllViews()
     }
    //MARK:- Methods
    
    private func dropShadowForAllViews(){
        for view in self.loginViews {
            view.applyShadow(cornerRadius: 30)
        }
    }
    func validate(txfs: [UITextField]) -> Bool {
        var validate = true
        txfs.forEach { (item) in
            if case item.text?.isEmpty = true {
                item.attributedPlaceholder = NSAttributedString(string: item.placeholder ?? "",
                                                                attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
                validate = false
            }
        }
        return validate
    }
    
    func setupShowBtn(_ sender: UIButton) {
        var text = sender.title(for: .normal)
        if text == "Show".localized {
            text = "Hide".localized
        } else {
            text = "Show".localized
        }
        sender.setTitle(text, for: .normal)
    }
    @IBAction func showPass(_ sender: UIButton) {
        setupShowBtn(sender)
        passwordTxf.isSecureTextEntry = !passwordTxf.isSecureTextEntry
    }
    @IBAction func showNewPass(_ sender: UIButton) {
        setupShowBtn(sender)
        newPassTxf.isSecureTextEntry = !newPassTxf.isSecureTextEntry
    }
    @IBAction func showConfirm(_ sender: UIButton) {
        setupShowBtn(sender)
        confirmPassTxf.isSecureTextEntry = !confirmPassTxf.isSecureTextEntry
    }
    @IBAction func save(_ sender: Any) {
        if !validate(txfs: [passwordTxf, newPassTxf, confirmPassTxf]) {
            return
        }
        if newPassTxf.text != confirmPassTxf.text {
            showAlert(title: "Alert".localized, message: "Password doesn't match".localized)
            return
        }
        var paramters: [String: Any] = [:]
        paramters["old_password"] = passwordTxf.text
        paramters["password"] = newPassTxf.text
        paramters["password_confirmation"] = confirmPassTxf.text
        startLoading()
        WebServiceFactory().ChangePassword(paramters: paramters) { (error, user, code) in
            self.stopLoading()
            if error != nil {
                self.showAlert(title: "Alert".localized, message: error?.eMsg)
            } else {
                self.makeAlert("Successfully updated".localized) {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        }
    }
  
}
