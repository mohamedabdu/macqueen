//
//  filterViewController.swift
//  MacQueen
//
//  Created by Kareem on 4/21/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import Cosmos
protocol FilterDelegate: class {
    func didFilter(ammenties: [Int]?, types: [Int]?, travel: [Int]?, rate: Double?, minPrice: Int?, maxPrice: Int?)
    func didReset()
}
protocol FilterDataSource: class {
    func rangePrices() -> (Int, Int)
    func filterModel() -> FilterModel?
}
class FilterViewController: UIViewController {
    
    //MARK:- @IBOutlet
    @IBOutlet weak var maxPriceLbl: UILabel!
    @IBOutlet weak var maxPriceView: UIView!
    @IBOutlet weak var minPriceLbl: UILabel!
    @IBOutlet weak var minPriceView: UIView!
    @IBOutlet weak var filterTableView: UITableView!
        {
        didSet {
            filterTableView.dataSource = self
            filterTableView.delegate = self
        }
    }
    @IBOutlet weak var rangeSlide: RangeSeekSlider!
    @IBOutlet weak var rateView: CosmosView!
    
    //MARK:- Variables
    var sectionArr = ["Hotel type".localized , "Ammentities".localized , "Travel type".localized]
    var types: [hotelDataType] = []
    var ammenties: [amentities] = []
    var travelType: [travelStylesData] = []
    var ammentiesSelected: [Int] = []
    var typesSelected: [Int] = []
    var travelSelected: [Int] = []
    var rate: Double?
    weak var delegate: FilterDelegate?
    weak var dataSource: FilterDataSource?
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        setupOldFilter()
    }
    func setup() {
        let prices = dataSource?.rangePrices()
        rateView.rating = 0
        rangeSlide.minValue = (prices?.0 ?? 0).cgFloat
        rangeSlide.maxValue = (prices?.1 ?? 0).cgFloat
        rangeSlide.selectedMaxValue = (prices?.1 ?? 0).cgFloat
        rangeSlide.delegate = self
        minPriceLbl.text = "$\(prices?.0 ?? 0)"
        maxPriceLbl.text = "$\(prices?.1 ?? 0)"
        startLoading()
        WebServiceFactory().FilterHotelTypes(lang: Localizer.current) { (error, data, code) in
            guard let data = data as? hotelTypesFilterModel else { return }
            self.types.append(contentsOf: data.data ?? [])
            WebServiceFactory().FilterAmentities { (error, data, code) in
                guard let data = data as? amentitiesFilterModel else { return }
                self.ammenties.append(contentsOf: data.data ?? [])
                WebServiceFactory().FilterTravelStyles(lang: Localizer.current) { (error, data, code) in
                    self.stopLoading()
                    guard let data = data as? travelStylesFilterModel else { return }
                    self.travelType.append(contentsOf: data.data ?? [])
                    self.filterTableView.reloadData()
                    self.setupOldFilter()
                }
            }
        }
    }
    func setupOldFilter() {
        let filterModel = dataSource?.filterModel()
        guard let filter = filterModel else { return }
        if filter.rate != nil {
            rateView.rating = filter.rate ?? 0
        }
        if filter.minPrice != nil {
            rangeSlide.selectedMinValue = (filter.minPrice ?? 0).cgFloat
            minPriceLbl.text = "$\(filter.minPrice ?? 0)"
        }
        if filter.maxPrice != nil {
            rangeSlide.selectedMaxValue = (filter.maxPrice ?? 0).cgFloat
            maxPriceLbl.text = "$\(filter.maxPrice ?? 0)"
        }
        var counter = 0
        for item in ammenties {
            if filter.ammentiesSelected.contains(item.id ?? 0) {
                self.ammentiesSelected.append(counter)
            }
            counter += 1
        }
        counter = 0
        for item in types {
            if filter.typesSelected.contains(item.id ?? 0) {
                self.typesSelected.append(counter)
            }
            counter += 1
        }
        counter = 0
        for item in travelType {
            if filter.travelSelected.contains(item.id ?? 0) {
                self.travelSelected.append(counter)
            }
            counter += 1
        }
        
        self.filterTableView.reloadData()
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        minPriceView.applyShadow(cornerRadius: 25)
        maxPriceView.applyShadow(cornerRadius: 25)
    }
    func filterAmmenties() -> [Int] {
        var array: [Int] = []
        for item in ammentiesSelected {
            if let model = ammenties[safe: item]?.id {
                array.append(model)
            }
        }
        return array
    }
    func filterTypes() -> [Int] {
        var array: [Int] = []
        for item in typesSelected {
            if let model = types[safe: item]?.id {
                array.append(model)
            }
        }
        return array
    }
    func filterTravel() -> [Int] {
        var array: [Int] = []
        for item in travelSelected {
            if let model = travelType[safe: item]?.id {
                array.append(model)
            }
        }
        return array
    }
    @IBAction func filter(_ sender: Any) {
        var minPrice: Int?
        var maxPrice: Int?
        minPrice = Int(rangeSlide.selectedMinValue)
        maxPrice = Int(rangeSlide.selectedMaxValue)
        if minPrice == dataSource?.rangePrices().0 && maxPrice == dataSource?.rangePrices().1 {
            minPrice = nil
            maxPrice = nil
        }
        var rate: Double?
        if  rateView.rating == 0 {
            rate = nil
        } else {
            rate = rateView.rating
        }
        if filterAmmenties().count > 0 || filterTypes().count > 0 || filterTravel().count > 0 || rate ?? 0 > 0 || maxPrice ?? 0 > 0 {
            self.delegate?.didFilter(ammenties: filterAmmenties(), types: filterTypes(), travel: filterTravel(), rate: rate, minPrice: minPrice, maxPrice: maxPrice)
        } else {
            self.delegate?.didReset()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


//MARK:-  TableView Delegate
extension FilterViewController : UITableViewDataSource , UITableViewDelegate {
    
    
    //MARK: - Header For Section
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = FilterHeaderView()
        view.headerLabel.text = sectionArr[section]
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    //MARK: - Header For Section
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return types.count
        } else if section == 1 {
            return ammenties.count
        } else {
            return travelType.count
        }
    }
    
    //MARK:-  TableView Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ammentiesTableViewCell", for: indexPath) as!  ammentiesTableViewCell
            cell.titleLbl.text = ammenties[indexPath.row].name
            if indexPath.row == ammenties.count-1 {
                cell.seperatorView.isHidden = false
                cell.separetorConstraints.constant = 30
            } else {
                cell.seperatorView.isHidden = true
                cell.separetorConstraints.constant = 0
            }
            if self.ammentiesSelected.contains(indexPath.row) {
                cell.choiceBtn.backgroundColor = UIColor(named: "DARKGrey") ?? .darkGray
                cell.choiceBtn.setImage(#imageLiteral(resourceName: "Accept"), for: .normal)
                cell.choiceBtn.cornerRadius = 6
            } else {
                cell.choiceBtn.backgroundColor = .clear
                cell.choiceBtn.setImage(#imageLiteral(resourceName: "emptyCheckMark"), for: .normal)
                cell.choiceBtn.cornerRadius = 0
            }
            cell.choiceBtn.UIViewAction {
                if !self.ammentiesSelected.contains(indexPath.row) {
                    self.ammentiesSelected.append(indexPath.row)
                } else {
                    self.ammentiesSelected.removeAll(indexPath.row)
                }
                tableView.reloadData()
            }
            return cell
            
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "filterTableViewCell", for: indexPath) as!  filterTableViewCell
            if indexPath.section == 0 {
                cell.bodyLbl.text = types[indexPath.row].name
                cell.titleLbl.text = ""
                if self.typesSelected.contains(indexPath.row) {
                    cell.choiceBtn.backgroundColor = UIColor(named: "DARKGrey") ?? .darkGray
                    cell.choiceBtn.setImage(#imageLiteral(resourceName: "Accept"), for: .normal)
                    cell.choiceBtn.cornerRadius = 6
                } else {
                    cell.choiceBtn.backgroundColor = .clear
                    cell.choiceBtn.setImage(#imageLiteral(resourceName: "emptyCheckMark"), for: .normal)
                    cell.choiceBtn.cornerRadius = 0
                }
                cell.choiceBtn.UIViewAction {
                    if !self.typesSelected.contains(indexPath.row) {
                        self.typesSelected.append(indexPath.row)
                    } else {
                        self.typesSelected.removeAll(indexPath.row)
                    }
                    tableView.reloadData()
                }
                if indexPath.row == types.count-1 {
                    cell.seperatorView.isHidden = false
                    cell.sepratorContraints.constant = 29
                } else {
                    cell.seperatorView.isHidden = true
                    cell.sepratorContraints.constant = 20
                }
                
            } else if indexPath.section == 2 {
                cell.bodyLbl.text = travelType[indexPath.row].name
                cell.titleLbl.text = ""
                if self.travelSelected.contains(indexPath.row) {
                    cell.choiceBtn.backgroundColor = UIColor(named: "DARKGrey") ?? .darkGray
                    cell.choiceBtn.setImage(#imageLiteral(resourceName: "Accept"), for: .normal)
                    cell.choiceBtn.cornerRadius = 6
                } else {
                    cell.choiceBtn.backgroundColor = .clear
                    cell.choiceBtn.setImage(#imageLiteral(resourceName: "emptyCheckMark"), for: .normal)
                    cell.choiceBtn.cornerRadius = 0
                }
                cell.choiceBtn.UIViewAction {
                    if !self.travelSelected.contains(indexPath.row) {
                        self.travelSelected.append(indexPath.row)
                    } else {
                        self.travelSelected.removeAll(indexPath.row)
                    }
                    tableView.reloadData()
                }
                if indexPath.row == travelType.count-1 {
                    cell.seperatorView.isHidden = false
                    cell.sepratorContraints.constant = 29
                } else {
                    cell.seperatorView.isHidden = true
                    cell.sepratorContraints.constant = 20
                }
            }
           
            return cell
        }
        
    }
}
extension FilterViewController: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        minPriceLbl.text = "$\(Int(minValue))"
        maxPriceLbl.text = "$\(Int(maxValue))"
    }
}
