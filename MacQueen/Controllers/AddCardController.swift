//
//  PaymentConfirmedViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/20/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
protocol AddCardDelegate: class {
    func didAddCard(card: CardModel)
    func didCheckedCvv(card: CardModel)
}
class AddCardController: UIViewController {
    enum Expire {
        case year
        case month
    }
    @IBOutlet weak var cardNumTxf: UITextField!
    @IBOutlet weak var cvvTxf: UITextField!
    @IBOutlet weak var cardHolderTxf: UITextField!
    @IBOutlet weak var expireMonthTxf: UITextField!
    @IBOutlet weak var expireYearTxf: UITextField!
    @IBOutlet weak var completeBtn: UIButton!
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var pickerView: UIPickerView! {
        didSet {
            pickerView.delegate = self
            pickerView.dataSource = self
        }
    }
    weak var delegate: AddCardDelegate?
    var expire: Expire = .month
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    var years = [2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030]
    var monthsID = [01,02,03,04,05,06,07,08,09,10,11,12]
    var selectedMonth: String?
    var selectedMonthID: Int?
    //MARK:-  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        handlers()
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
    }

    func setup() {
        completeBtn.alpha = 0.30
        cardNumTxf.addPaddingLeft(10)
        cvvTxf.addPaddingLeft(10)
        cardHolderTxf.addPaddingLeft(10)
        
        cardNumTxf.delegate = self
        cvvTxf.delegate = self
        cardHolderTxf.delegate = self
        expireMonthTxf.delegate = self
        expireYearTxf.delegate = self
    }
    func handlers() {
        expireMonthTxf.UIViewAction {
            self.viewPicker.isHidden = false
            self.expire = .month
            self.pickerView.reloadAllComponents()
        }
        expireYearTxf.UIViewAction {
            self.viewPicker.isHidden = false
            self.expire = .year
            self.pickerView.reloadAllComponents()
        }
    }
   
    func validate(txfs: [UITextField], onlyValidate: Bool = false) -> Bool {
        var validate = true
        txfs.forEach { (item) in
            if case item.text?.isEmpty = true {
                if !onlyValidate {
                    item.attributedPlaceholder = NSAttributedString(string: item.placeholder ?? "",
                                                                    attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
                }
                validate = false
            }
        }
        return validate
    }
    @IBAction func done(_ sender: Any) {
        viewPicker.isHidden = true
        if expire == .month {
            let month = pickerView.selectedRow(inComponent: 0)
            selectedMonth = months[month]
            selectedMonthID = monthsID[month]
            expireMonthTxf.text = "\(months[month])"
        } else {
            let year = pickerView.selectedRow(inComponent: 0)
            expireYearTxf.text = "\(years[year])"
        }
        if validate(txfs: [cardNumTxf, cardHolderTxf, cvvTxf, expireYearTxf, expireMonthTxf], onlyValidate: true) {
            completeBtn.alpha = 1
        } else {
            completeBtn.alpha = 0.30
        }
    }
    @IBAction func completeOrder(_ sender: Any) {
        if !validate(txfs: [cardNumTxf, cardHolderTxf, cvvTxf, expireYearTxf, expireMonthTxf]) {
            return
        }
        let card = CardModel(number: cardNumTxf.text, cvv:cvvTxf.text, holder: cardHolderTxf.text, month: selectedMonthID?.string, year: expireYearTxf.text)
        delegate?.didAddCard(card: card)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension AddCardController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if validate(txfs: [cardNumTxf, cardHolderTxf, cvvTxf, expireYearTxf, expireMonthTxf], onlyValidate: true) {
            completeBtn.alpha = 1
        } else {
            completeBtn.alpha = 0.30
        }
    }
}
extension AddCardController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if expire == .month {
            return months.count
        } else {
            return years.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if expire == .month {
            return months[row]
        } else {
            return years[row].string
        }
    }
}
