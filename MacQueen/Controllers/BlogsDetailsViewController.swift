//
//  BlogsDetailsViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/19/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class BlogsDetailsViewController: UIViewController {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var blogImage: UIImageView!
    @IBOutlet weak var blogTitle: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    
    var slug: String? {
        return blog?.slug
    }
    var blog: BlogModel.Datum?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        // Do any additional setup after loading the view.
    }
    

    func setup() {
        let url = URL(string: blog?.featuredImage ?? "")
        blogImage.kf.setImage(with: url)
        titleLbl.text = blog?.title
        blogTitle.text = blog?.title
        descLbl.text = blog?.body?.htmlToString
    }
}
