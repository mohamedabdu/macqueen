//
//  HomeViewController.swift
//  MacQueen
//
//  Created by Kareem on 4/21/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    //MARK: -  @IBOutlet
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var hotelsCollectionView: UICollectionView!
    {
        didSet {
            hotelsCollectionView.delegate = self
            hotelsCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var SearchHotelsView: UIView!
    @IBOutlet weak var blogCollectionView: UICollectionView! {
        didSet {
            blogCollectionView.delegate = self
            blogCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var searchTxf: UITextField!
    @IBOutlet weak var citiesTbl: UITableView! {
        didSet {
            citiesTbl.delegate = self
            citiesTbl.dataSource = self
        }
    }
    @IBOutlet weak var hotelsTbl: UITableView! {
        didSet {
            hotelsTbl.delegate = self
            hotelsTbl.dataSource = self
        }
    }
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var citiesHeight: NSLayoutConstraint!
    @IBOutlet weak var hotelsHeight: NSLayoutConstraint!
    
    
    //MARK: -   Variables
    let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    var checkoutDate: String?
    var checkinDate: String?
    var cities: [cities] = []
    var hotels: [hotels] = []
    var citiesExpanded: Bool = true
    var hotelsExpanded: Bool = true
    var hotelsCollection: [hotel] = []
    var blogs: [BlogModel.Datum] = []
    var location: LocationHelper?
    
    //MARK: -   View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        searchTxf.addTarget(self, action: #selector(HomeViewController.textFieldDidChange(_:)), for: .editingChanged)
        setup()
     }
    
    override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
        SearchHotelsView.applyShadow(cornerRadius: 30)
    
    }
    func setup() {
        self.fetchNearbyHotels()
        self.fetchBlogs()
        location = .init()
        location?.onUpdateLocation = { degree in
            self.fetchNearbyHotels()
        }
        location?.currentLocation()
        
        let todayDate = Date()
        guard let tomorrowDate = Calendar.current.date(byAdding: .day , value: 1 , to: todayDate) else { return }
        didTapDoneWithDateRange(startDate: todayDate, endDate: tomorrowDate)
    }
    func goToHotelDetails(hotel: hotel?) {
        guard let hotel = hotel else { return }
        let hotelsDetailsViewController = storyBoard.instantiateViewController(identifier: "HotelDetailsViewController") as! HotelDetailsViewController
        hotelsDetailsViewController.hotel = hotel
        hotelsDetailsViewController.date = dateLbl.text
        hotelsDetailsViewController.checkinDate = checkinDate
        hotelsDetailsViewController.checkoutDate = checkoutDate
        hotelsDetailsViewController.rooms = []
        hotelsDetailsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(hotelsDetailsViewController , animated: true, completion: nil)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text?.count ?? 0 > 2 {
            searchView.fadeIn()
            startLoading()
            WebServiceFactory().SearchByWord(keyword: textField.text ?? "") { (error, model, code) in
                self.stopLoading()
                if code > 200 {
                    self.showAlert(title: "Alert".localized, message: error?.eMsg)
                    return
                }
                let model = model as? searchByWordModel
                self.cities.removeAll()
                self.cities.append(contentsOf: model?.data?.cities ?? [])
                self.hotels.removeAll()
                self.hotels.append(contentsOf: model?.data?.hotels ?? [])
                self.citiesTbl.reloadData()
                self.hotelsTbl.reloadData()
                self.setupHeight()
            }
        } else {
            searchView.isHidden = true
        }
    }
    func setupHeight() {
        if citiesExpanded {
            citiesHeight.constant = 60
            citiesHeight.constant += (35*cities.count).cgFloat
        } else {
            citiesHeight.constant = 60
        }
        if hotelsExpanded {
            hotelsHeight.constant = 60
            hotelsHeight.constant += (35*hotels.count).cgFloat
        } else {
            hotelsHeight.constant = 60
        }
    }
   
    
    //MARK: -   @IBAction
    @IBAction func resetSearch(_ sender: UIButton) {
        searchTxf.text = ""
        textFieldDidChange(searchTxf)
    }
    @IBAction func seeAllHotelClicked(_ sender: UIButton) {
        let hotelsListViewController = storyBoard.instantiateViewController(identifier: "HotelsListViewController") as! HotelsListViewController
        hotelsListViewController.oldHotels = self.hotelsCollection
        hotelsListViewController.hotels = self.hotelsCollection
        hotelsListViewController.fromSeeAll = true
        hotelsListViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(hotelsListViewController , animated: true, completion: nil)

    }
    @IBAction func seeAllBlogsClicked(_ sender: UIButton) {
        let blogsListViewController = storyBoard.instantiateViewController(identifier: "BlogsListViewController") as! BlogsListViewController
        blogsListViewController.blogs = blogs
        blogsListViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(blogsListViewController , animated: true, completion: nil)
    }
    
}
// MARK:  Network
extension HomeViewController {
    func fetchNearbyHotels() {
        
        WebServiceFactory().HotelsNearby(lat: location?.lat, lng: location?.lng) { (error, data, code) in
            guard let data = data as? hotelsDataModel else { return }
            self.hotelsCollection.removeAll()
            self.hotelsCollection.append(contentsOf: data.data ?? [])
            self.hotelsCollectionView.reloadData()
        }
    }
    func fetchBlogs() {
        
        WebServiceFactory().Blogs { (erorr, data, code) in
            self.blogs.removeAll()
            self.blogs.append(contentsOf: data?.data ?? [])
            self.blogCollectionView.reloadData()
        }
        
    }
}
 //MARK: -   CollectionViewDelegate
extension HomeViewController : UICollectionViewDelegate , UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.hotelsCollectionView {
            return hotelsCollection.count
        } else {
            return blogs.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.hotelsCollectionView {
            var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HotelsCollectionViewCell", for: indexPath) as! HotelsCollectionViewCell
            cell.model = hotelsCollection[indexPath.row]
            return cell
        } else {
            var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "blogsCollectionViewCell", for: indexPath) as! blogsCollectionViewCell
            cell.model = blogs[indexPath.row]
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.hotelsCollectionView {
            goToHotelDetails(hotel: hotelsCollection[indexPath.row])
        } else {
            let blogsDetailsViewController = storyBoard.instantiateViewController(identifier: "BlogsDetailsViewController") as! BlogsDetailsViewController
            blogsDetailsViewController.blog = blogs[indexPath.row]
            blogsDetailsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
            self.present(blogsDetailsViewController , animated: true, completion: nil)
            
        }
    }
}


extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchTxf.endEditing(true)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SearchHeaderView()
        if tableView == citiesTbl {
            view.headerLabel.text = "Cities".localized
            view.closure = { [weak self] in
                self?.citiesExpanded = !(self?.citiesExpanded ?? false)
                self?.citiesTbl.reloadData()
                self?.setupHeight()
            }
        } else {
            view.headerLabel.text = "Hotels".localized
            view.closure = { [weak self] in
                self?.hotelsExpanded = !(self?.hotelsExpanded ?? false)
                self?.hotelsTbl.reloadData()
                self?.setupHeight()
            }
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == citiesTbl {
            if citiesExpanded {
                return cities.count
            } else {
                return 0
            }
        } else {
            if hotelsExpanded {
                return hotels.count
            } else {
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == citiesTbl {
            var cell = tableView.cell(type: SearchCityCell.self, indexPath)
            cell.model = cities[indexPath.row]
            return cell
        } else {
            var cell = tableView.cell(type: SearchCityCell.self, indexPath)
            cell.model = hotels[indexPath.row]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let scene = controller(SearchInputsViewController.self, storyboard: .updated)
        if tableView == citiesTbl {
            scene.type = .city
            scene.city = cities[indexPath.row]
            SearchInputsViewController.storeSearch(word: cities[indexPath.row].name)
        } else {
            scene.type = .hotel
            scene.hotel = hotels[indexPath.row]
            SearchInputsViewController.storeSearch(word: hotels[indexPath.row].name, type: .hotel)
        }
        self.present(scene , animated: true, completion: nil)
    }
}

extension HomeViewController {
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        let fromDate = formatter.string(from: startDate)
        let toDate = formatter.string(from: endDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let dateObj = dateFormatter.date(from: fromDate) else { return }
        checkinDate = dateFormatter.string(from: dateObj)
        let format = DateFormatter()
        format.dateFormat = "E , MMM d"
        let fromDateStr = format.string(from: dateObj)
        guard let toDateObj = dateFormatter.date(from: toDate) else { return }
        checkoutDate = dateFormatter.string(from: toDateObj)
        let toDateStr = format.string(from: toDateObj)
        self.dateLbl.text = fromDateStr + "  -  " + toDateStr
    }
}

//extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
 
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 3
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row == 0 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "topDestinationTableViewCell", for: indexPath) as! topDestinationTableViewCell
//            cell.topDestinationCollectionView.reloadData()
//            return cell
//        } else if indexPath.row == 1 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "popularHotelsTableViewCell", for: indexPath) as! popularHotelsTableViewCell
//            cell.popularHotelsCollectionView.reloadData()
//            return cell
//
//        } else  {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "packagesTableViewCell", for: indexPath) as! packagesTableViewCell
//             cell.packagesCollectionView.reloadData()
//             return cell
//        }
//    }
//
//}


