//
//  DealsViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/18/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import Cosmos
class DealsViewController: UIViewController {
    
    //MARK:-  @IBOutlet
    @IBOutlet weak var hotelTitleLbl: UILabel!
    @IBOutlet weak var hotelNameLbl: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var hotelRate: CosmosView!
    @IBOutlet weak var checkDate: UILabel!
    @IBOutlet weak var roomsLbl: UILabel!
    @IBOutlet weak var adultLbl: UILabel!
    
    @IBOutlet weak var dealsTableView: UITableView! {
        didSet {
            dealsTableView.dataSource = self
            dealsTableView.delegate = self
        }
    }
    @IBOutlet weak var imagesCollectionView: UICollectionView!{
        didSet {
            imagesCollectionView.dataSource = self
            imagesCollectionView.delegate = self
        }
    }
    @IBOutlet weak var facilitiesCollectionView: UICollectionView! {
        didSet {
            facilitiesCollectionView.dataSource = self
            facilitiesCollectionView.delegate = self
        }
    }
    @IBOutlet weak var heightFacilitiesCollectionView: NSLayoutConstraint!
    @IBOutlet weak var dealsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imageCounterLabel: UILabel!
    
    var hotel: hotel?
    var date: String?
    var checkoutDate: String?
    var checkinDate: String?
    var rooms: [RoomsModel] = []
    
    //MARK:-  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        self.heightFacilitiesCollectionView.constant = self.facilitiesCollectionView.collectionViewLayout.collectionViewContentSize.height
        self.view.setNeedsLayout()
        setup()
        handlers()
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.dealsTableViewHeight.constant = self.dealsTableView.contentSize.height
    }
    func setup() {
        imageCounterLabel.text = ""
        var adultCount = 0
        for item in rooms {
            adultCount += item.adults
        }
        checkDate.text = date
        roomsLbl.text = rooms.count.string
        adultLbl.text = adultCount.string
        hotelTitleLbl.text = hotel?.hotel_name
        hotelNameLbl.text = hotel?.hotel_name
        hotelRate.rating = hotel?.rate?.double() ?? 0
        reloadFavorite()
    }
    func handlers() {
        favoriteBtn.UIViewAction {
            if case UserStatus.isLogged = true {
                self.addToFavorite()
            } else {
                UserStatus.loginAlert(in: self)
            }
        }
    }
    func addToFavorite() {
        WebServiceFactory().addFavorite(hotelID: hotel?.hotel_id ?? 0) { (error, data, code) in
            if data != nil {
                self.hotel?.is_favourite = data?.data?.is_favourite
                self.reloadFavorite()
            }
        }
    }
    func reloadFavorite() {
        if case hotel?.is_favourite = true {
            favoriteBtn.setImage(#imageLiteral(resourceName: "HeartFill"), for: .normal)
        } else {
            favoriteBtn.setImage(#imageLiteral(resourceName: "Favorites"), for: .normal)
        }
    }
    
}


//MARK:-  TableView Delegate
extension DealsViewController : UITableViewDataSource , UITableViewDelegate {
    
   
    //MARK: - Header For Section
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = DealsHeaderView()
        view.headerLabel.text = "\("Deal".localized) \(section+1)"
        return view

    }
       
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    
    //MARK: - Header For Section
    func numberOfSections(in tableView: UITableView) -> Int {
        return hotel?.deals?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotel?.deals?[section].rooms?.count ?? 0
    }
    
    //MARK:-  TableView Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "DealsTableViewCell", for: indexPath) as! DealsTableViewCell
        cell.row = indexPath
        cell.delegate = self
        cell.model = hotel?.deals?[indexPath.section]
        
        return cell
    }
}

//MARK:- UICollectionViewDataSource , UICollectionViewDelegate

extension DealsViewController : UICollectionViewDataSource , UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          if collectionView == self.facilitiesCollectionView {
            return 4
        }
        return hotel?.images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if collectionView == self.facilitiesCollectionView {
            let cell = self.facilitiesCollectionView.dequeueReusableCell(withReuseIdentifier: "facilitiesCollectionViewCell", for: indexPath) as! facilitiesCollectionViewCell
            return cell
        } else {
            let cell = self.imagesCollectionView.dequeueReusableCell(withReuseIdentifier: "HotelImagesSliderCollectionViewCell", for: indexPath) as! HotelImagesSliderCollectionViewCell
        let url = URL(string: hotel?.images?[indexPath.row] ?? "")
        cell.imageHotel.kf.setImage(with: url)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == self.imagesCollectionView {
            self.imageCounterLabel.text = "\(indexPath.item + 1)/\(hotel?.images?.count ?? 0)"
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.imagesCollectionView {
            let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
            let scene = storyBoard.instantiateViewController(identifier: "imageCollectionViewController") as! imageCollectionViewController
            scene.imagesArr = hotel?.images ?? []
            scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
            self.present(scene , animated: true, completion: nil)
        }
    }
}


//MARK:- UICollection Delegate Flow layout
extension DealsViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == facilitiesCollectionView {
            return CGSize(width: collectionView.frame.size.width/2 , height: 35)
        }  else {
            return  CGSize(width: collectionView.bounds.width , height: collectionView.bounds.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == facilitiesCollectionView {
            return UIEdgeInsets.init(top: 0 ,left: 0, bottom: 0, right: 0)
        } else {
            return UIEdgeInsets.init(top: 0 ,left: 0, bottom: 0, right: 0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}

extension DealsViewController: DealCellDelegate {
    func didBook(section: Int) {
        guard let deal = hotel?.deals?[section] else { return }
        let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
        let dealsViewController = storyBoard.instantiateViewController(identifier: "HotelSummaryViewController") as! HotelSummaryViewController
        dealsViewController.hotel = hotel
        dealsViewController.date = date
        dealsViewController.checkinDate = checkinDate
        dealsViewController.checkoutDate = checkoutDate
        dealsViewController.rooms = rooms
        dealsViewController.deal = deal
        dealsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(dealsViewController , animated: true, completion: nil)
    }
}
