//
//  BillingDetailsViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/14/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class BillingDetailsViewController: UIViewController {

    //MARK:-  @IBOutlet
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var lastNameTextField: UITextField!
//    @IBOutlet weak var billingCountryView: UIView!
//    @IBOutlet weak var billingStateView: UIView!
//    @IBOutlet weak var billingStateTextField: UITextField!
//    @IBOutlet weak var billingCityView: UIView!
//    @IBOutlet weak var billingCityTextField: UITextField!
//    @IBOutlet weak var billingStreetView: UIView!
//    @IBOutlet weak var billingStreetTextField: UITextField!
//    @IBOutlet weak var billingPostalView: UIView!
//    @IBOutlet weak var billingPostalTextField: UITextField!
//    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var passengersTbl: UITableView! {
        didSet {
            passengersTbl.delegate = self
            passengersTbl.dataSource = self
            
        }
    }
    
    
    var hotel: hotel?
    var date: String?
    var checkoutDate: String?
    var checkinDate: String?
    var rooms: [RoomsModel] = []
    var deal: deals?
    var dealRow: Int?
    var countries: [Countries.Data] = []
    var cities: [Countries.Data] = []
    var selectedCountry: Int?
    var selectedCity: Int?
    //MARK:-  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        fetchCountries()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
     super.viewDidLayoutSubviews()
        dropShadowForAllViews()
     }
    //MARK:- Methods
    
    private func dropShadowForAllViews(){
        emailView.applyShadow(cornerRadius: 30)
        firstNameView.applyShadow(cornerRadius: 30)
        lastNameView.applyShadow(cornerRadius: 30)
//        billingCountryView.applyShadow(cornerRadius: 30)
//        billingCityView.applyShadow(cornerRadius: 30)
//        billingStateView.applyShadow(cornerRadius: 30)
//        billingStreetView.applyShadow(cornerRadius: 30)
//        billingPostalView.applyShadow(cornerRadius: 30)
    }
    func setup() {
//        billingCityTextField.UIViewAction { [weak self] in
//            self?.openPickerCity()
//        }
        if case UserStatus.isLogged = true {
            firstNameTextField.text = UserStatus.firstName
            lastNameTextField.text = UserStatus.lastName
            emailTextField.text = UserStatus.email
        }
    }
    func openPickerCountry() {
//        let scene = self.controller(PickerController.self, storyboard: .PickerViewHelper)
//        scene.source = countries
//        scene.titleClosure = { path in
//            return self.countries[path].name ?? ""
//        }
//        scene.didSelectClosure = { path in
//            self.countryLbl.attributedText = NSAttributedString(string: self.countries[path].name ?? "",
//                                                           attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
//            self.selectedCountry = path
//            self.fetchCities()
//        }
//        pushPop(vcr: scene)
    }
    func openPickerCity() {
//        let scene = self.controller(PickerController.self, storyboard: .PickerViewHelper)
//        scene.source = cities
//        scene.titleClosure = { path in
//            return self.cities[path].name ?? ""
//        }
//        scene.didSelectClosure = { path in
//            self.billingCityTextField.text = self.cities[path].name ?? ""
//            self.selectedCity = path
//        }
//        pushPop(vcr: scene)
    }
    func validate(txfs: [UITextField]) -> Bool {
        var validate = true
        txfs.forEach { (item) in
            if case item.text?.isEmpty = true {
                item.attributedPlaceholder = NSAttributedString(string: item.placeholder ?? "",
                                                                attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
                validate = false
            }
        }
        return validate
    }
    func validateTbl() -> Bool {
        var validate = true
        for cell in passengersTbl.visibleCells {
            guard let cell = cell as? RoomPassengerCell else { return false }
            if !self.validate(txfs: [cell.firstnameTxf, cell.lastnameTxf, cell.titleTxf]) {
                validate = false
            }
        }
        return validate
    }
    func setupRoomsPassengers() -> [RoomsModel] {
        var rooms: [RoomsModel] = []
        guard var firstRoom = self.rooms[safe: 0] else { return [] }
        firstRoom.lead_passenger = RoomsModel.RoomPasseger(title: "MR", first_name: firstNameTextField.text ?? "", last_name: lastNameTextField.text ?? "", type: "adult")
        rooms.append(firstRoom)
        for cell in passengersTbl.visibleCells {
            guard let cell = cell as? RoomPassengerCell else { return [] }
            guard var room = cell.roomModel else { return [] }
            room.lead_passenger = RoomsModel.RoomPasseger(title: cell.titleTxf.text ?? "", first_name: cell.firstnameTxf.text ?? "", last_name: cell.lastnameTxf.text ?? "", type: "adult")
            rooms.append(room)
        }
        return rooms
    }
    func setupParamters() -> [String: Any] {
        let roomsData = rooms.compactMap({($0.decRooms)})
        //let payment = PaymentModel(token: "", type: "").dec
        var params = ["checkin_date":checkinDate ?? "" , "checkout_date" : checkoutDate ?? "" , "nationality_code":"SA" , "key":deal?.code ?? "" , "rooms":roomsData] as [String : Any]
        params["email"] = emailTextField.text ?? ""
        params["first_name"] = firstNameTextField.text ?? ""
        params["last_name"] = lastNameTextField.text ?? ""
        params["billing_country"] = countries[safe: selectedCountry ?? 0]?.code ?? ""
        params["billing_state"] = "a"
        params["billing_city"] = "city"
        params["billing_street"] = "arewwr"
        params["billing_postal_code"] = "code"
        //params["payment"] = payment
        return params
    }
    //MARK:- @IBAction
    @IBAction func countryPick(_ sender: Any) {
        openPickerCountry()
    }
    @IBAction func payClicked(_ sender: UIButton) {
        if !validate(txfs: [emailTextField, firstNameTextField, lastNameTextField]) || !validateTbl() {
            return
        }
        if case emailTextField.text?.isEmail = false {
            emailTextField.attributedText = NSAttributedString(string: emailTextField.text ?? "",
                                                            attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            return
        }
//        if selectedCountry == nil {
//            countryLbl.attributedText = NSAttributedString(string: "Billing Country".localized,
//                                                            attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
//        }
        let roomsUpdated = setupRoomsPassengers()
        if roomsUpdated.count > 0 {
            self.rooms.removeAll()
            self.rooms.append(contentsOf: roomsUpdated)
        } else {
            showAlert(title: "Alert".localized, message: "Rooms Can't fetched".localized)
        }
        let goToConfirmedViewController = controller(SelectCardController.self, storyboard: .updated)
        goToConfirmedViewController.paramters = setupParamters()
        goToConfirmedViewController.hotel = hotel
        goToConfirmedViewController.deal = deal
        goToConfirmedViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(goToConfirmedViewController , animated: true, completion: nil)
       
    }
}
// MARK:  Network
extension BillingDetailsViewController {
    func fetchCountries() {
        WebServiceFactory().Countries(lang: Localizer.current) { (erorr, data, code) in
            guard let data = data as? Countries else { return }
            self.countries.removeAll()
            self.countries.append(contentsOf: data.data ?? [])
        }
    }
    func fetchCities() {
        guard let countryID = countries[safe: selectedCountry ?? 0]?.id else { return }
        
        WebServiceFactory().Cities(lang: Localizer.current, countryID: countryID) { (erorr, data, code) in
            guard let data = data as? Countries else { return }
            self.cities.removeAll()
            self.cities.append(contentsOf: data.data ?? [])
        }
    }
}
extension BillingDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count-1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.cell(type: RoomPassengerCell.self, indexPath)
        cell.roomModel = rooms[indexPath.row+1]
        cell.setup()
        return cell
    }
}
