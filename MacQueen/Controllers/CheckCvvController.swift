//
//  PaymentConfirmedViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/20/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class CheckCvvController: UIViewController {
    
    @IBOutlet weak var cardNumTxf: UITextField!
    @IBOutlet weak var cvvTxf: UITextField!
    @IBOutlet weak var completeBtn: UIButton!
   
    weak var delegate: AddCardDelegate?
    var card: SavedCardModel.Data?
    //MARK:-  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
    }

    func setup() {
        completeBtn.alpha = 0.30
        cardNumTxf.addPaddingLeft(10)
        cvvTxf.addPaddingLeft(10)
        
        cardNumTxf.delegate = self
        cvvTxf.delegate = self
        
        cardNumTxf.text = "**** **** **** \(card?.last4 ?? "")"
        cardNumTxf.isUserInteractionEnabled = false
    }

    func validate(txfs: [UITextField], onlyValidate: Bool = false) -> Bool {
        var validate = true
        txfs.forEach { (item) in
            if case item.text?.isEmpty = true {
                if !onlyValidate {
                    item.attributedPlaceholder = NSAttributedString(string: item.placeholder ?? "",
                                                                    attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
                }
                validate = false
            }
        }
        return validate
    }
  
    @IBAction func completeOrder(_ sender: Any) {
        if !validate(txfs: [cvvTxf]) {
            return
        }
        var card = CardModel(number: cardNumTxf.text, cvv:cvvTxf.text, holder: "", month: "", year: "")
        card.id = self.card?.card_id
        delegate?.didCheckedCvv(card: card)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension CheckCvvController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if validate(txfs: [cvvTxf], onlyValidate: true) {
            completeBtn.alpha = 1
        } else {
            completeBtn.alpha = 0.30
        }
    }
}
