//
//  NotificationsViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/18/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    
    //MARK: @IBOutlet
    @IBOutlet weak var notificationsTableView: UITableView! {
        didSet {
            notificationsTableView.delegate = self
            notificationsTableView.dataSource = self
        }
    }
    
    var notifications: [NotificationModel.Datum] = []
    //MARK: View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        fetchNotifications()

        // Do any additional setup after loading the view.
    }
    func fetchNotifications() {
        startLoading()
        WebServiceFactory().Notifications { (erorr, data, code) in
            self.stopLoading()
            if data?.data?.count ?? 0 == 0 {
                self.notificationsTableView.isHidden = true
                return
            } else {
                self.notificationsTableView.isHidden = false
            }
            self.notifications.removeAll()
            self.notifications.append(contentsOf: data?.data ?? [])
            self.notificationsTableView.reloadData()
        }
    }
    func removeNotifications(id: String) {
        startLoading()
        WebServiceFactory().DeleteNotifications(id: id) { (erorr, data, code) in
            self.stopLoading()
            if data?.data?.count ?? 0 == 0 {
                self.notificationsTableView.isHidden = true
                return
            } else {
                self.notificationsTableView.isHidden = false
            }
            self.notifications.removeAll()
            self.notifications.append(contentsOf: data?.data ?? [])
            self.notificationsTableView.reloadData()
        }
    }
}


//MARK: UITableViewDelegate
extension NotificationsViewController : UITableViewDelegate , UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationsTableViewCell", for: indexPath) as! notificationsTableViewCell
        cell.titleLbl.text = notifications[indexPath.row].data?.title
        cell.bodyLbl.text = notifications[indexPath.row].data?.body
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.removeNotifications(id: self.notifications[indexPath.row].id ?? "")
        }
    }
}
