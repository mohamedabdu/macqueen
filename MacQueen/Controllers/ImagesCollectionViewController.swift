//
//  imageCollectionViewController.swift
//  MacQueen
//
//  Created by Kareem on 1/10/20.
//  Copyright © 2020 Mahmoud Fares. All rights reserved.
//

import UIKit
import Kingfisher
import PhotoSlider

class imageCollectionViewController: UIViewController {
    
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var imagesCollectionView: UICollectionView! {
        didSet {
            imagesCollectionView.delegate = self
            imagesCollectionView.dataSource = self
        }
    }
    var imagesArr = [String]()
    var currentRow = 0
    var imagesArrUrl = [URL]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        print("imagesArrsssss\(imagesArr)")
        self.title = "Photos".localized
        imagesArrUrl = imagesArr.compactMap { URL(string:$0) }
       
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension imageCollectionViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.cell(type: ImageCollectionCell.self, indexPath)
        let url = URL(string: imagesArr[indexPath.row].addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        cell.gallaryImages.borderWidth = 0
        cell.gallaryImages.borderColor = .clear
        cell.gallaryImages.layer.cornerRadius = 10
        cell.gallaryImages.kf.indicatorType = .activity
        cell.gallaryImages.kf.setImage(
            with: url,
            placeholder: UIImage(named: "footer-img"),
            options: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let photoSlider = PhotoSlider.ViewController(imageURLs: imagesArrUrl)
        //                photoSlider.pageControl.isHidden = true
        photoSlider.backgroundViewColor = .clear
        photoSlider.delegate = self
        photoSlider.currentPage = indexPath.row
        //photoSlider.visibleCloseButton = false
        photoSlider.visiblePageControl = false
        photoSlider.captionNumberOfLines = 0
        
        // UIViewControllerTransitioningDelegate
        photoSlider.transitioningDelegate = self
        photoSlider.modalPresentationStyle = .overCurrentContext
        
        // Here implemention is better if you want to use ZoomingAnimationControllerTransitioning.
        
        photoSlider.modalTransitionStyle   = .crossDissolve
        present(photoSlider, animated: true, completion: nil)
        
        //        let mainSb = UIStoryboard(name: "Main", bundle: nil)
        //        let vc = mainSb.instantiateViewController(withIdentifier: "FullImageViewController") as! FullImageViewController
        //        let imageUrl = self.imagesArr[indexPath.row]
        //        vc.imageUrl = URL(string: imageUrl)
        //        vc.modalPresentationStyle = .overCurrentContext
        //        self.present(vc, animated: true, completion: nil)
        
    }
}

extension imageCollectionViewController : UICollectionViewDelegateFlowLayout {
    
    // UICollection Delegate Flow layout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/4 , height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0 ,left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}




// MARK: - PhotoSliderDelegate

extension imageCollectionViewController: PhotoSliderDelegate {
    
    func photoSliderControllerWillDismiss(_ viewController: PhotoSlider.ViewController) {
        currentRow = viewController.currentPage
        let indexPath = IndexPath(item: currentRow, section: 0)
        imagesCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
    }
    
}

// MARK: - ZoomingAnimationControllerTransitioning

extension imageCollectionViewController: ZoomingAnimationControllerTransitioning {
    
    func transitionSourceImageView() -> UIImageView {
        
        let indexPath = imagesCollectionView.indexPathsForSelectedItems?.first
        let cell = imagesCollectionView.cellForItem(at: indexPath!) as! ImageCollectionCell
        let imageView = UIImageView(image: cell.gallaryImages.image)
        
        var frame = cell.gallaryImages.frame
        frame.origin.y += UIApplication.shared.statusBarFrame.height
        // tune in UIImageView
        if #available(iOS 11.0, *) {
            frame.origin.x = view.safeAreaInsets.left
            if view.bounds.width > view.bounds.height {
                frame.size.height = view.safeAreaLayoutGuide.layoutFrame.height
            }
        }
        imageView.frame = frame
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        
        return imageView
    }
    
    func transitionDestinationImageView(sourceImageView: UIImageView) {
        
        guard let image = sourceImageView.image else {
            return
        }
        
        guard let cell = imagesCollectionView.visibleCells.first as? ImageCollectionCell else {
            return
        }
        let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
        var frame = CGRect.zero
        
        if view.bounds.size.width < view.bounds.height {
            
            if image.size.height < image.size.width {
                let width = (sourceImageView.image!.size.width * sourceImageView.bounds.size.width) / sourceImageView.image!.size.height
                let x = (width - cell.gallaryImages.bounds.height) * 0.5
                frame = CGRect(x: -1.0 * x, y: statusBarHeight, width: width, height: cell.gallaryImages.bounds.height)
            } else {
                frame = CGRect(x: 0.0, y: statusBarHeight, width: view.bounds.width, height: view.bounds.width)
            }
            
        } else {
            let width: CGFloat
            let x: CGFloat
            if #available(iOS 11.0, *) {
                width = view.bounds.width - view.safeAreaInsets.left - view.safeAreaInsets.right
                x = view.safeAreaInsets.left
            } else {
                width = view.bounds.width
                x = 0
            }
            
            let height: CGFloat
            if #available(iOS 11.0, *) {
                height = view.safeAreaLayoutGuide.layoutFrame.height
            } else {
                height = (image.size.height * width) / image.size.width
            }
            
            let y: CGFloat
            if #available(iOS 11.0, *) {
                y = (height - view.safeAreaLayoutGuide.layoutFrame.height - statusBarHeight) * 0.5
            } else {
                y = (height - UIScreen.main.bounds.height - statusBarHeight) * 0.5
            }
            
            frame = CGRect(x: x, y: -1.0 * y, width: width, height: height)
        }
        sourceImageView.frame = frame
        
    }
}

// MARK: - UIViewControllerTransitioningDelegate

extension imageCollectionViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animationController = PhotoSlider.ZoomingAnimationController(present: false)
        animationController.sourceTransition = dismissed as? ZoomingAnimationControllerTransitioning
        animationController.destinationTransition = self
        
        view.frame = dismissed.view.bounds
        
        return animationController
        
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animationController = PhotoSlider.ZoomingAnimationController(present: true)
        animationController.sourceTransition = source as? ZoomingAnimationControllerTransitioning
        animationController.destinationTransition = presented as? ZoomingAnimationControllerTransitioning
        return animationController
        
    }
    
}

// MARK: - Private Methods

extension imageCollectionViewController {
    
    func updateCurrentRow(to size: CGSize) {
        var row = Int(round(imagesCollectionView.contentOffset.x / imagesCollectionView.bounds.width))
        if row < 0 {
            row = 0
        }
        currentRow = row
    }
    
}

