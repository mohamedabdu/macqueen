//
//  OTPViewController.swift
//  MacQueen
//
//  Created by Kareem on 4/21/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController {
    
    //MARK:-  @IBOutlet
    @IBOutlet var oTpView: [UIView]!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var confirmLbl: UILabel!
    @IBOutlet weak var code1Txf: UITextField!
    @IBOutlet weak var code2Txf: UITextField!
    @IBOutlet weak var code3Txf: UITextField!
    @IBOutlet weak var code4Txf: UITextField!
    @IBOutlet weak var code5Txf: UITextField!
    @IBOutlet weak var emailCodeTxf: UITextField!
    
    var forEmail: Bool = false
    var forForget: Bool = false
    var user: UserModel?
    var email: String?
    weak var delegate: UIViewController?
    var code: String! {
        var code = ""
        if forEmail || forForget {
            code = emailCodeTxf.text ?? ""
        } else {
            if Localizer.current == "ar" {
                code = "\(code5Txf.text ?? "")\(code4Txf.text ?? "")\(code3Txf.text ?? "")\(code2Txf.text ?? "")\(code1Txf.text ?? "")"
            } else {
                code = "\(code1Txf.text ?? "")\(code2Txf.text ?? "")\(code3Txf.text ?? "")\(code4Txf.text ?? "")\(code5Txf.text ?? "")"
            }
        }
        return code
    }
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        dropShadowForAllViews()
    }
    //MARK:- Methods
    
    private func dropShadowForAllViews(){
        for view in self.oTpView {
            view.applyShadow(cornerRadius: 10)
        }
    }
    func setup() {
        if forEmail || forForget {
            if case email?.isEmail = true {
                confirmLbl.text = "Confirm your email".localized
            } else {
                confirmLbl.text = "Confirm your mobile".localized
            }
            if forForget {
                titleLbl.text = "\("Enter the code just sent to".localized) \(email ?? ""):"
            } else {
                titleLbl.text = "\("Enter the code just sent to".localized) \(user?.data?.email ?? ""):"
            }
            emailCodeTxf.isHidden = false
            stackView.isHidden = true
            emailCodeTxf.delegate = self
        } else {
            confirmLbl.text = "Confirm your number".localized
            titleLbl.text = "\("Enter the code just sent to".localized) \(user?.data?.mobile ?? ""):"
            emailCodeTxf.isHidden = true
            emailCodeTxf.superview?.isHidden = true
            stackView.isHidden = false
            code1Txf.delegate = self
            code2Txf.delegate = self
            code3Txf.delegate = self
            code4Txf.delegate = self
            code5Txf.delegate = self
            code1Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
            code2Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
            code3Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
            code4Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
            code5Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        }
        
    }
    func validate(txfs: [UITextField]) -> Bool {
        var validate = true
        txfs.forEach { (item) in
            if case item.text?.isEmpty = true {
                item.attributedPlaceholder = NSAttributedString(string: item.placeholder ?? "",
                                                                attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
                validate = false
            }
        }
        return validate
    }
    func didVerify() {
        if forForget {
            if !validate(txfs: [emailCodeTxf]) {
                return
            }
            startLoading()
            WebServiceFactory().ResetPasswordCode(email: email ?? "", code: code) { (error, data, code) in
                self.stopLoading()
                if error != nil {
                    self.showAlert(title: "Alert".localized, message: error?.eMsg)
                } else {
                    let scene = self.controller(ResetPasswordController.self, storyboard: .auth)
                    scene.email = self.email
                    scene.code = self.code
                    self.delegate?.dismiss(animated: true, completion: {
                        self.delegate?.present(scene, animated: true, completion: nil)
                    })
                }
                
            }
        } else {
            var data = ""
            if forEmail || forForget {
                data = user?.data?.email ?? ""
            } else {
                data = user?.data?.mobile ?? ""
            }
            startLoading()
            WebServiceFactory().ActiveUser(email: data, code: code) { (error, data, code) in
                self.stopLoading()
                if code > 300 {
                    self.showAlert(title: "Alert".localized, message: data?.message)
                } else {
                    UserStatus.isLogged = true
                    UserStatus.email = self.user?.data?.email
                    UserStatus.firstName = self.user?.data?.firstName
                    UserStatus.lastName = self.user?.data?.lastName
                    UserStatus.mobile = self.user?.data?.mobile
                    UserStatus.token = self.user?.meta?.token
                    RootWindowController.setRootWindowForHome()
                }
                
            }
        }
        
    }
    @IBAction func resend(_ sender: Any) {
        startLoading()
        var data = ""
        if forEmail || forForget {
            data = user?.data?.email ?? ""
        } else {
            data = user?.data?.mobile ?? ""
        }
        WebServiceFactory().resendActivation(email: data) { (error, data, code) in
            self.stopLoading()
            self.showAlert(title: "Alert".localized, message: data?.message)
        }
    }
    @IBAction func anotherWay(_ sender: Any) {
    }
    
}


extension OTPViewController: UITextFieldDelegate {
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == emailCodeTxf {
            didVerify()
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
            case code1Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code1Txf.text = ""
                    }
            }
            case code2Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code2Txf.text = ""
                    }
            }
            case code3Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code3Txf.text = ""
                    }
            }
            case code4Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code4Txf.text = ""
                    }
            }
            case code5Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code5Txf.text = ""
                    }
            }
            default:
                break
        }
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        if Localizer.current == "ar" {
            return setupArabic(textField)
        }
        switch textField {
            case code1Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code2Txf.becomeFirstResponder()
                    } else {
                        view.endEditing(true)
                    }
            }
            case code2Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code3Txf.becomeFirstResponder()
                    } else {
                        code1Txf.becomeFirstResponder()
                    }
            }
            case code3Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code4Txf.becomeFirstResponder()
                    } else {
                        code2Txf.becomeFirstResponder()
                    }
            }
            case code4Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code5Txf.becomeFirstResponder()
                    } else {
                        code3Txf.becomeFirstResponder()
                    }
            }
            case code5Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        didVerify()
                    } else {
                        code4Txf.becomeFirstResponder()
                    }
            }
            default:
                break
        }
    }
    func setupArabic(_ textField: UITextField) {
        switch textField {
            case code5Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code4Txf.becomeFirstResponder()
                    } else {
                        view.endEditing(true)
                    }
            }
            case code4Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code3Txf.becomeFirstResponder()
                    } else {
                        code5Txf.becomeFirstResponder()
                    }
            }
            case code3Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code2Txf.becomeFirstResponder()
                    } else {
                        code4Txf.becomeFirstResponder()
                    }
            }
            case code2Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code1Txf.becomeFirstResponder()
                    } else {
                        code3Txf.becomeFirstResponder()
                    }
            }
            case code1Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        self.didVerify()
                    } else {
                        code2Txf.becomeFirstResponder()
                    }
            }
            default:
                break
        }
    }
    
}
