//
//  ReservationsViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/19/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class ReservationsViewController: UIViewController {
    //MARK:- @IBOutlet
    @IBOutlet weak var resevationsTableView: UITableView! {
        didSet {
            resevationsTableView.delegate = self
            resevationsTableView.dataSource = self
        }
    }
    var hotels: [hotel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        fetchReservations()
        // Do any additional setup after loading the view.
    }
    func setup() {
        
    }
    
    func fetchReservations() {
        startLoading()
        WebServiceFactory().myReservations { (erorr, data, code) in
            self.stopLoading()
            guard let data = data as? hotelsDataModel else { return }
            if data.data?.count ?? 0 == 0 {
                self.resevationsTableView.isHidden = true
                return
            } else {
                self.resevationsTableView.isHidden = false
            }
            self.hotels.removeAll()
            self.hotels.append(contentsOf: data.data ?? [])
            self.resevationsTableView.reloadData()
        }
    }
}


//MARK:- TableView Delegate

extension ReservationsViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "HotelsTableViewCell", for: indexPath) as! HotelsTableViewCell
        cell.fromReservation = true
        cell.model = hotels[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard.init(name: "Payment", bundle: nil)
        let scene = storyBoard.instantiateViewController(identifier: "PaymentConfirmedViewController") as! PaymentConfirmedViewController
        scene.reserveID = hotels[indexPath.row].id
        scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(scene , animated: true, completion: nil)
    }
    /*  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
     let hotelsDetailsViewController = storyBoard.instantiateViewController(identifier: "HotelDetailsViewController") as! HotelDetailsViewController
     hotelsDetailsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
     self.present(hotelsDetailsViewController , animated: true, completion: nil)
     } */
}

