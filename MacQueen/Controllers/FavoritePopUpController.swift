//
//  PaymentConfirmedViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/20/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import CalendarDateRangePickerViewController
class FavoritePopUpController: UIViewController {
    @IBOutlet var Views: [UIView]!
    @IBOutlet weak var hotelLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var roomsLbl: UILabel!
    
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
    var rooms: [RoomsModel] = []
    var checkoutDate: String?
    var checkinDate: String?
    var hotel: hotels?
    var adultCount: Int = 1
    var delegate: UIViewController?
    //MARK:-  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        dropShadowForAllViews()
    }
    
    private func dropShadowForAllViews(){
        for view in self.Views {
            view.applyShadow(cornerRadius: 20)
        }
    }
    func setup() {
        hotelLbl.text = hotel?.name
        rooms.append(RoomsModel(adultsNumber: 1, childNumber: 0, children_ages: []))
        roomsLbl.text = "\(rooms.count) \("Rooms".localized), \(adultCount) \("Adults".localized), \(0) \("Childs".localized)"
        
        let todayDate = Date()
        guard let tomorrowDate = Calendar.current.date(byAdding: .day , value: 1 , to: todayDate) else { return }
        didTapDoneWithDateRange(startDate: todayDate, endDate: tomorrowDate)
    }
    private func setupCalender() {
        let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        dateRangePickerViewController.minimumDate = Calendar.current.date(byAdding: .day , value: 0 , to: Date())
        
        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 100 , to: dateRangePickerViewController.minimumDate)
        let navController = UINavigationController(rootViewController: dateRangePickerViewController)
        navController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(navController , animated: true, completion: nil)
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: {
           
        })
    }
    @IBAction func changeDate(_ sender: Any) {
        self.setupCalender()
    }
    @IBAction func changeRoom(_ sender: Any) {
        let roomsViewController = storyBoard.instantiateViewController(identifier: "RoomsViewController") as! RoomsViewController
        roomsViewController.delegate = self
        roomsViewController.rooms = rooms
        roomsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(roomsViewController , animated: true, completion: nil)
    }
    @IBAction func proceed(_ sender: Any) {
        let hotelsListViewController = storyBoard.instantiateViewController(identifier: "HotelsListViewController") as! HotelsListViewController
        hotelsListViewController.type = .hotel
        hotelsListViewController.hotel = hotel
        hotelsListViewController.date = dateLbl.text
        hotelsListViewController.checkOutDate = checkoutDate
        hotelsListViewController.checkInDate = checkinDate
        hotelsListViewController.rooms = rooms
        hotelsListViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.dismiss(animated: true) {
            self.delegate?.present(hotelsListViewController, animated: true, completion: nil)
        }
    }
    
    
}
//MARK: - Calender Delegate
extension FavoritePopUpController : CalendarDateRangePickerViewControllerDelegate {
    func didTapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        let fromDate = formatter.string(from: startDate)
        let toDate = formatter.string(from: endDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let dateObj = dateFormatter.date(from: fromDate) else { return }
        checkinDate = dateFormatter.string(from: dateObj)
        let format = DateFormatter()
        format.dateFormat = "E , MMM d"
        let fromDateStr = format.string(from: dateObj)
        guard let toDateObj = dateFormatter.date(from: toDate) else { return }
        checkoutDate = dateFormatter.string(from: toDateObj)
        let toDateStr = format.string(from: toDateObj)
        self.dateLbl.text = fromDateStr + "  -  " + toDateStr
        self.dismiss(animated: true, completion: nil)
    }
}
// MARK:  Rooms delegate
extension FavoritePopUpController: RoomsDelegate {
    func didSelectRooms(rooms: [RoomsModel]) {
        roomsLbl.text = ""
        self.rooms = rooms
        var adultCount = 0
        var childCount = 0
        for item in rooms {
            adultCount += item.adults
            childCount += item.children
        }
        self.adultCount = adultCount
        roomsLbl.text = "\(rooms.count) \("Rooms".localized), \(adultCount) \("Adults".localized), \(childCount) \("Childs".localized)"
        
    }
}
