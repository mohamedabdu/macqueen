//
//  HotelSummaryViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/14/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import Cosmos
class HotelSummaryViewController: UIViewController {

    //MARK:- @IBOutlet
    @IBOutlet weak var promoCodeView: UIView!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var hotelImagesCollectionView: UICollectionView! {
        didSet {
            hotelImagesCollectionView.dataSource = self
            hotelImagesCollectionView.delegate = self
        }
    }
    @IBOutlet weak var minPriceView: UIView!
    @IBOutlet weak var imageCounterLabel: UILabel!
    @IBOutlet weak var hotelTitleLbl: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var hotelNameLbl: UILabel!
    @IBOutlet weak var rate: CosmosView!
    @IBOutlet weak var daysLbl: UILabel!
    @IBOutlet weak var checkinDateLbl: UILabel!
    @IBOutlet weak var checkoutDateLbl: UILabel!
    @IBOutlet weak var dealNameLbl: UILabel!
    @IBOutlet weak var roomsLbl: UILabel!
    @IBOutlet weak var guestsLbl: UILabel!
    @IBOutlet weak var promoTxf: UITextField!
    @IBOutlet weak var priceLbl: UILabel!
    
    var hotel: hotel?
    var date: String?
    var checkoutDate: String?
    var checkinDate: String?
    var rooms: [RoomsModel] = []
    var deal: deals?
    var dealRow: Int?
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        handlers()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        minPriceView.applyShadow(cornerRadius: 10)
        promoCodeView.applyShadow(cornerRadius: 20)
        hotelImagesCollectionView.applyShadow(cornerRadius: 10)
        applyBtn.roundedButton()
    }
    func setup() {
        imageCounterLabel.text = ""
        var adultCount = 0
        for item in rooms {
            adultCount += item.adults
        }
        roomsLbl.text = rooms.count.string
        guestsLbl.text = adultCount.string
        hotelTitleLbl.text = hotel?.hotel_name
        hotelNameLbl.text = hotel?.hotel_name
        rate.rating = hotel?.rate?.double() ?? 0
        checkinDateLbl.text = checkinDate
        checkoutDateLbl.text = checkoutDate
        daysLbl.text = "\(calculateDate()) \("Night".localized)"
        dealNameLbl.text = "\("Deal".localized) \(dealRow ?? 1)"
        priceLbl.text = "SAR\(deal?.price ?? 0)"
        reloadFavorite()
    }
    func handlers() {
        favoriteBtn.UIViewAction {
            if case UserStatus.isLogged = true {
                self.addToFavorite()
            } else {
                UserStatus.loginAlert(in: self)
            }
        }
    }
    func addToFavorite() {
        WebServiceFactory().addFavorite(hotelID: hotel?.hotel_id ?? 0) { (error, data, code) in
            if data != nil {
                self.hotel?.is_favourite = data?.data?.is_favourite
                self.reloadFavorite()
            }
        }
    }
    func reloadFavorite() {
        if case hotel?.is_favourite = true {
            favoriteBtn.setImage(#imageLiteral(resourceName: "HeartFill"), for: .normal)
        } else {
            favoriteBtn.setImage(#imageLiteral(resourceName: "Favorites"), for: .normal)
        }
    }
    func calculateDate() -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        guard let firstDate = dateFormatter.date(from: checkinDate ?? "") else { return 0}
        guard let lastDate = dateFormatter.date(from: checkoutDate ?? "") else { return 0}
        
        let currentCalendar = Calendar.current
        let daysBetween = currentCalendar.dateComponents([.day], from: firstDate, to: lastDate)
        return daysBetween.day ?? 0
    }
    func goToBilling() {
        let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
        let biilingScene = storyBoard.instantiateViewController(identifier: "BillingDetailsViewController") as! BillingDetailsViewController
        biilingScene.hotel = hotel
        biilingScene.date = date
        biilingScene.checkinDate = checkinDate
        biilingScene.checkoutDate = checkoutDate
        biilingScene.rooms = rooms
        biilingScene.deal = deal
        biilingScene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(biilingScene , animated: true, completion: nil)
    }
    @IBAction func share(_ sender: Any) {
    }
    @IBAction func confirm(_ sender: Any) {
        startLoading()
        WebServiceFactory().CheckHotelAvailability(hotelId: hotel?.hotel_id ?? 0, provider: hotel?.provider ?? "", checkin_date: checkinDate ?? "", checkout_date: checkoutDate ?? "", nationality_code: "SA", key: deal?.code ?? "", rooms: rooms) { (error, data, code) in
            self.stopLoading()
            self.goToBilling()
        }
        
    }
}

extension HotelSummaryViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hotel?.images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.hotelImagesCollectionView.dequeueReusableCell(withReuseIdentifier: "HotelImagesSliderCollectionViewCell", for: indexPath) as! HotelImagesSliderCollectionViewCell
        if let url = URL(string: hotel?.images?[indexPath.row] ?? "") {
            cell.imageHotel.kf.setImage(with: url)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == self.hotelImagesCollectionView {
            self.imageCounterLabel.text = "\(indexPath.item + 1)/\(hotel?.images?.count ?? 0)"
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.hotelImagesCollectionView {
            let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
            let scene = storyBoard.instantiateViewController(identifier: "imageCollectionViewController") as! imageCollectionViewController
            scene.imagesArr = hotel?.images ?? []
            scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
            self.present(scene , animated: true, completion: nil)
        }
    }
}


//MARK:- UICollection Delegate Flow layout
extension HotelSummaryViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
            return  CGSize(width: collectionView.bounds.width , height: collectionView.bounds.height)
    }
}
