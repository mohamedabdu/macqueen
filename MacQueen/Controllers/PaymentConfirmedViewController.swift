//
//  PaymentConfirmedViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/20/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import Cosmos
class PaymentConfirmedViewController: UIViewController {

    @IBOutlet weak var hotelNameLbl: UILabel!
    @IBOutlet weak var hotelRate: CosmosView!
    @IBOutlet weak var daysLbl: UILabel!
    @IBOutlet weak var checkInLbl: UILabel!
    @IBOutlet weak var checkoutLbl: UILabel!
    @IBOutlet weak var dealNameLbl: UILabel!
    @IBOutlet weak var roomsLbl: UILabel!
    @IBOutlet weak var adultsLbl: UILabel!
    @IBOutlet weak var visaNumLbl: UILabel!
    @IBOutlet weak var roomCostLbl: UILabel!
    @IBOutlet weak var feesTaxLbl: UILabel!
    @IBOutlet weak var serviceTaxLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var numberLbl: UILabel!
    
    //MARK:-  @IBOutlet
    @IBOutlet weak var guestsNameTableView: UITableView! {
        didSet {
            guestsNameTableView.dataSource = self
            guestsNameTableView.delegate = self
        }
    }
    @IBOutlet weak var guestTableViewHeight: NSLayoutConstraint!
    
    var book: BookModel?
    var hotel: hotel?
    var deal: deals?
    var cardNumber: String?
    var reserveID: Int?
    var rserveModel: ReservationModel.DataClass?
    //MARK:-  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        if reserveID != nil {
            fetchReservation()
        } else {
            setup()
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.guestTableViewHeight.constant = self.guestsNameTableView.contentSize.height
    }
    func setup() {
        hotelNameLbl.text = hotel?.hotel_name
        hotelRate.rating = hotel?.rate?.double() ?? 0
        daysLbl.text = "\(calculateDate()) \("Night".localized)"
        checkInLbl.text = book?.data?.checkinDate
        checkoutLbl.text = book?.data?.checkoutDate
        dealNameLbl.text = "Deal".localized
        roomsLbl.text = "\(book?.data?.rooms?.count.string ?? "") \("Room".localized)"
        var adults = 0
        for room in book?.data?.rooms ?? [] {
            adults += room.adults ?? 0
        }
        adultsLbl.text = "\(adults.string) \("Adult".localized)"
        visaNumLbl.text = cardNumber
        roomCostLbl.text = "SAR\(deal?.price?.string ?? "")"
        feesTaxLbl.text = "SAR\(0)"
        serviceTaxLbl.text = "SAR\(0)"
        totalLbl.text = "SAR\(deal?.price?.string ?? "")"
        numberLbl.text = ""
    }
    func calculateDate() -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
            
        if reserveID != nil {
            guard let firstDate = dateFormatter.date(from: rserveModel?.checkinDate ?? "") else { return 0}
            guard let lastDate = dateFormatter.date(from: rserveModel?.checkoutDate ?? "") else { return 0}
            
            let currentCalendar = Calendar.current
            let daysBetween = currentCalendar.dateComponents([.day], from: firstDate, to: lastDate)
            return daysBetween.day ?? 0
        } else {
            guard let firstDate = dateFormatter.date(from: book?.data?.checkinDate ?? "") else { return 0}
            guard let lastDate = dateFormatter.date(from: book?.data?.checkoutDate ?? "") else { return 0}
            
            let currentCalendar = Calendar.current
            let daysBetween = currentCalendar.dateComponents([.day], from: firstDate, to: lastDate)
            return daysBetween.day ?? 0
        }
       
    }
    func reload() {
        hotelNameLbl.text = rserveModel?.hotel?.name
        hotelRate.rating = rserveModel?.hotel?.rate?.double() ?? 0
        daysLbl.text = "\(calculateDate()) \("Night".localized)"
        checkInLbl.text = rserveModel?.checkinDate
        checkoutLbl.text = rserveModel?.checkoutDate
        dealNameLbl.text = "Deal".localized
        roomsLbl.text = "\(rserveModel?.rooms?.count.string ?? "") \("Room".localized)"
        var adults = 0
        for room in rserveModel?.rooms ?? [] {
            adults += room.adults ?? 0
        }
        adultsLbl.text = "\(adults.string) \("Adult".localized)"
        visaNumLbl.text = "**** **** **** ****"
        roomCostLbl.text = "SAR\(rserveModel?.price ?? "")"
        feesTaxLbl.text = "SAR\(0)"
        serviceTaxLbl.text = "SAR\(0)"
        totalLbl.text = "SAR\(rserveModel?.price ?? "")"
        numberLbl.text = ""
    }
    func fetchReservation() {
        startLoading()
        WebServiceFactory().reservationDetail(id: reserveID) { (error, data, code) in
            self.stopLoading()
            if error != nil {
                
            }  else {
                self.rserveModel = data?.data
                self.reload()
            }
        }
    }
    @IBAction func done(_ sender: Any) {
        if reserveID != nil {
            RootWindowController.setRootWindowForHome()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}


//MARK:-  TableView Delegate
extension PaymentConfirmedViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return book?.data?.rooms?.count ?? 0
    }
    
    //MARK:-  TableView Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "guestsNamesTableViewCell", for: indexPath) as! guestsNamesTableViewCell
        cell.model = book?.data?.rooms?[indexPath.row].leadPassenger
        return cell
    }
}
