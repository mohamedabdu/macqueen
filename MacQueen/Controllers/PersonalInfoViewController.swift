//
//  PersonalInfoViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/19/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class PersonalInfoViewController: UIViewController {
    @IBOutlet weak var firstnameTxf: UITextField!
    @IBOutlet weak var lastnameTxf: UITextField!
    @IBOutlet weak var emailTxf: UITextField!
    @IBOutlet weak var mobileTxf: UITextField!
    
    var closure: HandlerView?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        // Do any additional setup after loading the view.
    }
    func setup() {
        firstnameTxf.text = UserStatus.firstName
        lastnameTxf.text = UserStatus.lastName
        emailTxf.text = UserStatus.email
        mobileTxf.text = UserStatus.mobile
        if emailTxf.text != nil && emailTxf.text != "" && emailTxf.text != " " {
            emailTxf.isUserInteractionEnabled = false
        }
        mobileTxf.isUserInteractionEnabled = false
    }

    @IBAction func save(_ sender: Any) {
        var paramters: [String: Any] = [:]
        paramters["email"] = emailTxf.text
        paramters["first_name"] = firstnameTxf.text
        paramters["last_name"] = lastnameTxf.text
        paramters["mobile"] = mobileTxf.text
        startLoading()
        WebServiceFactory().UpdateProfile(paramters: paramters) { (error, user, code) in
            self.stopLoading()
            if error != nil {
                self.showAlert(title: "Alert".localized, message: error?.eMsg)
            } else {
                UserStatus.email = user?.data?.email
                UserStatus.firstName = user?.data?.firstName
                UserStatus.lastName = user?.data?.lastName
                UserStatus.mobile = user?.data?.mobile
                self.makeAlert("Successfully updated".localized) {
                    self.closure?()
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
}
