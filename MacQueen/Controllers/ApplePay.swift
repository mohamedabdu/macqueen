import UIKit
import PassKit
import Frames

class ApplePayController: UIViewController,
PKPaymentAuthorizationViewControllerDelegate {
   
    
    @IBOutlet weak var applePay: UIButton!
    
    
    var checkoutAPIClient: CheckoutAPIClient?
    
    let merchantId = "merchant.com.zeidex.MacQueen"
    
    var customerCardList: CustomerCardList?
    var selectedCard: Any?
 

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //applePay.addTarget(self, action: #selector(onTouchApplePayButton), for: .touchUpInside)
        getPaymentToken()
    }
    func getPaymentToken() {
        WebServiceFactory().makePaymentSdk { (error, data, code) in
            self.checkoutAPIClient = CheckoutAPIClient(publicKey: data?.data?.public_key ?? "",
                                                       environment: .sandbox)
        }
    }
    // View Controller
    
    @objc func onTouchApplePayButton() {
        if PKPaymentAuthorizationController.canMakePayments() {
            let paymentRequest = createPaymentRequest()
            if let applePayViewController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest) {
                applePayViewController.delegate = self
                self.present(applePayViewController, animated: true)
            }
        }
    }
    
    private func createPaymentRequest() -> PKPaymentRequest {
        let paymentRequest = PKPaymentRequest()
        
        paymentRequest.currencyCode = "SAR"
        paymentRequest.countryCode = "SA"
        paymentRequest.merchantIdentifier = merchantId
        paymentRequest.supportedNetworks = [.visa, .masterCard, .amex, .discover, .JCB]
        paymentRequest.merchantCapabilities = .capability3DS
        paymentRequest.paymentSummaryItems = getProductsToSell(amount: 100)
        
        let sameDayShipping = PKShippingMethod(label: "Same Day Shipping", amount: 4.99)
        sameDayShipping.detail = "Same day guaranteed delivery"
        sameDayShipping.identifier = "sameDay"
        
        let twoDayShipping = PKShippingMethod(label: "Two Day Shipping", amount: 2.99)
        twoDayShipping.detail = "Delivered within the following 2 days"
        twoDayShipping.identifier = "twoDay"
        
        let oneWeekShipping = PKShippingMethod(label: "Same day", amount: 0.99)
        oneWeekShipping.detail = "Delivered within 1 week"
        oneWeekShipping.identifier = "oneWeek"
        
        //paymentRequest.shippingMethods = [sameDayShipping, twoDayShipping, oneWeekShipping]
        paymentRequest.shippingMethods = []
        
        return paymentRequest
    }
    
    private func getProductsToSell(amount: Double) -> [PKPaymentSummaryItem] {
        let demoProduct1 = PKPaymentSummaryItem(label: "Deal".localized, amount: NSDecimalNumber(value: amount))
        //let demoDiscount = PKPaymentSummaryItem(label: "Demo Discount", amount: 2.99)
        //let shipping = PKPaymentSummaryItem(label: "Shipping", amount: 14.99)
        
        let totalAmount = demoProduct1.amount
        let totalPrice = PKPaymentSummaryItem(label: "Checkout.com", amount: totalAmount)
        
        return [demoProduct1, totalPrice]
    }
    
    // Delegate method Apple Pay
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController,
                                            didAuthorizePayment payment: PKPayment,
                                            handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        checkoutAPIClient?.createApplePayToken(paymentData: payment.token.paymentData, successHandler: { applePayToken in
            print(applePayToken)
            completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
        }, errorHandler: { error in
            completion(PKPaymentAuthorizationResult(status: .failure, errors: nil))
            print(error)
        })
    }
   
    @IBAction func backbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

