//
//  HotelsListViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/14/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import GoogleMaps
import Cosmos
class HotelsListViewController: UIViewController {
    enum Tab {
        case list
        case map
    }
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var checkoutDateLbl: UILabel!
    @IBOutlet weak var roomsLbl: UILabel!
    @IBOutlet weak var adultLbl: UILabel!
    //MARK:- @IBOutlet
    @IBOutlet weak var hotelView: UIView!
    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var hotelFavorite: UIButton!
    @IBOutlet weak var hotelName: UILabel!
    @IBOutlet weak var hotelPrice: UILabel!
    @IBOutlet weak var hotelRate: CosmosView!
    @IBOutlet weak var viewDealBtn: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var hotelsTableView: UITableView! {
        didSet {
            hotelsTableView.delegate = self
            hotelsTableView.dataSource = self
        }
    }
    //MARK:- Variables
    let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
    var viewType: Tab = .list
    var type: SearchInputsViewController.SearchType = .city
    var city: cities?
    var hotel: hotels?
    var date: String?
    var checkInDate: String?
    var checkOutDate: String?
    var rooms: [RoomsModel] = []
    var hotelsIDS: [Int] = []
    var page: Int = 1
    var hotels: [hotel] = []
    var oldHotels: [hotel] = []
    var chunkedArray: [[Int]] = []
    var stopLoadingApi: Bool = false
    var mapHelper: GoogleMapHelper?
    var locationHelper: LocationHelper?
    var loadedBounds: Bool = false
    var fromSeeAll: Bool = false
    var closureSearchInput: HandlerView?
    var filter: FilterModel?
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        handleView()
        setupMap()
        setupLocation()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        stopLoadingApi = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopLoadingApi = true
        closureSearchInput?()
    }
    func setup() {
        var adultCount = 0
        for item in rooms {
            adultCount += item.adults
        }
        checkoutDateLbl.text = date
        roomsLbl.text = rooms.count.string
        adultLbl.text = adultCount.string
        switch type {
            case .city:
            setupCity()
            case .hotel:
            setupHotel()
        }
        if fromSeeAll {
            reload()
        }
    }
    func setupHotel() {
        titleLbl.text = hotel?.name
        startLoading()
        WebServiceFactory().GetHotels(currency: "EGP", lang: Localizer.current, checkin_date: checkInDate ?? "", checkout_date: checkOutDate ?? "", nationality_code: "SA", hotelsIds: [hotel?.id ?? 0], rooms: rooms) { (error, data, code) in
            self.stopLoading()
            guard let data = data as? hotelsDataModel else { return }
            if data.data?.count == 0 {
                self.makeAlert("No hotel found with these specification".localized, closure: {
                    self.dismiss(animated: true, completion: nil)
                })
            }
            self.hotels.append(contentsOf: data.data ?? [])
            self.oldHotels.append(contentsOf: data.data ?? [])
            self.reload()
        }
    }
    func setupCity() {
        titleLbl.text = city?.name
        startLoading()
        WebServiceFactory().GetHotelsByCityId(cityId: city?.id ?? 0) { (error, data, code) in
            self.stopLoading()
            if code > 200 {
                return
            }
            guard let data = data as? GetHotelsByCityIdModel else { return }
            self.hotelsIDS.append(contentsOf: data.data ?? [])
            self.chunkedArray = self.hotelsIDS.chunked(into: 20)
            self.loadHotels()
        }
    }
    func loadHotels() {
        if stopLoadingApi {
            return
        }
        let array = chunkedArray[safe: page-1]
        if array?.count ?? 0 < 1 {
            if self.oldHotels.count == 0 {
                self.showAlert(title: "Alert".localized, message: "There are no hotels found".localized)
            }
            return
        }
        WebServiceFactory().GetHotels(currency: "EGP", lang: Localizer.current, checkin_date: checkInDate ?? "", checkout_date: checkOutDate ?? "", nationality_code: "SA", hotelsIds: array ?? [], rooms: rooms) { (error, data, code) in
            guard let data = data as? hotelsDataModel else { return }
            self.hotels.append(contentsOf: data.data ?? [])
            self.oldHotels.append(contentsOf: data.data ?? [])
            self.reload()
            self.page += 1
            self.loadHotels()
        }
    }
    func reload() {
        if viewType == .list {
            self.hotelsTableView.reloadData()
        } else {
            mapHelper?.refresh()
        }
    }
    
     //MARK:- @IBAction
    @IBAction func filterClicked(_ sender: UIButton) {
     

        let searchViewController = storyBoard.instantiateViewController(identifier: "FilterViewController") as! FilterViewController
        searchViewController.delegate = self
        searchViewController.dataSource = self
        searchViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(searchViewController , animated: true, completion: nil)
    }
    @IBAction func mapViewClicked(_ sender: UIButton) {
        if viewType == .list {
            sender.setTitle("List View".localized, for: .normal)
            viewType = .map
        } else {
            sender.setTitle("Map View".localized, for: .normal)
            viewType = .list
        }
        handleView()
        reload()
    }
}
// MARK:  Handle View
extension HotelsListViewController {
    func handleView() {
        if viewType == .list {
            mapView.isHidden = true
            hotelsTableView.isHidden = false
        } else {
            mapView.isHidden = false
            hotelsTableView.isHidden = true
        }
    }
}
// MARK:  MapView
extension HotelsListViewController: GoogleMapHelperDelegate, MarkerDataSource {
    func setupMap() {
        mapHelper = .init()
        mapHelper?.delegate = self
        mapHelper?.markerDataSource = self
        mapHelper?.mapView = mapView
        mapHelper?.zoom = .cityIn
    }
    func setupLocation() {
//        locationHelper = .init()
//        locationHelper?.onUpdateLocation = { degree in
//            self.mapHelper?.updateCamera(lat: degree?.latitude ?? 0, lng: degree?.longitude ?? 0)
//        }
//        locationHelper?.currentLocation()
    }
    func setMarkers() -> [GMSMarker] {
        var markers: [GMSMarker] = []
        var bounds = GMSCoordinateBounds()
        for hotel in hotels {
            let view = Bundle.main.loadNibNamed("MarkerView", owner: nil, options: nil)?.first as? MarkerView
            let viewSub = view?.containView as? MarkerView
            viewSub?.priceLbl.text = "SAR\(hotel.min_price ?? 0)"
            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: hotel.latitude?.double() ?? 0, longitude: hotel.longitude?.double() ?? 0))
            marker.title = "SAR\(hotel.min_price?.string ?? "")"
            marker.iconView = viewSub
            marker.userData = hotel
            markers.append(marker)
            
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        if !loadedBounds && markers.count > 0 {
            mapView.setMinZoom(1, maxZoom: 10)//prevent to over zoom on fit and animate if bounds be too small
            
            let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
            mapView.animate(with: update)
            
            mapView.setMinZoom(1, maxZoom: 20) // allow the user zoom in more than level 15 again
            loadedBounds = true
        }
        
        return markers
    }
    func marker() -> MarkerAttrbuite {
        let view = Bundle.main.loadNibNamed("HotelView", owner: nil, options: nil)?.first as? UIView
        var attr = MarkerAttrbuite()
        attr.use = .view
        attr.iconView = view?.subviews.first
        return attr
    }
    func didChangeCameraLocation(lat: Double, lng: Double) {
        mapHelper?.refresh()
    }
    func didTapOnMarker(marker: GMSMarker) {
        let data = marker.userData as? hotel
        let url = URL(string: data?.image ?? "")
        hotelImage.kf.setImage(with: url)
        hotelName.text = data?.hotel_name
        hotelPrice.text = "SAR\(data?.min_price ?? 0)"
        hotelRate.rating = data?.rate?.double() ?? 0
        viewDealBtn.UIViewAction {
            self.goToHotelDeals(hotel: data)
        }
        hotelView.UIViewAction {
            self.goToHotelDetails(hotel: data)
        }
        hotelFavorite.UIViewAction {
            if case UserStatus.isLogged = true {
                
            } else {
                UserStatus.loginAlert(in: self)
            }
        }
        mapHelper?.updateCamera(lat: marker.position.latitude, lng: marker.position.longitude)
        hotelView.isHidden = false
    }
    func didTapOnMap(lat: Double, lng: Double) {
        hotelView.isHidden = true
    }
    func goToHotelDetails(hotel: hotel?) {
        guard let hotel = hotel else { return }
        let hotelsDetailsViewController = storyBoard.instantiateViewController(identifier: "HotelDetailsViewController") as! HotelDetailsViewController
        hotelsDetailsViewController.hotel = hotel
        hotelsDetailsViewController.date = date
        hotelsDetailsViewController.checkinDate = checkInDate
        hotelsDetailsViewController.checkoutDate = checkOutDate
        hotelsDetailsViewController.rooms = rooms
        hotelsDetailsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(hotelsDetailsViewController , animated: true, completion: nil)
    }
    func goToHotelDeals(hotel: hotel?) {
        guard let hotel = hotel else { return }
        let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
        let dealsViewController = storyBoard.instantiateViewController(identifier: "DealsViewController") as! DealsViewController
        dealsViewController.hotel = hotel
        dealsViewController.date = self.date
        dealsViewController.checkinDate = self.checkInDate
        dealsViewController.checkoutDate = self.checkOutDate
        dealsViewController.rooms = self.rooms
        dealsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(dealsViewController , animated: true, completion: nil)
    }
}
//MARK:- TableView Delegate

extension HotelsListViewController : UITableViewDelegate , UITableViewDataSource {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "HotelsTableViewCell", for: indexPath) as! HotelsTableViewCell
        cell.model = hotels[indexPath.row]
        if case hotels[indexPath.row].is_favourite = true {
            cell.favoritesBtn.setImage(#imageLiteral(resourceName: "HeartFill"), for: .normal)
        } else {
            cell.favoritesBtn.setImage(#imageLiteral(resourceName: "Favorites"), for: .normal)
        }
        cell.closureOpenDeals = {
            self.goToHotelDeals(hotel: self.hotels[indexPath.row])
        }
        cell.favoritesBtn.UIViewAction {
            if case UserStatus.isLogged = true {
                self.addToFavorite(path: indexPath.row)
            } else {
                UserStatus.loginAlert(in: self)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.goToHotelDetails(hotel: self.hotels[indexPath.row])
    }
    func addToFavorite(path: Int) {
        WebServiceFactory().addFavorite(hotelID: hotels[path].hotel_id ?? 0) { (error, data, code) in
            if data != nil {
                self.hotels[path].is_favourite = data?.data?.is_favourite
                self.hotelsTableView.reloadData()
            }
        }
    }
}
extension HotelsListViewController: FilterDelegate, FilterDataSource {
    func rangePrices() -> (Int, Int) {
        var minPrice = 0
        var maxPrice = 0
        var pricesArray: [Int] = []
        for hotel in oldHotels {
            pricesArray.append(hotel.min_price ?? 0)
        }
        minPrice = pricesArray.min() ?? 0
        maxPrice = pricesArray.max() ?? 0
        if minPrice == maxPrice {
            minPrice = 0
        }
        return (minPrice, maxPrice)
    }
    func filterModel() -> FilterModel? {
        return filter
    }
    func didReset() {
        filter = nil
        hotels.removeAll()
        self.hotels.append(contentsOf: self.oldHotels)
        self.hotelsTableView.reloadData()
    }
    func didFilter(ammenties: [Int]?, types: [Int]?, travel: [Int]?, rate: Double?, minPrice: Int?, maxPrice: Int?) {
        filter = .init()
        var hotelFound: Bool = false
        var counter = 0
        hotels.removeAll()
        for hotel in oldHotels {
            hotelFound = false
            if filterAmenties(array: ammenties, hotel: hotel) {
                hotelFound = true
            }
            if types?.count ?? 0 > 0 {
                filter?.typesSelected = types ?? []
                if types?.contains(hotel.type_id ?? 0) ?? false {
                    hotelFound = true
                }
            }
            if travel?.count ?? 0 > 0 {
                filter?.travelSelected = travel ?? []
                if travel?.contains(hotel.travel_style_id ?? 0) ?? false {
                    hotelFound = true
                }
            }
            if let rate = rate {
                filter?.rate = rate
                if hotel.rate?.double() ?? 0 == rate {
                    hotelFound = true
                }
            }
            
            if let minPrice = minPrice, let maxPrice = maxPrice {
                filter?.minPrice = minPrice
                filter?.maxPrice = maxPrice

                if hotel.min_price ?? 0 >= minPrice && hotel.min_price ?? 0 <= maxPrice {
                    hotelFound = true
                }
            }
           
            if hotelFound {
                hotels.append(hotel)
            }
            counter += 1
        }
        if self.hotels.count == 0 {
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.makeAlert("There are no results compatible with the filter. Do you want to filter again?".localized, cancel: true, closure: {
                    
                    let searchViewController = self.storyBoard.instantiateViewController(identifier: "FilterViewController") as! FilterViewController
                    searchViewController.delegate = self
                    searchViewController.dataSource = self
                    searchViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
                    self.present(searchViewController , animated: true, completion: nil)
                    
                }, cancelClosure: {
                    self.hotels.append(contentsOf: self.oldHotels)
                    self.hotelsTableView.reloadData()
                })
            }
        }
        hotelsTableView.reloadData()

    }
    func filterAmenties(array: [Int]?, hotel: hotel) -> Bool {
        if array?.count ?? 0 > 0 {
            filter?.ammentiesSelected = array ?? []
            for item in hotel.amenities ?? [] {
                if array?.contains(item.id ?? 0) ?? false {
                    return true
                }
            }
            return false
        } else {
            return false
        }
    }
}
