//
//  LoginViewController.swift
//  MacQueen
//
//  Created by Kareem on 4/16/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import ActionPicker


class LoginViewController: UIViewController {

    
    //MARK:- @IBOutlet
    @IBOutlet var loginViews: [UIView]!
    @IBOutlet weak var emailTxf: UITextField!
    @IBOutlet weak var passwordTxf: UITextField!
    
    var isOTPEmail: Bool = false
    var mobile: String?
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupFont(view: view)
    }
    override func viewDidLayoutSubviews() {
     super.viewDidLayoutSubviews()
        dropShadowForAllViews()
     }
    //MARK:- Methods
    func setup() {
        if mobile != nil {
            emailTxf.text = mobile
        }
    }
    private func dropShadowForAllViews(){
        for view in self.loginViews {
            view.applyShadow(cornerRadius: 30)
        }
    }
    func validate(txfs: [UITextField]) -> Bool {
        var validate = true
        txfs.forEach { (item) in
            if case item.text?.isEmpty = true {
                item.attributedPlaceholder = NSAttributedString(string: item.placeholder ?? "",
                                                                attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
                validate = false
            }
        }
        return validate
    }
    func createUser() -> UserModel? {
        let user = UserModel(data: UserModel.Datum(id: nil, firstName: nil, lastName: nil, email: emailTxf.text, mobile: emailTxf.text, billingCountry: nil, billingCity: nil, billingState: nil, billingStreet: nil, billingPostalCode: nil, createdAt: nil), message: nil, meta: nil, token: nil, errors: nil)
        return user
    }
    @IBAction func loginClicked(_ sender: UIButton) {
        if !validate(txfs: [emailTxf, passwordTxf]) {
            return
        }
        var paramters: [String: Any] = [:]
        if case emailTxf.text?.isEmail = true {
            paramters["email"] = emailTxf.text
            isOTPEmail = true
        } else {
            paramters["mobile"] = emailTxf.text
            isOTPEmail = false
        }
        paramters["password"] = passwordTxf.text
        startLoading()
        WebServiceFactory().Login(paramters: paramters) { (error, user, code) in
            self.stopLoading()
            if error != nil {
                if code == 403 {
                    let scene = self.controller(OTPViewController.self, storyboard: .auth)
                    scene.forEmail = self.isOTPEmail
                    scene.user = self.createUser()
                    scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
                    self.present(scene, animated: true, completion: nil)
                } else {
                    self.showAlert(title: "Alert".localized, message: error?.eMsg)
                }
            } else {
                UserStatus.isLogged = true
                UserStatus.email = user?.data?.email
                UserStatus.firstName = user?.data?.firstName
                UserStatus.lastName = user?.data?.lastName
                UserStatus.mobile = user?.data?.mobile
                UserStatus.token = user?.token
                RootWindowController.setRootWindowForHome()
            }
           
        }

    }
    func setupShowBtn(_ sender: UIButton) {
        var text = sender.title(for: .normal)
        if text == "Show".localized {
            text = "Hide".localized
        } else {
            text = "Show".localized
        }
        sender.setTitle(text, for: .normal)
    }
    @IBAction func showPassword(_ sender: UIButton) {
        setupShowBtn(sender)
        passwordTxf.isSecureTextEntry = !passwordTxf.isSecureTextEntry
    }
    @IBAction func forgetPassword(_ sender: Any) {
        let scene = controller(ForgetPasswordController.self, storyboard: .auth)
        scene.delegate = self
        scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(scene, animated: true, completion: nil)
    }
    @IBAction func loginOptions(_ sender: Any) {
        RootWindowController.setRootWindowForHome()
    }
    @IBAction func signUp(_ sender: Any) {
        let scene = controller(SignUpViewController.self, storyboard: .auth)
        scene.isRegister = true
        scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(scene, animated: true, completion: nil)
    }
    
}
