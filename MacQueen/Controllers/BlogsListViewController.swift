//
//  BlogsViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/19/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class BlogsListViewController: UIViewController {
    
    @IBOutlet weak var blogsTableView: UITableView! {
        didSet {
            blogsTableView.delegate = self
            blogsTableView.dataSource = self
        }
    }
    var blogs: [BlogModel.Datum] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        // Do any additional setup after loading the view.
    }
    
}

//MARK:- TableView Delegate

extension BlogsListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blogs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "blogsTableViewCell", for: indexPath) as! blogsTableViewCell
        cell.model = blogs[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
        let blogsDetailsViewController = storyBoard.instantiateViewController(identifier: "BlogsDetailsViewController") as! BlogsDetailsViewController
        blogsDetailsViewController.blog = blogs[indexPath.row]
        blogsDetailsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(blogsDetailsViewController , animated: true, completion: nil)
    }
}
