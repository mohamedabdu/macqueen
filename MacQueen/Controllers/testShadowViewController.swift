//
//  testShadowViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/13/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class testShadowViewController: UIViewController {

    @IBOutlet weak var shadowBtn: RoundShadowView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let shadow = UIBezierPath(roundedRect: self.shadowBtn.bounds , cornerRadius: 30).cgPath
        shadowBtn.layer.cornerRadius = 30
        shadowBtn.backgroundColor = .white
        shadowBtn.layer.shadowRadius = 5
        shadowBtn.layer.shadowOffset = .init(width: 2 , height: 2)
        shadowBtn.layer.shadowColor = UIColor.black.cgColor
        shadowBtn.layer.shadowOpacity = 0.8
        shadowBtn.layer.masksToBounds = false
        shadowBtn.layer.shadowPath = shadow
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
