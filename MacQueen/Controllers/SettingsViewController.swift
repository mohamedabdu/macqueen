//
//  SettingsViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/18/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    
    //MARK: Variables
    let storyBoard = UIStoryboard(name: "Updated", bundle: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserStatus.loginAlert(in: self, backToHome: true)
        setup()
    }
    func setup() {
        nameLbl.text = "\(UserStatus.firstName ?? "") \(UserStatus.lastName ?? "")"
        emailLbl.text = "\(UserStatus.email ?? "")"
    }
    //MARK: @IBAction
    @IBAction func personalIfoClicked(_ sender: UIButton) {
        let personalInfoViewController = storyBoard.instantiateViewController(identifier: "PersonalInfoViewController") as! PersonalInfoViewController
        personalInfoViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        personalInfoViewController.closure = {
            self.setup()
        }
        self.present(personalInfoViewController , animated: true, completion: nil)
    }
    @IBAction func myReservationClicked(_ sender: UIButton) {
        let reservationsViewController = storyBoard.instantiateViewController(identifier: "ReservationsViewController") as! ReservationsViewController
        reservationsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(reservationsViewController , animated: true, completion: nil)

    }
    @IBAction func logout(_ sender: Any) {
        UserStatus.isLogged = false
        UserStatus.token = nil
        RootWindowController.setRootWindowForHome()
    }
    @IBAction func language(_ sender: Any) {
        changeLang( delegate: self, closure: {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                initLang()
                RootWindowController.setRootWindowForHome()
                initLang()
            }
        })
    }
    @IBAction func creditCards(_ sender: Any) {
        let scene = controller(UserSavedCardsViewController.self, storyboard: .updated)
        scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(scene, animated: true, completion: nil)
    }
    @IBAction func changePassword(_ sender: Any) {
        let scene = controller(ChangePasswordController.self, storyboard: .auth)
        scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(scene, animated: true, completion: nil)
    }
}
