//
//  FavoritesViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/19/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {

    //MARK:- @IBOutlet
    @IBOutlet weak var favoritesTableView: UITableView! {
        didSet {
            favoritesTableView.delegate = self
            favoritesTableView.dataSource = self
        }
    }
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    var checkoutDate: String?
    var checkinDate: String?
    var rooms: [RoomsModel] = []
    var date: String?
    var hotelsArray: [hotel] = []
    var hotel: hotels?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserStatus.loginAlert(in: self, backToHome: true)
        setup()
        fetchFavorite()
    }
    func setup() {
        rooms.append(RoomsModel(adultsNumber: 1, childNumber: 0, children_ages: []))
        let todayDate = Date()
        guard let tomorrowDate = Calendar.current.date(byAdding: .day , value: 1 , to: todayDate) else { return }
        didTapDoneWithDateRange(startDate: todayDate, endDate: tomorrowDate)
    }
    func fetchFavorite() {
        startLoading()
        WebServiceFactory().favorites { (erorr, data, code) in
            self.stopLoading()
            if data?.data?.count ?? 0 == 0 {
                self.favoritesTableView.isHidden = true
                return
            } else {
                self.favoritesTableView.isHidden = false
            }
            self.hotelsArray.removeAll()
            self.hotelsArray.append(contentsOf: data?.data ?? [])
            self.favoritesTableView.reloadData()
        }
    }
}
extension FavoritesViewController {
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        let fromDate = formatter.string(from: startDate)
        let toDate = formatter.string(from: endDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let dateObj = dateFormatter.date(from: fromDate) else { return }
        checkinDate = dateFormatter.string(from: dateObj)
        let format = DateFormatter()
        format.dateFormat = "E , MMM d"
        let fromDateStr = format.string(from: dateObj)
        guard let toDateObj = dateFormatter.date(from: toDate) else { return }
        checkoutDate = dateFormatter.string(from: toDateObj)
        let toDateStr = format.string(from: toDateObj)
        self.date = fromDateStr + "  -  " + toDateStr
    }
}


//MARK:- TableView Delegate

extension FavoritesViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotelsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "HotelsTableViewCell", for: indexPath) as! HotelsTableViewCell
        cell.isComingFromFav = true
        cell.model = hotelsArray[indexPath.row]
        cell.favoritesBtn.setImage(#imageLiteral(resourceName: "HeartFill"), for: .normal)
        cell.favoritesBtn.UIViewAction {
            if case UserStatus.isLogged = true {
                self.addToFavorite(path: indexPath.row)
            } else {
                UserStatus.loginAlert(in: self)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
//        let hotelsDetailsViewController = storyBoard.instantiateViewController(identifier: "HotelDetailsViewController") as! HotelDetailsViewController
//        hotelsDetailsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
//        hotelsDetailsViewController.hotel = hotels[indexPath.row]
//        hotelsDetailsViewController.date = date
//        hotelsDetailsViewController.checkinDate = checkinDate
//        hotelsDetailsViewController.checkoutDate = checkoutDate
//        hotelsDetailsViewController.rooms = rooms
//        self.present(hotelsDetailsViewController , animated: true, completion: nil)
        self.hotel = hotels()
        hotel?.id = hotelsArray[indexPath.row].id
        hotel?.name = hotelsArray[indexPath.row].hotel_name
        let scene = controller(FavoritePopUpController.self, storyboard: .auth)
        scene.hotel = hotel
        scene.delegate = self
        pushPop(vcr: scene)
    }
    func addToFavorite(path: Int) {
        let id = hotelsArray[path].id ?? 0
        self.hotelsArray.remove(at: path)
        self.favoritesTableView.reloadData()
        WebServiceFactory().addFavorite(hotelID: id) { (error, data, code) in
            if data != nil {
            }
        }
    }
}
