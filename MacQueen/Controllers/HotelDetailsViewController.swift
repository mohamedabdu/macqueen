//
//  HotelDetailsViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/13/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import CalendarDateRangePickerViewController
import Cosmos

class HotelDetailsViewController: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var checkoutView: UIView!
    @IBOutlet weak var checkInView: UIView!
    @IBOutlet weak var hotelTitleLbl: UILabel!
    @IBOutlet weak var hotelNameLbl: UILabel!
    @IBOutlet weak var hotelRate: CosmosView!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var checkDate: UILabel!
    @IBOutlet weak var roomsLbl: UILabel!
    @IBOutlet weak var adultLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var heightAmmentitiesCollectionView: NSLayoutConstraint!
    @IBOutlet weak var heightFacilitiesCollectionView: NSLayoutConstraint!
    @IBOutlet weak var minPriceView: UIView!
    @IBOutlet weak var imagesCollectionView: UICollectionView!{
        didSet {
            imagesCollectionView.dataSource = self
            imagesCollectionView.delegate = self
        }
    }
    @IBOutlet weak var facilitiesCollectionView: UICollectionView! {
        didSet {
            facilitiesCollectionView.dataSource = self
            facilitiesCollectionView.delegate = self
        }
    }
    @IBOutlet weak var ammentitiesCollectionView: UICollectionView! {
        didSet {
            ammentitiesCollectionView.delegate = self
            ammentitiesCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var imageCounterLabel: UILabel!
    @IBOutlet weak var fromDateLabel: UILabel!
    @IBOutlet weak var toDateLabel: UILabel!
    
    //MARK: - Variables
    var currentIndex = 0
    var hotel: hotel?
    var date: String?
    var checkoutDate: String?
    var checkinDate: String?
    var rooms: [RoomsModel] = []
    var ammenetiesExpanded: Bool = false
    var descrpitionExpanded: Bool = false
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    var selectedDates = [String]()

    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        self.heightFacilitiesCollectionView.constant = self.facilitiesCollectionView.collectionViewLayout.collectionViewContentSize.height
        
        
        self.heightAmmentitiesCollectionView.constant = self.ammentitiesCollectionView.collectionViewLayout.collectionViewContentSize.height
        self.view.setNeedsLayout()
        setup()
        setupDesc(isLimit: true)
        handlers()
        setupFormatDate()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        minPriceView.applyShadow(cornerRadius: 10)
        imagesCollectionView.applyShadow(cornerRadius: 10)
    }
    func setup() {
        descriptionLbl.numberOfLines = 5
        imageCounterLabel.text = ""
        var adultCount = 0
        for item in rooms {
            adultCount += item.adults
        }
        checkDate.text = date
        roomsLbl.text = rooms.count.string
        adultLbl.text = adultCount.string
        hotelTitleLbl.text = hotel?.hotel_name
        hotelNameLbl.text = hotel?.hotel_name
        hotelRate.rating = hotel?.rate?.double() ?? 0
        locationLbl.text = hotel?.address
        //descriptionLbl.text = hotel?.description
        priceLbl.text = "SAR\(hotel?.min_price?.string ?? "")"
        imagesCollectionView.reloadData()
        ammentitiesCollectionView.reloadData()
        facilitiesCollectionView.reloadData()
        reloadFavorite()
    }
    func setupDesc(isLimit: Bool = false) {
        let descModel: hotel.DescriptionModel? = hotel?.description
        let desc: String = "\n"
        let normalString = NSMutableAttributedString(string: desc)
        let optinalString = NSMutableAttributedString(string: "")
        //descModel = try? JSONDecoder().decode(DescriptionModel.self, from: hotel?.description?.data(using: .utf8) ?? Data())
    
        var attrIntroString = NSMutableAttributedString(string:"", attributes:[NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)])

        if descModel?.hotel_introduction != nil {
            let totalText = "Hotel Introduction"
            let attrIntro = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)]
            attrIntroString = NSMutableAttributedString(string:totalText, attributes:attrIntro)
            attrIntroString.append(normalString)
            if isLimit {
                attrIntroString.append(NSMutableAttributedString(string: String(descModel?.hotel_introduction?.htmlToString.prefix(100) ?? "\n")))
            } else {
                attrIntroString.append(descModel?.hotel_introduction?.htmlToAttributedString ?? optinalString)
            }

        }
       
        if descModel?.hotel_information != nil {
            let infoText = "Hotel Information"
            let attrInfo = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)]
            let attrInfoString = NSMutableAttributedString(string:infoText, attributes:attrInfo)
            attrIntroString.append(NSMutableAttributedString(string: "\n"))
            attrIntroString.append(attrInfoString)
            attrIntroString.append(NSMutableAttributedString(string: "\n"))
            
            if isLimit {
                attrIntroString.append(NSMutableAttributedString(string: String(descModel?.hotel_information?.htmlToString.prefix(100) ?? "\n")))
            } else {
                attrIntroString.append(descModel?.hotel_information?.htmlToAttributedString ?? optinalString)
            }
        }
    
        if descModel?.hotel_amenity != nil {
            let amenText = "Hotel Amenity"
            let attrAmen = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)]
            let attrImenString = NSMutableAttributedString(string:amenText, attributes:attrAmen)
            attrIntroString.append(NSMutableAttributedString(string: "\n"))
            attrIntroString.append(attrImenString)
            attrIntroString.append(NSMutableAttributedString(string: "\n"))
            
            if isLimit {
                attrIntroString.append(NSMutableAttributedString(string: String(descModel?.hotel_amenity?.htmlToString.prefix(100) ?? "\n")))
            } else {
                attrIntroString.append(descModel?.hotel_amenity?.htmlToAttributedString ?? optinalString)
            }
            
        }
       

        if descModel?.room_amenity != nil {
            let roomText = "Rooms"
            let attrRoom = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)]
            let attrRoomString = NSMutableAttributedString(string:roomText, attributes:attrRoom)
            attrIntroString.append(NSMutableAttributedString(string: "\n"))
            attrIntroString.append(attrRoomString)
            attrIntroString.append(NSMutableAttributedString(string: "\n"))
            attrIntroString.append(descModel?.room_amenity?.htmlToAttributedString ?? optinalString)
            
            if isLimit {
                attrIntroString.append(NSMutableAttributedString(string: String(descModel?.room_amenity?.htmlToString.prefix(100) ?? "\n")))
            } else {
                attrIntroString.append(descModel?.room_amenity?.htmlToAttributedString ?? optinalString)
            }
        }
       
        
        descriptionLbl.attributedText = attrIntroString

        
    }
    func handlers() {
        favoriteBtn.UIViewAction {
            if case UserStatus.isLogged = true {
                self.addToFavorite()
            } else {
                UserStatus.loginAlert(in: self)
            }
        }
    }
    func addToFavorite() {
        WebServiceFactory().addFavorite(hotelID: hotel?.hotel_id ?? 0) { (error, data, code) in
            if data != nil {
                self.hotel?.is_favourite = data?.data?.is_favourite
                self.reloadFavorite()
            }
        }
    }
    func reloadFavorite() {
        if case hotel?.is_favourite = true {
            favoriteBtn.setImage(#imageLiteral(resourceName: "HeartFill"), for: .normal)
        } else {
            favoriteBtn.setImage(#imageLiteral(resourceName: "Favorites"), for: .normal)
        }
    }
    //MARK:- @IBAction
  
    @IBAction func calenderClicked(_ sender: UIButton) {
         setupCalender()
    }
    @IBAction func dealsClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
        let dealsViewController = storyBoard.instantiateViewController(identifier: "DealsViewController") as! DealsViewController
        dealsViewController.hotel = hotel
        dealsViewController.date = date
        dealsViewController.checkinDate = checkinDate
        dealsViewController.checkoutDate = checkoutDate
        dealsViewController.rooms = rooms
        dealsViewController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
        self.present(dealsViewController , animated: true, completion: nil)
    }
    @IBAction func share(_ sender: Any) {
    }
    @IBAction func readMore(_ sender: Any) {
        descrpitionExpanded = !descrpitionExpanded

        if descrpitionExpanded {
            setupDesc()
        } else {
            setupDesc(isLimit: true)
        }
    }
    @IBAction func viewAll(_ sender: Any) {
        ammenetiesExpanded = !ammenetiesExpanded
        ammentitiesCollectionView.reloadData()
        self.heightAmmentitiesCollectionView.constant = self.ammentitiesCollectionView.collectionViewLayout.collectionViewContentSize.height
    }
    @IBAction func location(_ sender: Any) {
        let navigate = NavigateMap(lat: hotel?.latitude?.double(), lng: hotel?.longitude?.double(), title: hotel?.hotel_name)
        navigate.openMapForPlace(delegate: self)
    }
    
    //MARK:- Methods
    private func setupCalender() {
          let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
          dateRangePickerViewController.delegate = self
          dateRangePickerViewController.minimumDate = Calendar.current.date(byAdding: .day , value: 0 , to: Date())
          
          dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 100 , to: dateRangePickerViewController.minimumDate)
          let navController = UINavigationController(rootViewController: dateRangePickerViewController)
          navController.modalPresentationStyle = .automatic //or .overFullScreen for transparency
          self.present(navController , animated: true, completion: nil)
            }
}



//MARK:- UICollectionViewDataSource , UICollectionViewDelegate

extension HotelDetailsViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == facilitiesCollectionView {
            return 4
        } else if collectionView == ammentitiesCollectionView {
            if ammenetiesExpanded {
                return hotel?.amenities?.count ?? 0
            } else {
                let count = hotel?.amenities?.count ?? 0
                if count > 4 {
                    return 4
                } else {
                    return count
                }
            }
        } else {
            return hotel?.images?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.ammentitiesCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ammentitiesCollectionViewCell", for: indexPath) as! ammentitiesCollectionViewCell
            cell.titleLbl.text = hotel?.amenities?[indexPath.row].name
            let url = URL(string: hotel?.amenities?[indexPath.row].icon ?? "")
            cell.imageCell.kf.setImage(with: url)
            return cell
        } else if collectionView == self.facilitiesCollectionView {
            let cell = self.facilitiesCollectionView.dequeueReusableCell(withReuseIdentifier: "facilitiesCollectionViewCell", for: indexPath) as! facilitiesCollectionViewCell
            return cell
        } else {
            let cell = self.imagesCollectionView.dequeueReusableCell(withReuseIdentifier: "HotelImagesSliderCollectionViewCell", for: indexPath) as! HotelImagesSliderCollectionViewCell
            let url = URL(string: hotel?.images?[indexPath.row] ?? "")
            cell.imageHotel.kf.setImage(with: url)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == self.imagesCollectionView {
            self.imageCounterLabel.text = "\(indexPath.item + 1)/\(hotel?.images?.count ?? 0)"
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.imagesCollectionView {
            let storyBoard = UIStoryboard(name: "Updated", bundle: nil)
            let scene = storyBoard.instantiateViewController(identifier: "imageCollectionViewController") as! imageCollectionViewController
            scene.imagesArr = hotel?.images ?? []
            scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
            self.present(scene , animated: true, completion: nil)
        }
    }
}


//MARK:- UICollection Delegate Flow layout
extension HotelDetailsViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == facilitiesCollectionView {
            return CGSize(width: collectionView.frame.size.width/2-5 , height: 35)
        } else if collectionView == ammentitiesCollectionView  {
            return  CGSize(width: collectionView.frame.size.width/2-5 , height: 35 )
        } else {
            return  CGSize(width: collectionView.bounds.width , height: collectionView.bounds.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == facilitiesCollectionView {
            return UIEdgeInsets.init(top: 0 ,left: 0, bottom: 0, right: 0)
        } else {
            return UIEdgeInsets.init(top: 0 ,left: 0, bottom: 0, right: 0)
        }
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0.0
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0.0
//    }
}
// MARK:  Netowrk
extension HotelDetailsViewController {
    func reloadDeals() {
        WebServiceFactory().GetHotels(currency: "EGP", lang: Localizer.current, checkin_date: checkinDate ?? "", checkout_date: checkoutDate ?? "", nationality_code: "SA", hotelsIds: [hotel?.hotel_id ?? 0], rooms: rooms) { (error, data, code) in
            guard let data = data as? hotelsDataModel else { return }
            if data.data?.count ?? 0 > 0 {
                self.hotel = data.data?.first
                self.setup()
            }else {
                self.showAlert(title: "Alert".localized, message: "This hotel not available in this dates".localized)
            }
        }
    }
}
//MARK: - Calender Delegate
extension HotelDetailsViewController : CalendarDateRangePickerViewControllerDelegate {
    func didTapCancel() {
          self.dismiss(animated: true, completion: nil)
      }
    func setupFormatDate() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let dateObj = dateFormatter.date(from: checkinDate ?? "") else { return }
        let format = DateFormatter()
        format.dateFormat = "E , MMM d"
        let fromDateStr = format.string(from: dateObj)
        self.fromDateLabel.text = fromDateStr
        
        guard let toDateObj = dateFormatter.date(from: checkoutDate ?? "") else { return }
        let toDateStr = format.string(from: toDateObj)
        self.toDateLabel.text = toDateStr
        
    }
      func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        selectedDates.removeAll()
        let fromDate = formatter.string(from: startDate)
        let toDate = formatter.string(from: endDate)
        print(fromDate + " - " + toDate)
        
         
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let dateObj = dateFormatter.date(from: fromDate) else { return }
        checkinDate = dateFormatter.string(from: dateObj)
        let format = DateFormatter()
        format.dateFormat = "E , MMM d"
        let fromDateStr = format.string(from: dateObj)
        self.fromDateLabel.text = fromDateStr
        guard let toDateObj = dateFormatter.date(from: toDate) else { return }
        checkoutDate = dateFormatter.string(from: toDateObj)
        let toDateStr = format.string(from: toDateObj)
        self.toDateLabel.text = toDateStr
        self.checkDate.text = fromDateStr + "  -  " + toDateStr
        self.dismiss(animated: true, completion: nil)
        self.reloadDeals()
      }
}
