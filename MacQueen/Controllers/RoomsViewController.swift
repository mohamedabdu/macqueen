//
//  RoomsViewController.swift
//  MacQueen
//
//  Created by Kareem on 4/21/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
protocol RoomsDelegate: class {
    func didSelectRooms(rooms: [RoomsModel])
}
class RoomsViewController: UIViewController {

    //MARK: - @IBOutlet
    @IBOutlet weak var roomsTableView: UITableView! {
        didSet {
            roomsTableView.delegate = self
            roomsTableView.dataSource = self
        }
    }
    //MARK: - Variables
    var adultsArr = [Int]()
    var rooms: [RoomsModel] = []
    var adult = 1
    weak var delegate: RoomsDelegate?
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
    }
    func setup() {
        roomsTableView.reloadData()
    }
    //MARK: - @IBAction
    @IBAction func addRoomClicked(_ sender: UIButton) {
        rooms.append(RoomsModel(adultsNumber: 1, childNumber: 0, children_ages: []))
        self.roomsTableView.reloadData()
    }
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate?.didSelectRooms(rooms: self.rooms)
        })
    }
    
}


//MARK: - UITableViewDelegate
extension RoomsViewController : UITableViewDelegate , UITableViewDataSource, SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rooms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: RoomsTableViewCell.self, indexPath)
        cell.delegate = self
        cell.model = ""
        cell.addBtn.tag = indexPath.row
        cell.adultLbl.text = rooms[indexPath.row].adults.string
        cell.childArr = []
        var counter = 0
        rooms[indexPath.row].children_ages.forEach { (item) in
            cell.childArr.append(item)
            counter += 1
        }
        cell.childLbl.text = cell.childArr.count.string
        cell.childsTableViewHeight.constant = (cell.childArr.count*62).cgFloat
        cell.childsAgesTableView.reloadData()
        cell.addBtn.UIViewAction {
            cell.childArr.append(1)
            cell.childLbl.text = cell.childArr.count.string
            cell.childsTableViewHeight.constant = (cell.childArr.count*62).cgFloat
            cell.childsAgesTableView.reloadData()
            self.rooms[indexPath.row].children = cell.childArr.count
            self.rooms[indexPath.row].children_ages = cell.ages()
            tableView.reloadData()
        }
        cell.minusBtn.UIViewAction {
            if cell.childArr.count == 0 {
                return
            }
            cell.childArr.removeLast()
            cell.childLbl.text = cell.childArr.count.string
            cell.childsTableViewHeight.constant = (cell.childArr.count*62).cgFloat
            cell.childsAgesTableView.reloadData()
            self.rooms[indexPath.row].children = cell.childArr.count
            self.rooms[indexPath.row].children_ages = cell.ages()
            tableView.reloadData()
        }
        cell.addAdultBtn.UIViewAction {
            cell.adults += 1
            cell.adultLbl.text = cell.adults.string
            self.rooms[indexPath.row].adults = cell.adults
        }
        cell.minusAdultBtn.UIViewAction {
            if cell.adults == 0 {
                return
            }
            cell.adults -= 1
            cell.adultLbl.text = cell.adults.string
            self.rooms[indexPath.row].adults = cell.adults
        }
        cell.closureUpdateChilds = { ages in
            self.rooms[indexPath.row].children = cell.childArr.count
            self.rooms[indexPath.row].children_ages = ages
        }
        return cell
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        if indexPath.row == 0 {
                return nil
        }
        let deleteAction = SwipeAction(style: .destructive, title: "Delete".localized) { action, indexPath in
            // handle action by updating model with deletion
            self.rooms.remove(at: indexPath.row)
            tableView.reloadData()
        }
       // deleteAction.title = "Delete".localized
        // customize the action appearance
        return [deleteAction]
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//    }
}
