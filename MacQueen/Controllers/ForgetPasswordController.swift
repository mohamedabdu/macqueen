//
//  LoginViewController.swift
//  MacQueen
//
//  Created by Kareem on 4/16/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import ActionPicker

class ForgetPasswordController: UIViewController {

    
    //MARK:- @IBOutlet
    @IBOutlet var loginViews: [UIView]!
    @IBOutlet weak var emailTxf: UITextField!
    @IBOutlet weak var forgetLbl: UILabel!
    @IBOutlet weak var forgetBtn: UIButton!
    
    var isChangePassword: Bool = false
    weak var delegate: UIViewController?
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        if isChangePassword {
            forgetLbl.text = "Change password".localized
            forgetBtn.setTitle("Change password".localized, for: .normal)
            emailTxf.localizationPlaceHolder = "Email or mobile".localized
        }
    }
    override func viewDidLayoutSubviews() {
     super.viewDidLayoutSubviews()
        dropShadowForAllViews()
     }
    //MARK:- Methods
    
    private func dropShadowForAllViews(){
        for view in self.loginViews {
            view.applyShadow(cornerRadius: 30)
        }
    }
    func validate(txfs: [UITextField]) -> Bool {
        var validate = true
        txfs.forEach { (item) in
            if case item.text?.isEmpty = true {
                item.attributedPlaceholder = NSAttributedString(string: item.placeholder ?? "",
                                                                attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
                validate = false
            }
        }
        return validate
    }
    
    @IBAction func forgetPass(_ sender: UIButton) {
        if !validate(txfs: [emailTxf]) {
            return
        }
        var paramters: [String: Any] = [:]
        if case emailTxf.text?.isEmail = true {
            paramters["email"] = emailTxf.text
        } else {
            paramters["mobile"] = emailTxf.text
        }
        paramters["agentType"] = "mobile"
        startLoading()
        WebServiceFactory().ForgetPassword(paramters: paramters) { (error, user, code) in
            self.stopLoading()
            if error != nil {
                self.showAlert(title: "Alert".localized, message: error?.eMsg)
            } else {
                let scene = self.controller(OTPViewController.self, storyboard: .auth)
                scene.delegate = self.delegate
                scene.forForget = true
                scene.email = self.emailTxf.text
                scene.modalPresentationStyle = .automatic //or .overFullScreen for transparency
                self.delegate?.dismiss(animated: true) {
                    self.delegate?.present(scene, animated: true, completion: nil)
                }
            }
           
        }

    }
    
}
