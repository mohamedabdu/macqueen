//
//  SignUpViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/17/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
import AuthenticationServices

class SignUpViewController: UIViewController {

    
    //MARK:- @IBOutlet
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet var signingViews: [UIView]!
    @IBOutlet weak var alreadyLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var phoneTxf: UITextField!
    
    //MARK:- View LifeCycle
    var countries: [CountriesModel.Dial] = []
    var selectedCountry: Int?
    var isRegister: Bool = false
    var dialCode: String = "+20"
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
        fetchCountries()
    }
    
    override func viewDidLayoutSubviews() {
     super.viewDidLayoutSubviews()
        dropShadowForAllViews()
     }
    //MARK:- Methods
    
    private func dropShadowForAllViews(){
        for view in self.signingViews {
            view.applyShadow(cornerRadius: 30)
        }
    }
    func setup() {
        if isRegister {
            titleLbl.text = "Sign Up".localized
        } else {
            titleLbl.text = "Sign In".localized
            alreadyLbl.isHidden = true
            loginBtn.isHidden = true
        }
    }
    func handlers() {
        
    }
    func fetchCountries() {
        WebServiceFactory().CountriesDial { (erorr, data, code) in
            self.countries.removeAll()
            self.countries.append(contentsOf: data?.data?.countriesDials ?? [])
        }
    }
    func openPickCountry() {
        let scene = self.controller(PickerController.self, storyboard: .PickerViewHelper)
        scene.source = countries
        scene.titleClosure = { path in
            return "\(self.countries[path].name ?? "") - \(self.countries[path].dialCode ?? "")"
        }
        scene.didSelectClosure = { path in
            self.countryLbl.text = "\(self.countries[path].name ?? "") - \(self.countries[path].dialCode ?? "")"
            self.selectedCountry = path
            self.dialCode = self.countries[path].dialCode ?? ""
        }
        pushPop(vcr: scene)
    }
    @IBAction func next(_ sender: Any) {
        if isRegister {
            let scene = controller(CompleteUserInfoViewController.self, storyboard: .auth)
            scene.modalPresentationStyle = .automatic
            scene.mobile = "\(dialCode)\(phoneTxf.text ?? "")"
            present(scene, animated: true, completion: nil)
        } else {
            let scene = controller(LoginViewController.self, storyboard: .auth)
            scene.modalPresentationStyle = .automatic
            scene.mobile = "\(dialCode)\(phoneTxf.text ?? "")"
            present(scene, animated: true, completion: nil)
        }
        
    }
    @IBAction func continueEmail(_ sender: Any) {
        if isRegister {
            let scene = controller(CompleteUserInfoViewController.self, storyboard: .auth)
            scene.modalPresentationStyle = .automatic
            present(scene, animated: true, completion: nil)
        } else {
            let scene = controller(LoginViewController.self, storyboard: .auth)
            scene.modalPresentationStyle = .automatic
            present(scene, animated: true, completion: nil)
        }
        
    }
    @IBAction func continueFacebook(_ sender: Any) {
        let driver = FacebookDriver(delegate: self)
        driver.callback { (model) in
            var paramters: [String: Any] = [:]
            paramters["provider"] = "facebook"
            paramters["provider_token"] = model.id ?? ""
            paramters["first_name"] = model.name
            paramters["email"] = model.email
            WebServiceFactory().SocialLogin(paramters: paramters) { (error, user, code) in
                if error != nil {
                    self.showAlert(title: "Alert".localized, message: error?.eMsg)
                } else {
                    UserStatus.isLogged = true
                    UserStatus.email = user?.data?.email
                    UserStatus.firstName = user?.data?.firstName
                    UserStatus.lastName = user?.data?.lastName
                    UserStatus.mobile = user?.data?.mobile
                    UserStatus.token = user?.token
                    RootWindowController.setRootWindowForHome()
                }
            }
        }
        
    }
    @IBAction func countryList(_ sender: Any) {
        self.openPickCountry()
    }
    @IBAction func continueTwitter(_ sender: Any) {
        let closure:callbackTwitter = { twitter in
            print(twitter.id as Any ,twitter.username as Any)
            var paramters: [String: Any] = [:]
            paramters["provider"] = "twitter"
            paramters["provider_token"] = twitter.id ?? ""
            paramters["first_name"] = twitter.username ?? "Unkown"
            WebServiceFactory().SocialLogin(paramters: paramters) { (error, user, code) in
                if error != nil {
                    self.showAlert(title: "Alert".localized, message: error?.eMsg)
                } else {
                    UserStatus.isLogged = true
                    UserStatus.email = user?.data?.email
                    UserStatus.firstName = user?.data?.firstName
                    UserStatus.lastName = user?.data?.lastName
                    UserStatus.mobile = user?.data?.mobile
                    UserStatus.token = user?.token
                    RootWindowController.setRootWindowForHome()
                }
            }
        }
        let twitterDriver = TwitterDriver()
        twitterDriver.delegate = self
        twitterDriver.closure = closure
        
       
        
    }
    @IBAction func continueApple(_ sender: Any) {
        self.handleAppleIdRequest()
    }
    
}
import TwitterKit
extension SignUpViewController: TWTRComposerViewControllerDelegate {
    //MARK:- TWTRComposerViewControllerDelegate
    
    func composerDidCancel(_ controller: TWTRComposerViewController) {
        print("composerDidCancel, composer cancelled tweet")
    }
    
    func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
        print("composerDidSucceed tweet published")
    }
    func composerDidFail(_ controller: TWTRComposerViewController, withError error: Error) {
        print("composerDidFail, tweet publish failed == \(error.localizedDescription)")
    }
}

extension SignUpViewController: ASAuthorizationControllerDelegate {

    @available(iOS 13.0, *)
    @objc func handleAppleIdRequest() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    
    
    func checkLogin(userID: String) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            appleIDProvider.getCredentialState(forUserID: userID) {  (credentialState, error) in
                switch credentialState {
                    case .authorized:
                        // The Apple ID credential is valid.
                        break
                    case .revoked:
                        // The Apple ID credential is revoked.
                        break
                    case .notFound: break
                    // No credential was found, so show the sign-in UI.
                    default:
                        break
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            print("User id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))")
            var paramters: [String: String] = [:]
            paramters["provider"] = "apple"
            paramters["provider_token"] = userIdentifier
            paramters["email"] = email
            paramters["first_name"] = "\(appleIDCredential.fullName?.givenName ?? "") \(appleIDCredential.fullName?.familyName ?? "")"
            if  paramters["first_name"] == nil || paramters["first_name"] == "" || paramters["first_name"] == " " {
                paramters["first_name"] = "Unkown"
            }
            WebServiceFactory().SocialLogin(paramters: paramters) { (error, user, code) in
                if error != nil {
                    self.showAlert(title: "Alert".localized, message: error?.eMsg)
                } else {
                    UserStatus.isLogged = true
                    UserStatus.email = user?.data?.email
                    UserStatus.firstName = user?.data?.firstName
                    UserStatus.lastName = user?.data?.lastName
                    UserStatus.mobile = user?.data?.mobile
                    UserStatus.token = user?.token
                    RootWindowController.setRootWindowForHome()
                }
            }
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
    }
    
}




