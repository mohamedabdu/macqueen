//
//  UserSavedCardsViewController.swift
//  MacQueen
//
//  Created by Kareem on 6/28/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class UserSavedCardsViewController: UIViewController {
    
    @IBOutlet weak var savedCardTbl: UITableView! {
        didSet {
            savedCardTbl.delegate = self
            savedCardTbl.dataSource = self
        }
    }
    
    var cards: [SavedCardModel.Data] = []
    var cardNumber: String?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getSavedCard()
    }
    
    func getSavedCard() {
        WebServiceFactory().getSavedCards { (error, data, code) in
            self.cards = []
            if data != nil {
                self.cards.append(contentsOf: data?.data ?? [])
                if self.cards.count == 0 {
                    self.savedCardTbl.isHidden = true
                    return
                }
                self.savedCardTbl.reloadData()
            }
        }
    }
    
    func removeUserCards(cardId:Int) {
        WebServiceFactory().RemoveUserCards(cardId: cardId) { (error , response, code) in
            if code == 200 {
                self.getSavedCard()
            } else {
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.removeUserCards(cardId: self.cards[indexPath.row].id ?? 0)
        }
    }
}

extension UserSavedCardsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: SavedCardCell.self, indexPath)
        cell.model = "**** **** **** \(cards[indexPath.row].last4 ?? "")"
        return cell
    }
}


