//
//  SearchViewController.swift
//  MacQueen
//
//  Created by Kareem on 4/21/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit
protocol SearchDelegate: class {
    func didSelectCity(city: cities)
    func didSelectHotel(hotel: hotels)
}
class SearchViewController: UIViewController {

    //MARK: - @IBOutlet
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTxf: UITextField!
    @IBOutlet weak var searchTableView: UITableView! {
        didSet {
            self.searchTableView.dataSource = self
            self.searchTableView.delegate = self
        }
    }
    
    var sectionArr = ["Cities".localized, "Hotels".localized]
    var cities: [cities] = []
    var hotels: [hotels] = []
    var citiesExpanded: Bool = true
    var hotelsExpanded: Bool = true
    weak var delegate: SearchDelegate?
     //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont(view: view)
        setup()
    }
    func setup() {
        searchTxf.addTarget(self, action: #selector(HomeViewController.textFieldDidChange(_:)), for: .editingChanged)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text?.count ?? 0 > 2 {
            searchView.fadeIn()
            startLoading()
            WebServiceFactory().SearchByWord(keyword: textField.text ?? "") { (error, model, code) in
                self.stopLoading()
                if code > 200 {
                    self.showAlert(title: "Alert".localized, message: error?.eMsg)
                    return
                }
                let model = model as? searchByWordModel
                self.cities.removeAll()
                self.cities.append(contentsOf: model?.data?.cities ?? [])
                self.hotels.removeAll()
                self.hotels.append(contentsOf: model?.data?.hotels ?? [])
                self.searchTableView.reloadData()
            }
        }
    }

    @IBAction func resetSearch(_ sender: Any) {
        searchTxf.text = ""
        textFieldDidChange(searchTxf)
    }
}

 //MARK: - TableView Delegate
extension SearchViewController : UITableViewDataSource , UITableViewDelegate {
   
    //MARK: - Header For Section
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SearchHeaderView()
        view.headerLabel.text = self.sectionArr[section]
        if section == 0 {
            view.closure = { [weak self] in
                self?.citiesExpanded = !(self?.citiesExpanded ?? false)
                self?.searchTableView.reloadData()
            }
        } else {
            view.closure = { [weak self] in
                self?.hotelsExpanded = !(self?.hotelsExpanded ?? false)
                self?.searchTableView.reloadData()
            }
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    //MARK: - TableView DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if citiesExpanded {
                return cities.count
            } else {
                return 0
            }
        } else {
            if hotelsExpanded {
                return hotels.count
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            var cell = tableView.cell(type: SearchCityCell.self, indexPath)
            cell.model = cities[indexPath.row]
            return cell
        } else {
            var cell = tableView.cell(type: SearchCityCell.self, indexPath)
            cell.model = hotels[indexPath.row]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.delegate?.didSelectCity(city: cities[indexPath.row])
            self.dismissClicked(self)
        } else {
            self.delegate?.didSelectHotel(hotel: hotels[indexPath.row])
            self.dismissClicked(self)
        }
    }
}
