//
//  MainTabBarViewController.swift
//  MacQueen
//
//  Created by Kareem on 5/12/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
        tabBar.backgroundColor = UIColor.white
        tabBar.clipsToBounds = true
       
        self.tabBar.items?[0].selectedImage = self.tabBar.items?[0].image?.withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[0].image = self.tabBar.items?[0].image?.withRenderingMode(.alwaysOriginal)
        
        //MARK:- Here all with original icon
//        for item in self.tabBar.items!{
//            item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
//            item.image = item.image?.withRenderingMode(.alwaysOriginal)
//        }
    }
}
