//
//  ShippmentSumModel.swift
//  AlNawares
//
//  Created by Kareem on 3/18/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import ObjectMapper

class Countries: Mappable {
    var data: [Data]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        data  <- map["data"]
    }
    
    class Data: Mappable {
        
        var id: Int?
        var name :String?
        var code: String?
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            id  <- map["id"]
            name  <- map["name"]
            code  <- map["code"]
        }
    }
    
}






