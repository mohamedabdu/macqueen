//
//  accountStatmentModel.swift
//  AlNawares
//
//  Created by Kareem on 3/19/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import ObjectMapper

class amentitiesFilterModel: Mappable {
   
    var data: [amentities]?
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        data  <- map["data"]
    }
}
class amentities: Mappable {
   
    var id: Int?
    var name :String?
    var icon :String?
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        id  <- map["id"]
        name  <- map["name"]
        icon  <- map["icon"]
    }
}

