//
//  CardsModel.swift
//  MacQueen
//
//  Created by Kareem on 3/29/20.
//  Copyright © 2020 Mahmoud Fares. All rights reserved.
//

import Foundation
import ObjectMapper


class CardsModel: Mappable {
    var data: [Cards]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        data  <- map["data"]
    }
}

class Cards: Mappable {

    var id: Int?
    var card_type:String?
    var scheme:String?
    var last4:String?
    var card_id:String?
    var issuer:String?
    var payment_provider_id:Int?
    var expiry_month:String?
    var expiry_year:String?
    
    
    required init?(map: Map) {
    }

    func mapping(map: Map) {
        id  <- map["id"]
        card_type  <- map["card_type"]
        scheme  <- map["scheme"]
        last4  <- map["last4"]
        card_id  <- map["card_id"]
        issuer  <- map["issuer"]
        payment_provider_id <- map["payment_provider_id"]
        expiry_month <- map["expiry_month"]
        expiry_year <- map["expiry_year"]
    }
}


