// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let notificationModel = try? newJSONDecoder().decode(NotificationModel.self, from: jsonData)

import Foundation

// MARK: - NotificationModel
struct NotificationModel: Codable {
    let currentPage: Int?
    let data: [Datum]?
    
    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case data
    }
    
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: String?
        let type: String?
        let notifiableType, notifiableID: String?
        let data: DataClass?
        let deviceToken: String?
        let readAt: String?
        let createdAt, updatedAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id, type
            case notifiableType = "notifiable_type"
            case notifiableID = "notifiable_id"
            case data
            case deviceToken = "device_token"
            case readAt = "read_at"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
        }
    }
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let id: Int?
        let title: String?
        let body: String?
        let image: String?
    }
 
}
