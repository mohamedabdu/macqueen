//
//  CreatePickupModel.swift
//  AlNawares
//
//  Created by Kareem on 3/12/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation

struct RoomsModel: Codable {
    var adults : Int
    var children: Int
    var children_ages:[Int]
    var lead_passenger: RoomPasseger?
    init(adultsNumber: Int = 1 , childNumber: Int = 0 , children_ages: [Int] = []) {
          self.adults = adultsNumber
          self.children = 0
          self.children_ages = children_ages
    }
    var decRooms: [String: Any] {
        return ["adults": adults, "children": children, "children_ages": children_ages, "lead_passenger": lead_passenger?.decPassenger ?? [:]]
    }
    
    struct RoomPasseger: Codable {
        var title: String
        var first_name: String
        var last_name: String
        var type: String
        init(title: String, first_name: String, last_name: String, type: String) {
            self.type = type
            self.title = title
            self.first_name = first_name
            self.last_name = last_name
        }
        
        var decPassenger: [String: Any] {
            return ["title": title, "first_name": first_name, "last_name": last_name, "type": type]
        }
    }
}

struct PaymentModel: Codable {
    var type : String?
    var token: String?
    var id: String?
    var cvv: String?
    init(token: String, type: String) {
        self.type = type
        self.token = token
    }
    init(id: String, cvv: String, type: String) {
        self.type = type
        self.id = id
        self.cvv = cvv
    }
    var dec: [String: Any] {
        return ["token": token ?? "", "type": type ?? ""]
    }
    var decID: [String: Any] {
        return ["id": id ?? "", "cvv": cvv ?? "", "type": type ?? ""]
    }
}
