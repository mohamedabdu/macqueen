//
//  DescriptionModel.swift
//  MacQueen
//
//  Created by M.abdu on 6/22/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
class DescriptionModel: Codable {
    /*
     "hotel_introduction": null,
     "hotel_information": "# Property Location : # With a stay at Cleopatra Hotel, you'll be centrally located in Cairo, steps from Tahrir Square and minutes from Egyptian Museum.  This hotel is within close proximity of League of Arab States and American University of Cairo. #  Rooms : # Make yourself at home in one of the 72 guestrooms featuring minibars and LCD televisions. Complimentary wireless Internet access keeps you connected, and satellite programming is available for your entertainment. Private bathrooms with showers feature complimentary toiletries and hair dryers. Conveniences include phones, as well as safes and electric kettles. #  Amenities : # Take in the views from a terrace and make use of amenities such as complimentary wireless Internet access and concierge services. Additional amenities at this hotel include wedding services and a television in a common area. #  Dining : # Satisfy your appetite at the hotel's restaurant, which serves breakfast, lunch, and dinner. Dining is also available at a coffee shop/café, and 24-hour room service is provided. #  Business & Other Amenities : # Featured amenities include a 24-hour front desk, multilingual staff, and luggage storage. Planning an event in Cairo? This hotel has 11 square feet (1 square meters) of space consisting of conference space and a meeting room.",
     "hotel_amenity": null,
     "room_amenity": null,
     "location_information": null,
     "attraction_information": null
     */
    var hotel_introduction: String?
    var hotel_information: String?
    var hotel_amenity: String?
    var room_amenity: String?
    var location_information: String?
    var attraction_information: String?
}
