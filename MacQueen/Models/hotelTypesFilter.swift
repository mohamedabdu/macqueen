//
//  ShippmentSumModel.swift
//  AlNawares
//
//  Created by Kareem on 3/18/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import ObjectMapper

class hotelTypesFilterModel: Mappable {
    var data: [hotelDataType]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        data  <- map["data"]
    }
}
class hotelDataType: Mappable {
   
    var id: Int?
    var name :String?
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        id  <- map["id"]
        name  <- map["name"]
    }
}





