//
//  loginModel.swift
//  AlNawares
//
//  Created by Kareem on 3/11/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import ObjectMapper

class searchByWordModel: Mappable {
   
    var data: searchByWordData?
    
    
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {

        data  <- map["data"]
        
    }
}
class searchByWordData: Mappable {
    
    var cities: [cities]?
    var hotels : [hotels]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        cities  <- map["cities"]
        hotels  <- map["hotels"]
    }
}
class cities : Mappable {
    var country_name : String?
    var name: String?
    var id: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        country_name  <- map["country_name"]
        name  <- map["name"]
        id  <- map["id"]
    }
}
class hotels : Mappable {
    var country_name : String?
    var name: String?
    var id: Int?

    required init?(map: Map) {
        
    }
    required init?() {
        
    }
    func mapping(map: Map) {
        country_name  <- map["country_name"]
        name  <- map["name"]
        id  <- map["id"]
    }
}






