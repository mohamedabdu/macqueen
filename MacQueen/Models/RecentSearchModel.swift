
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let recentSearchModel = try? newJSONDecoder().decode(RecentSearchModel.self, from: jsonData)

import Foundation

// MARK: - RecentSearchModel
struct RecentSearchModel: Codable {
    let data: [Datum]?
    
    // MARK: - Datum
    struct Datum: Codable {
        let deviceToken, keyword: String?
        let meta: Meta?
        
        enum CodingKeys: String, CodingKey {
            case deviceToken = "device_token"
            case keyword, meta
        }
    }
    
    // MARK: - Meta
    struct Meta: Codable {
        let type: String?
    }

}
