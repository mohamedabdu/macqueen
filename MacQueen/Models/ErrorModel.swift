//
//  ErrorModel.swift
//  Nasil
//
//  Created by Medhat on 8/24/19.
//  Copyright © 2019 Nasil. All rights reserved.
//

class ErrorModel {
    var eCode: Int!
    var eMsg: String!
    
    init(eCode: Int, eMsg: String) {
        self.eCode = eCode
        self.eMsg = eMsg
    }
}
