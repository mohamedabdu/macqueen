//
//  PickupModelList.swift
//  AlNawares
//
//  Created by Kareem on 3/12/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import ObjectMapper

class travelStylesFilterModel: Mappable {
   
    var data: [travelStylesData]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
         data  <- map["data"]
    }
}
class travelStylesData: Mappable {
   
    var id: Int?
    var name :String?
    var image :String?
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        id  <- map["id"]
        name  <- map["name"]
        image  <- map["image"]
    }
}





