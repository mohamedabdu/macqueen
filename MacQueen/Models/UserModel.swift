
import Foundation

// MARK: - BlogModel
struct UserModel: Codable {
    let data: Datum?
    let message: String?
    let meta: Meta?
    let token: String?
    let errors: Errors?

    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let firstName, lastName, email, mobile: String?
        let billingCountry: String?
        let billingCity: String?
        let billingState: String?
        let billingStreet: String?
        let billingPostalCode: String?
        let createdAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id, email, mobile
            case firstName = "first_name"
            case lastName = "last_name"
            case billingCountry = "billing_country"
            case billingCity = "billing_city"
            case billingState = "billing_state"
            case billingStreet = "billing_street"
            case billingPostalCode = "billing_postal_code"
            case createdAt = "created_at_human"
            
        }
    }
    
    // MARK: - Meta
    struct Meta: Codable {
        let token: String?
    }
}


class Errors: Codable {
    
    var email: [String]?
    var first_name: [String]?
    var id: Int?
    var image: String?
    var last_name: [String]?
    var mobile: [String]?
    var building: String?
    var cityTitle: String?
    var countryTitle: String?
    var deliveryFees: Int?
    var floorNumber: String?
    var longAddress: String?
    var street: String?
    var category: String?
    var tallness: String?
    var shape: String?
    var password: [String]?
    var message: String?
    var old_password: [String]?
    func description() -> String {
        
        let mirroredObject = Mirror(reflecting: self)
        let str: NSMutableString = NSMutableString()
        for (_, attr) in mirroredObject.children.enumerated() {
            
            if let propertyName = attr.label as String? {
                let string: [String]? = attr.value as? [String]
                if let _ = string {
                    str.append("\(propertyName) : \(string?.first ?? "")")
                }
                
            }
        }
        //print("desc=\(str)")
        return str as String
    }
    
}
