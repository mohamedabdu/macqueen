// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let bookModel = try? newJSONDecoder().decode(BookModel.self, from: jsonData)

import Foundation

// MARK: - BookModel
struct BookModel: Codable {
    let data: DataClass?
    
    struct DataClass: Codable {
        let id, hotelID: Int?
        let hotelCode, bookingCode, name, city: String?
        let address, longitude, latitude, checkinDate: String?
        let checkoutDate: String?
        let price: Int?
        let currency: String?
        let isRefundable: Bool?
        let policy: String?
        let rooms: [Room]?
        let hotel: Hotel?
        
        enum CodingKeys: String, CodingKey {
            case id
            case hotelID = "hotel_id"
            case hotelCode = "hotel_code"
            case bookingCode = "booking_code"
            case name, city, address, longitude, latitude
            case checkinDate = "checkin_date"
            case checkoutDate = "checkout_date"
            case price, currency, isRefundable, policy, rooms, hotel
        }
    }
    
    // MARK: - Hotel
    struct Hotel: Codable {
        let id: Int?
        let name, nameAr, address, rate: String?
        let countryCode, countryName, countryNameAr, longitude: String?
        let latitude, checkinTime, checkoutTime, phone: String?
        let postalCode: String?
        let discountPercentage: Int?
        
        enum CodingKeys: String, CodingKey {
            case id, name
            case nameAr = "name_ar"
            case address, rate
            case countryCode = "country_code"
            case countryName = "country_name"
            case countryNameAr = "country_name_ar"
            case longitude, latitude
            case checkinTime = "checkin_time"
            case checkoutTime = "checkout_time"
            case phone
            case postalCode = "postal_code"
            case discountPercentage = "discount_percentage"
        }
    }
    
    // MARK: - Room
    struct Room: Codable {
        let leadPassenger: LeadPassenger?
        let children: Int?
        let childrenAges: [Int]?
        let adults: Int?
        
        enum CodingKeys: String, CodingKey {
            case leadPassenger = "lead_passenger"
            case children
            case childrenAges = "children_ages"
            case adults
        }
    }
    
    // MARK: - LeadPassenger
    struct LeadPassenger: Codable {
        let type, firstName, title, lastName: String?
        
        enum CodingKeys: String, CodingKey {
            case type
            case firstName = "first_name"
            case title
            case lastName = "last_name"
        }
    }

}

// MARK:
