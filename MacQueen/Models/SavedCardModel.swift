//
//  ShippmentSumModel.swift
//  AlNawares
//
//  Created by Kareem on 3/18/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import ObjectMapper

class SavedCardModel: Mappable {
    var data: [Data]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        data  <- map["data"]
    }
    
    class Data: Mappable {
        
        var id: Int?
        var payment_provider_id: Int?
        var card_id: String?
        var expiry_month: String?
        var expiry_year: String?
        var scheme: String?
        var last4: String?
        var card_type: String?
        var issuer: String?
        var issuer_country: String?
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            id  <- map["id"]
            payment_provider_id  <- map["payment_provider_id"]
            card_id  <- map["card_id"]
            last4 <- map["last4"]
        }
    }
    
}






