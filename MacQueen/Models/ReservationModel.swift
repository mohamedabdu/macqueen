// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let reservationModel = try? newJSONDecoder().decode(ReservationModel.self, from: jsonData)

import Foundation

// MARK: - ReservationModel
struct ReservationModel: Codable {
    let data: DataClass?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let id, hotelID: Int?
        let hotelCode, bookingCode, name, city: String?
        let address, longitude, latitude, checkinDate: String?
        let checkoutDate, price, currency: String?
        let isRefundable: Bool?
        let policy: String?
        let rooms: [Room]?
        let hotel: Hotel?
        
        enum CodingKeys: String, CodingKey {
            case id
            case hotelID = "hotel_id"
            case hotelCode = "hotel_code"
            case bookingCode = "booking_code"
            case name, city, address, longitude, latitude
            case checkinDate = "checkin_date"
            case checkoutDate = "checkout_date"
            case price, currency, isRefundable, policy, rooms, hotel
        }
    }
    
    // MARK: - Hotel
    struct Hotel: Codable {
        let id: Int?
        let name, nameAr, address, rate: String?
        let countryCode, countryName, countryNameAr, longitude: String?
        let latitude, checkinTime, checkoutTime, phone: String?
        let postalCode: String?
        let discountPercentage: Int?
        let images: [String]?
        
        enum CodingKeys: String, CodingKey {
            case id, name
            case nameAr = "name_ar"
            case address, rate
            case countryCode = "country_code"
            case countryName = "country_name"
            case countryNameAr = "country_name_ar"
            case longitude, latitude
            case checkinTime = "checkin_time"
            case checkoutTime = "checkout_time"
            case phone
            case postalCode = "postal_code"
            case discountPercentage = "discount_percentage"
            case images
        }
    }
    
    // MARK: - Room
    struct Room: Codable {
        let adults, children: Int?
        let childrenAges: [Int]?
        let leadPassenger: LeadPassenger?
        
        enum CodingKeys: String, CodingKey {
            case adults, children
            case childrenAges = "children_ages"
            case leadPassenger = "lead_passenger"
        }
    }
    
    // MARK: - LeadPassenger
    struct LeadPassenger: Codable {
        let type, title, lastName, firstName: String?
        
        enum CodingKeys: String, CodingKey {
            case type, title
            case lastName = "last_name"
            case firstName = "first_name"
        }
    }

}
