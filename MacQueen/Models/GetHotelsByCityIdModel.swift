//
//  SearchByInvoiceModel.swift
//  AlNawares
//
//  Created by Kareem on 3/17/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import ObjectMapper

class GetHotelsByCityIdModel: Mappable {
    
    var data : [Int]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        data  <- map["data"]
    }
}

