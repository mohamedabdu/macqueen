//
//  CreatePickupModel.swift
//  AlNawares
//
//  Created by Kareem on 3/12/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation

struct CardModel: Codable {
    var number : String?
    var cvv: String?
    var holder:String?
    var month: String?
    var year: String?
    var id: String?
    init(number: String?, cvv: String?, holder: String?, month: String?, year: String?) {
        self.number = number
        self.cvv = cvv
        self.holder = holder
        self.month = month
        self.year = year
    }
    
    
}

