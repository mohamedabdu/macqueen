//
//  FilterModel.swift
//  MacQueen
//
//  Created by M.abdu on 6/30/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation

class FilterModel {
    var ammentiesSelected: [Int] = []
    var typesSelected: [Int] = []
    var travelSelected: [Int] = []
    var rate: Double?
    var minPrice: Int?
    var maxPrice: Int?
}
