// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let countriesModel = try? newJSONDecoder().decode(CountriesModel.self, from: jsonData)

import Foundation

// MARK: - CountriesModel
struct CountriesModel: Codable {
    let data: DataClass?
    // MARK: - DataClass
    struct DataClass: Codable {
        let userCountryDial: Dial?
        let countriesDials: [Dial]?
        
        enum CodingKeys: String, CodingKey {
            case userCountryDial = "user_country_dial"
            case countriesDials = "countries_dials"
        }
    }
    
    // MARK: - Dial
    struct Dial: Codable {
        let name, dialCode, code: String?
        
        enum CodingKeys: String, CodingKey {
            case name
            case dialCode = "dial_code"
            case code
        }
    }

}

