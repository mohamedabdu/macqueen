//
//  ShippmentSumModel.swift
//  AlNawares
//
//  Created by Kareem on 3/18/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentTokenModel: Mappable {
    var data: Data?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        data  <- map["data"]
    }
    
    class Data: Mappable {
        
        var env: String?
        var url :String?
        var public_key :String?
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            env  <- map["env"]
            url  <- map["url"]
            public_key  <- map["public_key"]
        }
    }
    
}






