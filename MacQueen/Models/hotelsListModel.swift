//
//  BillsModel.swift
//  AlNawares
//
//  Created by Kareem on 3/15/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import ObjectMapper

class hotelsDataModel: Mappable {
    var data : [hotel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        data  <- map["data"]
    }
}
class HotelSingleModel: Mappable {
    var data : hotel?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        data  <- map["data"]
    }
}
class hotel: Mappable {
    var id: Int?
    var provider : String?
    var hotel_id : Int?
    var hotel_name : String?
    var hotel_code : String?
    var country_code : String?
    var address : String?
    var description : DescriptionModel?
    var image : String?
    var images : [String]?
    var imageFav : String?
    var rate : String?
    var longitude : String?
    var latitude : String?
    var min_price : Int?
    var currency : String?
    var discount_percentage : String?
    var type : String?
    var type_id : Int?
    var travel_style_id : Int?
    var amenities : [ammenties]?
    var deals:[deals]?
    var rooms: [HotelRooms]?
    var is_favourite: Bool?
    var name: String?
    var price: String?
    var hotel: hotel?
    required init?(map: Map) {
    }

    func mapping(map: Map) {
        id  <- map["id"]
        provider  <- map["provider"]
        hotel_id  <- map["hotel_id"]
        hotel_name  <- map["hotel_name"]
        hotel_code  <- map["hotel_code"]
        country_code  <- map["country_code"]
        address  <- map["address"]
        description  <- map["description"]
        image  <- map["image"]
        images  <- map["images"]
        imageFav  <- map["images"]
        rate  <- map["rate"]
        longitude  <- map["longitude"]
        latitude  <- map["latitude"]
        min_price  <- map["min_price"]
        currency  <- map["currency"]
        discount_percentage  <- map["discount_percentage"]
        type  <- map["type"]
        type_id  <- map["type_id"]
        travel_style_id  <- map["travel_style_id"]
        amenities  <- map["amenities"]
        deals  <- map["deals"]
        rooms <- map["rooms"]
        is_favourite <- map["is_favourite"]
        name <- map["name"]
        price <- map["price"]
        hotel <- map["hotel"]
    }
    class DescriptionModel: Mappable {
        /*
         "hotel_introduction": null,
         "hotel_information": "# Property Location : # With a stay at Cleopatra Hotel, you'll be centrally located in Cairo, steps from Tahrir Square and minutes from Egyptian Museum.  This hotel is within close proximity of League of Arab States and American University of Cairo. #  Rooms : # Make yourself at home in one of the 72 guestrooms featuring minibars and LCD televisions. Complimentary wireless Internet access keeps you connected, and satellite programming is available for your entertainment. Private bathrooms with showers feature complimentary toiletries and hair dryers. Conveniences include phones, as well as safes and electric kettles. #  Amenities : # Take in the views from a terrace and make use of amenities such as complimentary wireless Internet access and concierge services. Additional amenities at this hotel include wedding services and a television in a common area. #  Dining : # Satisfy your appetite at the hotel's restaurant, which serves breakfast, lunch, and dinner. Dining is also available at a coffee shop/café, and 24-hour room service is provided. #  Business & Other Amenities : # Featured amenities include a 24-hour front desk, multilingual staff, and luggage storage. Planning an event in Cairo? This hotel has 11 square feet (1 square meters) of space consisting of conference space and a meeting room.",
         "hotel_amenity": null,
         "room_amenity": null,
         "location_information": null,
         "attraction_information": null
         */
        var hotel_introduction: String?
        var hotel_information: String?
        var hotel_amenity: String?
        var room_amenity: String?
        var location_information: String?
        var attraction_information: String?
        required init?(map: Map) {
        }
        func mapping(map: Map) {
            hotel_introduction  <- map["hotel_introduction"]
            hotel_information  <- map["hotel_information"]
            hotel_amenity  <- map["hotel_amenity"]
            location_information  <- map["location_information"]
            attraction_information  <- map["attraction_information"]
        }
     
    }

    class HotelRooms: Mappable {
        var adults: Int?
        var children_ages: [Int]?
        var children: Int?
        var lead_passenger: LeadPassenger?
        required init?(map: Map) {
        }
        func mapping(map: Map) {
            adults  <- map["adults"]
            children_ages  <- map["children_ages"]
            children  <- map["children"]
            lead_passenger  <- map["lead_passenger"]
        }
    }
    class LeadPassenger: Mappable {
        var type: String?
        var title: String?
        var last_name: String?
        var first_name: String?
        required init?(map: Map) {
        }
        func mapping(map: Map) {
            type  <- map["type"]
            title  <- map["title"]
            last_name  <- map["last_name"]
            first_name  <- map["first_name"]
        }
    }
}
class ammenties: Mappable {

    var id : Int?
    var name : String?
    var icon : String?
   

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        id  <- map["id"]
        name  <- map["name"]
        icon  <- map["icon"]
    }
}
class deals: Mappable {

    var code : String?
    var currency : String?
    var isRefundable : Bool?
    var price : Int?
    var rooms : [rooms]?
   

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        code  <- map["code"]
        currency  <- map["currency"]
        isRefundable  <- map["isRefundable"]
        rooms  <- map["rooms"]
        price  <- map["price"]
    }
}
class rooms: Mappable {

    var benefits : String?
    var name : String?
    var isRefundable : Bool?
    var price : Int?
    var pax : pax?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        benefits  <- map["benefits"]
        name  <- map["name"]
        pax  <- map["pax"]
        price  <- map["price"]
    }
}
class pax: Mappable {

    var adults_number : Int?
    var children_number : Int?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        adults_number  <- map["adults_number"]
        children_number  <- map["children_number"]
    }
}












