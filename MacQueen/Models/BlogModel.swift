
import Foundation

// MARK: - BlogModel
struct BlogModel: Codable {
    let data: [Datum]?
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let slug, title, summary, body: String?
        let publishedAt: String?
        let featuredImage: String?
        let featuredImageCaption: String?
        let user: User?
        let meta: Meta?
        
        enum CodingKeys: String, CodingKey {
            case id, slug, title, summary, body
            case publishedAt = "published_at"
            case featuredImage = "featured_image"
            case featuredImageCaption = "featured_image_caption"
            case user, meta
        }
    }
    
    // MARK: - Meta
    struct Meta: Codable {
        let title, metaDescription, canonicalLink: String?
        
        enum CodingKeys: String, CodingKey {
            case title
            case metaDescription = "description"
            case canonicalLink = "canonical_link"
        }
    }
    
    // MARK: - User
    struct User: Codable {
        let id: Int?
        let name: String?
    }

}

