//
//  TwitterDelegate.swift
//  RedBricks
//
//  Created by Mohamed Abdu on 6/11/18.
//  Copyright © 2018 Atiaf. All rights reserved.
//

import Foundation
import TwitterKit

extension AppDelegate{
//    func application(_ application: UIApplication,open url: URL,options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
//        if TWTRTwitter.sharedInstance().application(application, open: url, options: options) {
//            return true
//        }
//        // Your other open URL handlers follow […]
//        return true
//    }
    // twitter
    func initTwitter(){
        TWTRTwitter.sharedInstance().start(withConsumerKey: SocialConstant.twitterId, consumerSecret: SocialConstant.twitterKey)
    }
    //
}
