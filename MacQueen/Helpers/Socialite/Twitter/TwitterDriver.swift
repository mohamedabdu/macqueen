//
//  TwitterDriver.swift
//  RedBricks
//
//  Created by Mohamed Abdu on 6/10/18.
//  Copyright © 2018 Atiaf. All rights reserved.
//

import Foundation
import TwitterKit

typealias callbackTwitter = (TwitterModel)->()

class TwitterDriver:SocialError {
    private var _closure:callbackTwitter?
    var closure: callbackTwitter? {
        set{
            _closure = newValue
            twitterLogin()
        }get{
            return _closure
        }
    }
    var delegate: TWTRComposerViewControllerDelegate?
    func twitterLogin() {
        if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
            // App must have at least one logged-in user to compose a Tweet
            
            let shareImg2 = #imageLiteral(resourceName: "Rectangle1")
            //let shareImg = UIImage.init(named: "mountain")!
            let composer = TWTRComposerViewController.init(initialText: "UK flag picture will be tweeted", image: shareImg2, videoURL: nil)
            composer.delegate = delegate
            let vc = self.delegate as? UIViewController
            vc?.present(composer, animated: true, completion: nil)
            
        } else {
            let store = TWTRTwitter.sharedInstance().sessionStore
            store.reload()
            for case let session as TWTRSession in store.existingUserSessions() {
                store.logOutUserID(session.userID)
            }
            
            // Log in, and then check again
            TWTRTwitter.sharedInstance().logIn { session, error in
                if session != nil { // Log in succeeded
                    let model = TwitterModel(twitter: session!)
                    self.closure?(model)
                } else {
                    //show error
                    self.alertError()
                }
            }
        }
      
    }
    
    //MARK:- TWTRComposerViewControllerDelegate
    
    func composerDidCancel(_ controller: TWTRComposerViewController) {
        print("composerDidCancel, composer cancelled tweet")
    }
    
    func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
        print("composerDidSucceed tweet published")
    }
    func composerDidFail(_ controller: TWTRComposerViewController, withError error: Error) {
        print("composerDidFail, tweet publish failed == \(error.localizedDescription)")
    }
}


