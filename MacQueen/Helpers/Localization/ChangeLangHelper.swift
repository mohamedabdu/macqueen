//
//  ChangeLangHelper.swift
//  Tafran
//
//  Created by mohamed abdo on 5/6/18.
//  Copyright © 2018 mohamed abdo. All rights reserved.
//

import Foundation
import UIKit

func changeLang(delegate: UIViewController, closure:@escaping () -> Void ) {
    let alert = UIAlertController(title: "Change Language".localized, message: nil, preferredStyle: .actionSheet)
    alert.addAction(UIAlertAction(title: "العربية", style: .default, handler: { _ in
        setAppLang(.arabic)
        initLang()
        closure()
    }))
    alert.addAction(UIAlertAction(title: "English", style: .default, handler: { _ in
        setAppLang(.english)
        initLang()
        closure()
    }))
    alert.addAction(UIAlertAction.init(title: "cancel".localized, style: .cancel, handler: nil))
    /*If you want work actionsheet on ipad
     then you have to use popoverPresentationController to present the actionsheet,
     otherwise app will crash on iPad */
    switch UIDevice.current.userInterfaceIdiom {
    case .pad:
        alert.popoverPresentationController?.permittedArrowDirections = .up
        alert.popoverPresentationController?.sourceView = delegate.view

    default:
        break
    }
    delegate.present(alert, animated: true, completion: nil)
}
