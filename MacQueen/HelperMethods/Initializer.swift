//
//  Initializer.swift
//  Supplements
//
//  Created by Abonabih on 11/13/17.
//  Copyright © 2017 Kareem. All rights reserved.
//

import Foundation
import UIKit

class Initializer {
    
    class func getStoryBoard()-> UIStoryboard {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        return storyBoard
    }
    
    class func createViewControllerWithId(storyBoardId:String)->UIViewController{
        let storboard = getStoryBoard()
        let vc = storboard.instantiateViewController(withIdentifier: storyBoardId)
        return vc
        
    }
    
    
    class func createWindow()->UIWindow{
        
        let appDelegate = UIApplication.shared.delegate as! SceneDelegate
        
        let window = appDelegate.window
        
        return window!
        
    }
    
}
