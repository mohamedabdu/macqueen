//
//  RootWindowController.swift
//  Supplements
//
//  Created by Abonabih on 11/13/17.
//  Copyright © 2017 Kareem. All rights reserved.
//

import UIKit

class RootWindowController: NSObject {

    class func setRootWindowForHome()
    {
        guard let rootVC = UIStoryboard.init(name: "Updated", bundle: nil).instantiateViewController(withIdentifier: "MainTabBarViewController") as? MainTabBarViewController else {
            return
        }
        UIApplication.shared.windows.first?.rootViewController = rootVC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
        
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve

        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        UIView.transition(with:  UIApplication.shared.windows.first! , duration: duration, options: options, animations: {}, completion:
        { completed in
            // maybe do something on completion here
        })


    }
    class func setRootWindowForLogin()
    {
        guard let rootVC = UIStoryboard.init(name: "Signing", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else {
            return
        }
        UIApplication.shared.windows.first?.rootViewController = rootVC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
        
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        UIView.transition(with:  UIApplication.shared.windows.first! , duration: duration, options: options, animations: {}, completion:
            { completed in
                // maybe do something on completion here
        })
        
        
    }
    class func setRootWindowForRegister()
    {
        guard let rootVC = UIStoryboard.init(name: "Signing", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else {
            return
        }
        rootVC.isRegister = true
        UIApplication.shared.windows.first?.rootViewController = rootVC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
        
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        UIView.transition(with:  UIApplication.shared.windows.first! , duration: duration, options: options, animations: {}, completion:
            { completed in
                // maybe do something on completion here
        })
        
        
    }
    class func setRootLoginVc()
    {
        
        guard let rootVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginNav") as? UINavigationController else {
            return
        }
        UIApplication.shared.windows.first?.rootViewController = rootVC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }

}
