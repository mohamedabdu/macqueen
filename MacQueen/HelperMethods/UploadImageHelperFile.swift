import Alamofire
import SwiftyJSON
import ProgressHUD
import SwiftMessages

class UploadImage {
    func imagupload(myImageView:UIImageView , url:String , parameters:[String:Any]){

   
        ProgressHUD.show()
        requestWith(endUrl: url, imagedata: myImageView.image!.jpegData(compressionQuality: 1.0), parameters: parameters, onCompletion: { (json) in
        ProgressHUD.dismiss()
                        print(json)
                    }) { (error) in
                        print(error)
                    }
                }

    func requestWith(endUrl: String, imagedata: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){

    let url = endUrl
    let headers: HTTPHeaders = [
        "Content-type": "multipart/form-data" ,
        "Accept": "application/json" , "Authorization": UserStatus.token ?? ""
     ]
    Alamofire.upload(multipartFormData: { (multipartFormData) in
        for (key, value) in parameters {
            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
        }
        if let data = imagedata {
//            multipartFormData.append(data, withName: "imageName.jpeg")
            multipartFormData.append(data, withName: "image", fileName: "imagename.jpeg", mimeType: "image/jpeg")
        }
    }, to:url,headers: headers)
    { (result) in
        switch result{
        case .success(let upload, _, _):
            upload.responseJSON { response in
                let json : JSON = JSON(response.result.value)
                 print(json)
                let dic = json.dictionaryValue
                let success = dic["success"]?.boolValue
                let message = dic["message"]?.stringValue
//                let image = dic["image"]?.stringValue
                if success == true {
                    HelperMethods.showAlertWith(title: "" , msg: message , type: .success)
                } else {
                    HelperMethods.showAlertWith(title: "" , msg: message , type: .error)
                }
                if let err = response.error{
                    onError?(err)
                    return
                }
                onCompletion?(json)
            }
        case .failure(let error):
           //print("Error in upload: \(error.localizedDescription)")
            onError?(error)
        }
     }
  }
}
