//
//  HelperMethods.swift
//  AlNawares
//
//  Created by Kareem on 3/24/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation
import SwiftMessages

class HelperMethods {
    
       // MARK: - Show Alert
      static func showAlertWith(title: String? = nil, msg: String? = nil, type: Theme = .warning, layout: MessageView.Layout = .cardView) {
            // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
            // files in the main bundle first, so you can easily copy them into your project and make changes.
            let view = MessageView.viewFromNib(layout: layout)
            
            // Theme message elements with the warning style.
            
            view.configureTheme(type)
            view.button?.isHidden = true

            if type == .warning {
                view.configureTheme(backgroundColor: .red, foregroundColor: .white)
            }
            
            // Add a drop shadow.
    //        view.configureDropShadow()
            
            // Set message title, body, and icon. Here, we're overriding the default warning
            // image with an emoji character.
            
            view.configureContent(title: title ?? "", body: msg ?? "", iconText: "")

            // Show the message.
            SwiftMessages.show(view: view)
        }
}
