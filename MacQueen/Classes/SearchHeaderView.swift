//
//  SearchHeaderView.swift
//  MacQueen
//
//  Created by Kareem on 5/17/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation

class SearchHeaderView: UIView {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    
    var closure: HandlerView?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        self.contentView = Bundle.main.loadNibNamed("SearchHeaderView", owner: self, options: nil)?[0] as? UIView
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(contentView)
        self.contentView.setupFont()
        self.headerView.applyShadow(cornerRadius: 25)
    }
    @IBAction func expand(_ sender: Any) {
        closure?()
    }
}


