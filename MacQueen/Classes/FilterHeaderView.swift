//
//  SearchHeaderView.swift
//  MacQueen
//
//  Created by Kareem on 5/17/20.
//  Copyright © 2020 Kareem. All rights reserved.
//

import Foundation

class FilterHeaderView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        self.contentView = Bundle.main.loadNibNamed("FilterHeadersView", owner: self, options: nil)?[0] as? UIView
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(contentView)
        headerLabel.setupFont()
    }
  }


