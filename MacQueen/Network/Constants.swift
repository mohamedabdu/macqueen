//
//  Constants.swift
//  Hava
//
//  Created by Kareem on 8/23/19.
//  Copyright © 2019 Nasil. All rights reserved.
//



enum API: String {
    case baseURL = "https://dev2.macqueen.co/api/"
}

class Constants {
    static let isLogged = "isLogged"
    static let email = "email"
    static let token = "token"
    static let id = "id"
    static let firstName = "first_name"
    static let lastName = "last_name"
    static let mobile = "mobile"
    static let image = "image"
    static let firebaseToken = "firebaseToken"
    static let googleAPI = "AIzaSyD4NLmjaP7WLX-xWX2l6JQVrF3JH_-3yhE"
}


