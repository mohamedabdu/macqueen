//
//  NetworkManager.swift
//
//  Created by Kareem on 8/23/19.
//  Copyright © 2019 Nasil. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {

    func createHTTPRequest(
        url: String,
        params: Dictionary<String, Any>,
        method: HTTPMethod,
        headers : HTTPHeaders  ,
        completionHandler: @escaping (_ httpResponse: DataResponse<Any> , _ code:Int) -> ()) {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        manager.session.configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        print(params)

        var headersMethod = headers
        if case UserStatus.isLogged = true {
            headersMethod["Authorization"] = "Bearer \(UserStatus.token ?? "")"
//            headersMethod["Authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9kZXYyLm1hY3F1ZWVuLmNvXC9hcGlcL2xvZ2luIiwiaWF0IjoxNTkyMzc5MDg0LCJuYmYiOjE1OTIzNzkwODQsImp0aSI6Ik55NDZ3S1E3U0ZkUU9jVEQiLCJzdWIiOjEwLCJwcnYiOiIzNGIzNDM3OTI5M2I4ZmEyMDc5ZTkyZmY5MmRiYjhiN2VkMWE3MWFlIn0.URotynMUBArJ4l4dKjnW1qMj7rlqtiqJSLXFQrGzBng"
        }
        
        let request = manager.request(
            url,
            method: method,
            parameters: params,
            encoding: URLEncoding.httpBody,
            headers: headersMethod)
        
        request.responseJSON { response in
            print(response.debugDescription)
            let code  = response.response?.statusCode
            completionHandler(response , code ?? 0)
        }
    }
    func createHTTPBodyRequest(
        url: String,
        params: Dictionary<String, Any>,
        method: HTTPMethod,
        headers : HTTPHeaders  ,
        completionHandler: @escaping (_ httpResponse: DataResponse<Any> , _ code:Int) -> ()) {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        manager.session.configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        print(params)
                var request        = URLRequest(url: URL(string: url)!)
                do {
                    let data = try JSONSerialization.data(withJSONObject: params, options: [])
                    let paramString = String(data: data, encoding: String.Encoding.utf8)
                    request.httpBody = paramString?.data(using: .utf8)
                } catch let error {
                    print("Error : \(error.localizedDescription)")
                }
        request.httpMethod = method.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        var headersMethod = headers
        if case UserStatus.isLogged = true {
            headersMethod["Authorization"] = "Bearer \(UserStatus.token ?? "")"
        }
        request.allHTTPHeaderFields = headersMethod
        manager.request(request).responseJSON { (response) in
            print(response.debugDescription)
            let code  = response.response?.statusCode
            completionHandler(response , code ?? 0)
        }
    }
    
}
