//
//  UserStatus.swift
//  AlNawares
//
//  Created by Kareem on 3/11/20.
//  Copyright © 2020 Kareem. All rights reserved.
//


import Foundation
struct UserStatus {
    static func loginAlert(in delegate: UIViewController?, backToHome: Bool = false) {
        if case isLogged = true {
            return
        }
        guard let scene = delegate?.controller(LoginPopUpController.self, storyboard: .auth) else { return }
        scene.delegate = delegate
        scene.backToHome = backToHome
        delegate?.pushPop(vcr: scene)
    }
    static var isLogged:Bool? {
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.isLogged)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: Constants.isLogged)
        }
    }
    static var email:String? {
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.email)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey:  Constants.email)
        }
    }
    static var token:String? {
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.token)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey:  Constants.token)
        }
    }
    static var id:String? {
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.id)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey:  Constants.id)
        }
    }
    static var firstName:String? {
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.firstName)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey:  Constants.firstName)
        }
    }
    static var lastName:String? {
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.lastName)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey:  Constants.lastName)
        }
    }
    static var mobile:String? {
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.mobile)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey:  Constants.mobile)
        }
    }
    static var image:String? {
        set{
            UserDefaults.standard.set(newValue, forKey: Constants.image)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey:  Constants.image)
        }
    }
    
    static var firebaseToken:String? {
           set{
               UserDefaults.standard.set(newValue, forKey: Constants.firebaseToken)
               UserDefaults.standard.synchronize()
           }
           get{
               return UserDefaults.standard.string(forKey:  Constants.firebaseToken)
           }
       }
}
