//
//  ListsWebserviceFactory.swift
//  Nasil
//
//  Created by Medhat on 8/24/19.
//  Copyright © 2019 Nasil. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
class WebServiceFactory {
    func safeUrl(url: String) -> String {
        let safeURL = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        return safeURL
    }
    
    //MARK:- GetHotelsListByHotelsIDs
    func GetHotels(currency:String , lang:String , checkin_date:String , checkout_date:String , nationality_code:String , hotelsIds:[Int] , rooms: [RoomsModel] ,
                   completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        let roomsData = rooms.compactMap({($0.decRooms)})
        
        NetworkManager().createHTTPBodyRequest(
            url: API.baseURL.rawValue + "hotels" ,
            params: ["checkin_date": checkin_date  , "checkout_date": checkout_date , "lang": lang , "nationality_code":nationality_code , "hotels":hotelsIds , "rooms": roomsData] ,
            method: .post, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let hotelsDataModel = Mapper<hotelsDataModel>().map(JSONObject: response.result.value)
                
                completionHandler(nil,hotelsDataModel , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    
    //MARK:- SearchByWord
    func SearchByWord(keyword:String ,
                      completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        let url = safeUrl(url: API.baseURL.rawValue + "citiesHotels/keyword?keyword=\(keyword)")
        NetworkManager().createHTTPRequest(
            url: url ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let searchByWordModel = Mapper<searchByWordModel>().map(JSONObject: response.result.value)
                completionHandler(nil ,searchByWordModel , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    
    //MARK: - FILTER REQUESTS =====================================================
    //MARK:- FilterAmentities
    func FilterAmentities(
        completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "amenities" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let amentitiesFilterModel = Mapper<amentitiesFilterModel>().map(JSONObject: response.result.value)
                completionHandler(nil ,amentitiesFilterModel , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    //MARK:- FilterHotelTypes
    func FilterHotelTypes(lang:String ,
                          completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "hotelTypes?lang=\(lang)" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let hotelTypesFilterModel = Mapper<hotelTypesFilterModel>().map(JSONObject: response.result.value)
                completionHandler(nil ,hotelTypesFilterModel , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    //MARK:- FilterTravelTypes
    func FilterTravelStyles(lang:String ,
                            completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "travelStyles?lang=\(lang)" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let travelStylesFilterModel = Mapper<travelStylesFilterModel>().map(JSONObject: response.result.value)
                completionHandler(nil ,travelStylesFilterModel , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    // ==========================================================================================
    
    //MARK:- GetHotelsIdsByCityId
    func GetHotelsByCityId(cityId:Int ,
                           completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "provider-cities/\(cityId)/hotels" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let hotelsByCityIdModel = Mapper<GetHotelsByCityIdModel>().map(JSONObject: response.result.value)
                completionHandler(nil ,hotelsByCityIdModel , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    
    //MARK:- CheckHotelAvailability
    func CheckHotelAvailability(hotelId:Int , provider:String , checkin_date:String , checkout_date:String , nationality_code:String , key:String , rooms: [RoomsModel] ,
                                completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        let roomsData = rooms.compactMap({($0.decRooms)})

        NetworkManager().createHTTPBodyRequest(
            url: API.baseURL.rawValue + "hotels/availability/\(hotelId)/\(provider)" ,
            params: ["checkin_date":checkin_date , "checkout_date" : checkout_date , "nationality_code":nationality_code , "key":key , "rooms":roomsData] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let hotelsByCityIdModel = Mapper<CheckDealModel>().map(JSONObject: response.result.value)
                completionHandler(nil ,hotelsByCityIdModel , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    
    //MARK:- Countries
    func Countries(lang:String ,
                          completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "countries?lang=\(lang)" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = Mapper<Countries>().map(JSONObject: response.result.value)
                completionHandler(nil ,model , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    //MARK:- FilterHotelTypes
    func Cities(lang:String,countryID: Int ,
                   completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "countries/\(countryID)/cities?lang=\(lang)" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = Mapper<Countries>().map(JSONObject: response.result.value)
                completionHandler(nil ,model , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    //MARK:- CheckHotelAvailability
    func book(params: [String: Any] , hotelId: Int, provider: String,
                                completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: BookModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPBodyRequest(
            url: API.baseURL.rawValue + "hotels/book/\(hotelId)/\(provider)/1" ,
            params: params ,
            method: .post, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                
                
                let bookModel = try? JSONDecoder().decode(BookModel.self, from: response.data ?? Data())
                completionHandler(nil ,bookModel , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    func makePaymentSdk(
        completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: PaymentTokenModel? ,
        _ statusCode:Int) -> ()) {
        let url =  "\(API.baseURL.rawValue)payments/1/getToken"
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default)
            .responseJSON { response in
                let statusCode = response.response?.statusCode
                if statusCode == 200 {
                    let hotelsByCityIdModel = Mapper<PaymentTokenModel>().map(JSONObject: response.result.value)
                    completionHandler(nil ,hotelsByCityIdModel , response.response?.statusCode ?? 0)
                } else {
                }
        }
    }
    func getSavedCards(
        completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: SavedCardModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPRequest(
            url: "\(API.baseURL.rawValue)users/cards" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                let statusCode = response.response?.statusCode
                if statusCode == 200 {
                    let hotelsByCityIdModel = Mapper<SavedCardModel>().map(JSONObject: response.result.value)
                    completionHandler(nil ,hotelsByCityIdModel , response.response?.statusCode ?? 0)
                } else {
                }
        }
    }
    //MARK:- Hotels Nerby
    func HotelsNearby(lat: Double?, lng: Double?,
                completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        var paramters: [String: Any] = [:]
        if lat != nil && lng != nil {
            paramters["lat"] = lat ?? 0
            paramters["lng"] = lng ?? 0
        }
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "hotels/nearby" ,
            params: paramters ,
            method: .post, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = Mapper<hotelsDataModel>().map(JSONObject: response.result.value)
                completionHandler(nil ,model , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    //MARK:- Blogs
    func Blogs(
                      completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: BlogModel? ,
        _ statusCode:Int) -> ()) {
       
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "blogs" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = try? JSONDecoder().decode(BlogModel.self, from: response.data ?? Data())
                completionHandler(nil ,model , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    //MARK:- Register
    func Register( paramters: [String: Any],
        completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: UserModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "register" ,
            params: paramters ,
            method: .post, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = try? JSONDecoder().decode(UserModel.self, from: response.data ?? Data())
                if model?.errors != nil {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.errors?.description() ?? "") ,nil , response.response?.statusCode ?? 0)
                } else {
                    completionHandler(nil ,model , response.response?.statusCode ?? 0)
                }
                //                    }
                //                }
        }
    }
    //MARK:- Login
    func Login( paramters: [String: Any],
                   completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: UserModel? ,
        _ statusCode:Int) -> ()) {
        
        var parms = paramters
        parms["device_token"] = UserDefaults.standard.string(forKey: "DEVICE_TOKEN") ?? ""
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "login" ,
            params: parms ,
            method: .post, headers: [:]) { (response , code)  in
//                self.handleErrors(response) {(errorModel) in
//                    if(errorModel != nil) {
//                        completionHandler(errorModel,nil)
//                    }
//                }
                //                    } else {
                let model = try? JSONDecoder().decode(UserModel.self, from: response.data ?? Data())
                if model?.errors != nil {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.errors?.description() ?? "") ,nil , response.response?.statusCode ?? 0)
                } else if code > 300 {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.message ?? "") ,nil , response.response?.statusCode ?? 0)
                } else {
                    completionHandler(nil ,model , response.response?.statusCode ?? 0)
                }
                //                    }
                //                }
        }
    }
    //MARK:- Actiivate
    func ActiveUser( email: String, code: String,
                completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: UserModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "activate/\(email)/\(code)" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = try? JSONDecoder().decode(UserModel.self, from: response.data ?? Data())
                completionHandler(nil ,model , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    func resendActivation( email: String,
                     completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: UserModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "activate/resend/\(email)?agentType=mobile" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = try? JSONDecoder().decode(UserModel.self, from: response.data ?? Data())
                completionHandler(nil ,model , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    //MARK:- UpdateProfile
    func UpdateProfile( paramters: [String: Any],
                completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: UserModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPBodyRequest(
            url: API.baseURL.rawValue + "update-profile" ,
            params: paramters ,
            method: .put, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = try? JSONDecoder().decode(UserModel.self, from: response.data ?? Data())
                if code > 300 {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.errors?.description() ?? "") ,nil , response.response?.statusCode ?? 0)
                } else {
                    completionHandler(nil ,model , response.response?.statusCode ?? 0)
                }
                //                    }
                //                }
        }
    }
    //MARK:- Login
    func ForgetPassword( paramters: [String: Any],
                completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: UserModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "forgot-password" ,
            params: paramters ,
            method: .post, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    }
                //                }
                //                    } else {
                let model = try? JSONDecoder().decode(UserModel.self, from: response.data ?? Data())
                if code > 300 {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.message ?? "") ,nil , response.response?.statusCode ?? 0)
                } else {
                    completionHandler(nil ,model , response.response?.statusCode ?? 0)
                }
                //                    }
                //                }
        }
    }
    //MARK:- Actiivate
    func ResetPasswordCode( email: String, code: String,
                     completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: UserModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "reset-password/\(email)/\(code)" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                
                let model = try? JSONDecoder().decode(UserModel.self, from: response.data ?? Data())
                if code > 300 {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.message ?? "") ,nil , response.response?.statusCode ?? 0)
                } else {
                    completionHandler(nil ,model , response.response?.statusCode ?? 0)
                }
                //                    }
                //                }
        }
    }
    
    //MARK:- Actiivate
    func ResetPassword( email: String, code: String,paramters: [String: Any] ,
                            completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: UserModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "reset-password/\(email)/\(code)" ,
            params: paramters ,
            method: .post, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                
                let model = try? JSONDecoder().decode(UserModel.self, from: response.data ?? Data())
                if code > 300 {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.message ?? "") ,nil , response.response?.statusCode ?? 0)
                } else {
                    completionHandler(nil ,model , response.response?.statusCode ?? 0)
                }
                //                    }
                //                }
        }
    }
    //MARK:- Actiivate
    func ChangePassword(paramters: [String: Any] ,
                        completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: UserModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "change-password" ,
            params: paramters ,
            method: .post, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                
                let model = try? JSONDecoder().decode(UserModel.self, from: response.data ?? Data())
                if model?.errors != nil {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.errors?.description() ?? "") ,nil , response.response?.statusCode ?? 0)
                } else if code > 300 {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.message ?? "") ,nil , response.response?.statusCode ?? 0)
                } else {
                    completionHandler(nil ,model , response.response?.statusCode ?? 0)
                }
                //                    }
                //                }
        }
    }
    //MARK:- Hotels Nerby
    func myReservations(
                      completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        let paramters: [String: Any] = [:]
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "hotel-reservations" ,
            params: paramters ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = Mapper<hotelsDataModel>().map(JSONObject: response.result.value)
                completionHandler(nil ,model , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    //MARK:- Hotels Favorites
    func favorites(
        completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: hotelsDataModel? ,
        _ statusCode:Int) -> ()) {
        let paramters: [String: Any] = [:]
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "hotels/favourites" ,
            params: paramters ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = Mapper<hotelsDataModel>().map(JSONObject: response.result.value)
                completionHandler(nil ,model , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    //MARK:- Hotels Favorites
    func addFavorite(hotelID: Int,
        completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: HotelSingleModel? ,
        _ statusCode:Int) -> ()) {
        let paramters: [String: Any] = [:]
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "hotels/\(hotelID)/favourite" ,
            params: paramters ,
            method: .get, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let model = Mapper<HotelSingleModel>().map(JSONObject: response.result.value)
                completionHandler(nil ,model , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    //MARK:- Login
    func SocialLogin( paramters: [String: Any],
                completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: UserModel? ,
        _ statusCode:Int) -> ()) {
        var parms = paramters
        parms["device_token"] = UserDefaults.standard.string(forKey: "DEVICE_TOKEN") ?? ""
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "social-login" ,
            params: parms ,
            method: .post, headers: [:]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    }
                //                }
                //                    } else {
                let model = try? JSONDecoder().decode(UserModel.self, from: response.data ?? Data())
                if model?.errors != nil {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.errors?.description() ?? "") ,nil , response.response?.statusCode ?? 0)
                } else if code > 300 {
                    completionHandler(ErrorModel(eCode: 422, eMsg: model?.message ?? "") ,nil , response.response?.statusCode ?? 0)
                } else {
                    completionHandler(nil ,model , response.response?.statusCode ?? 0)
                }
                //                    }
                //                }
        }
    }
    func storeSearch( params: [String: Any],
        completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: String? ,
        _ statusCode:Int) -> ()) {

        NetworkManager().createHTTPRequest(
            url: "\(API.baseURL.rawValue)recent-search" ,
            params: params ,
            method: .post, headers: [:]) { (response , code)  in
                let statusCode = response.response?.statusCode
                if statusCode == 200 {
                    completionHandler(nil , "" , response.response?.statusCode ?? 0)
                } else {
                }
        }
    }
    func recentSearch(
                      completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: RecentSearchModel? ,
        _ statusCode:Int) -> ()) {
        
        let deviceToken = UserDefaults.standard.string(forKey: "DEVICE_TOKEN") ?? ""
        NetworkManager().createHTTPRequest(
            url: "\(API.baseURL.rawValue)recent-search?device_token=\(deviceToken)" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                let statusCode = response.response?.statusCode
                if statusCode == 200 {
                    let data = try? JSONDecoder().decode(RecentSearchModel.self, from: response.data ?? Data())
                    completionHandler(nil , data , response.response?.statusCode ?? 0)
                } else {
                }
        }
    }
    func RemoveUserCards(cardId:Int ,
                         completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: Mappable? ,
        _ statusCode:Int) -> ()) {
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "users/cards/\(cardId)" ,
            params: [:],
            method: .delete , headers: ["Accept": "application/json" , "Authorization": "Bearer \(UserStatus.token ?? "") "]) { (response , code)  in
                //                self.handleErrors(response) {(errorModel) in
                //                    if(errorModel != nil) {
                //                        completionHandler(errorModel,nil)
                //                    } else {
                let CardsModel = Mapper<CardsModel>().map(JSONObject: response.result.value)
                completionHandler(nil,CardsModel , response.response?.statusCode ?? 0)
                //                    }
                //                }
        }
    }
    func reservationDetail(id: Int?,
        completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: ReservationModel? ,
        _ statusCode:Int) -> ()) {
        
        NetworkManager().createHTTPRequest(
            url: "\(API.baseURL.rawValue)hotel-reservations/\(id ?? 0)" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                let statusCode = response.response?.statusCode
                if statusCode == 200 {
                    let data = try? JSONDecoder().decode(ReservationModel.self, from: response.data ?? Data())
                    completionHandler(nil , data , response.response?.statusCode ?? 0)
                } else {
                }
        }
    }
    //MARK:- Countries
    func CountriesDial(completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: CountriesModel? ,
        _ statusCode:Int) -> ()) {
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "get-country-dials?lang=\(Localizer.current)" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                let statusCode = response.response?.statusCode
                if statusCode == 200 {
                    let data = try? JSONDecoder().decode(CountriesModel.self, from: response.data ?? Data())
                    completionHandler(nil , data , response.response?.statusCode ?? 0)
                } else {
                }
        }
    }
    //MARK:- Countries
    func Notifications(completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: NotificationModel? ,
        _ statusCode:Int) -> ()) {
        
        var deviceToken = UserDefaults.standard.string(forKey: "DEVICE_TOKEN") ?? ""
        deviceToken = "coKfiyNlg-g:APA91bH2S5lWp4QnDUKGRxwtnbsLFxWa8PdjetDIlHl2wvrH4VdXZ8oeiQk5RbpE4wPA34I0rx9RKBN8odoL2fhI8IeCyxir9psI6cmgBaXsrrxQ3Q2tTplvlzkLPmJvivBUh8x3O5bR"

        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "notifications?device_token=\(deviceToken)" ,
            params: [:] ,
            method: .get, headers: [:]) { (response , code)  in
                let statusCode = response.response?.statusCode
                if statusCode == 200 {
                    let data = try? JSONDecoder().decode(NotificationModel.self, from: response.data ?? Data())
                    completionHandler(nil , data , response.response?.statusCode ?? 0)
                } else {
                }
        }
    }
    //MARK:- Countries
    func DeleteNotifications(id: String, completionHandler: @escaping (
        _ error: ErrorModel?,
        _ jsonData: NotificationModel? ,
        _ statusCode:Int) -> ()) {
        
        var deviceToken = UserDefaults.standard.string(forKey: "DEVICE_TOKEN") ?? ""
        deviceToken = "coKfiyNlg-g:APA91bH2S5lWp4QnDUKGRxwtnbsLFxWa8PdjetDIlHl2wvrH4VdXZ8oeiQk5RbpE4wPA34I0rx9RKBN8odoL2fhI8IeCyxir9psI6cmgBaXsrrxQ3Q2tTplvlzkLPmJvivBUh8x3O5bR"
        NetworkManager().createHTTPRequest(
            url: API.baseURL.rawValue + "notifications/\(id)?device_token=\(deviceToken)" ,
            params: [:] ,
            method: .delete, headers: [:]) { (response , code)  in
                let statusCode = response.response?.statusCode
                if statusCode == 200 {
                    let data = try? JSONDecoder().decode(NotificationModel.self, from: response.data ?? Data())
                    completionHandler(nil , data , response.response?.statusCode ?? 0)
                } else {
                }
        }
    }
}

